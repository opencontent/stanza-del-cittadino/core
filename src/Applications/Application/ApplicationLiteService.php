<?php

declare(strict_types=1);

namespace App\Applications\Application;

use App\Applications\Application\DTOs\ApplicationLiteData;
use App\Applications\Application\DTOs\PersonData;
use App\Applications\Domain\ApplicationLiteRepositoryInterface;
use App\Applications\Domain\ExternalAction;
use App\Applications\Exceptions\ApplicationLiteException;
use App\Entity\ApplicationLite;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use App\Entity\Servizio;
use App\Services\CPSUserProvider;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class ApplicationLiteService
{
  private EntityManagerInterface $entityManager;
  private CPSUserProvider $cpsUserProvider;
  private ApplicationLiteRepositoryInterface $applicationLiteRepository;

  public function __construct(
    ApplicationLiteRepositoryInterface $applicationLiteRepository,
    CPSUserProvider $cPSUserProvider,
    EntityManagerInterface $entityManager
  ) {
    $this->applicationLiteRepository = $applicationLiteRepository;
    $this->cpsUserProvider = $cPSUserProvider;
    $this->entityManager = $entityManager;
  }

  /** @throws ApplicationLiteException */
  public function createOrUpdate(
    ApplicationLiteData $data,
    Servizio $service,
    Ente $ente
  ): ApplicationLite {
    /** @var PraticaRepository $applicationRepo */
    $applicationRepo = $this->entityManager->getRepository(Pratica::class);
    $applicationLite = $applicationRepo->findApplicationLiteByExternalId($data->externalId);

    $actions = [];
    foreach ($data->actions as $action) {
      $actions[] = new ExternalAction($action->action, $action->url);
    }

    if ($applicationLite) {

      $applicationLite->update($data->status, new DateTime("now"));
      $applicationLite->replaceExternalActions($actions);
      $applicationLite->replaceProcessHistory($data->processHistory);

    } else {

      $applicant = $this->createOrFetchPerson($data->applicant);
      $this->entityManager->persist($applicant);

      $applicationLite = ApplicationLite::createAsExternal(
        $data->externalId,
        $service,
        $applicant,
        $data->subject,
        $data->status,
        $ente,
      );

      $applicationLite->replaceExternalActions($actions);
      $applicationLite->replaceProcessHistory($data->processHistory);

      if ($data->beneficiary) {
        $beneficiary = $this->createOrFetchPerson($data->beneficiary);
        $this->entityManager->persist($beneficiary);

        $applicationLite->setBeneficiary($beneficiary);
      }

    }

    return $this->applicationLiteRepository->save($applicationLite);
  }

  private function createOrFetchPerson(PersonData $data): CPSUser
  {
    $userRepo = $this->entityManager->getRepository(CPSUser::class);
    $person = $userRepo->findByTaxCode($data->identifiers->taxCode);

    if (!$person) {
      $person = $this->cpsUserProvider->createPerson($data);
    }

    return $person;
  }
}
