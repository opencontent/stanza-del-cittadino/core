<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\AllegatoScia;
use PHPUnit\Framework\TestCase;

class AllegatoSciaTest extends TestCase
{
  public function testConstructorInitializesFields(): void
  {
    $allegatoScia = new AllegatoScia();

    $this->assertEquals(AllegatoScia::TYPE_DEFAULT, $allegatoScia->getType());
  }

  public function testGetType(): void
  {
    $allegatoScia = new AllegatoScia();
    $this->assertEquals(AllegatoScia::TYPE_DEFAULT, $allegatoScia->getType());
  }
}
