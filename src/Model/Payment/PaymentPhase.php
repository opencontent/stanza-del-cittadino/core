<?php

namespace App\Model\Payment;

class PaymentPhase
{
  public const PHASE_REQUEST = 'request';
  public const PHASE_RELEASE = 'release';
}
