<?php


namespace App\Form\Admin\Servizio;


use App\Entity\Servizio;
use App\Form\Base\BlockQuoteType;
use App\Form\Base\PaymentConfigType;
use App\Model\Gateway;
use App\Payment\Gateway\GenericExternalPay;
use App\Payment\GatewayCollection;
use App\Payment\PaymentDataInterface;
use App\Services\FormServerApiAdapterService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Contracts\Translation\TranslatorInterface;

class MultiplePaymentsDataType extends AbstractType
{

  private EntityManagerInterface $em;

  private GatewayCollection $gatewayCollection;

  private TranslatorInterface $translator;

  public function __construct(
    TranslatorInterface $translator,
    EntityManagerInterface $entityManager,
    FormServerApiAdapterService $formServerService,
    GatewayCollection $gatewayCollection

  )
  {
    $this->em = $entityManager;
    $this->gatewayCollection = $gatewayCollection;
    $this->translator = $translator;
  }

  public function buildForm(FormBuilderInterface $builder, array $options): void
  {

    /** @var Servizio $service */
    $service = $builder->getData();

    $tenantGateways = $service->getEnte()->getGateways();
    // Aggiungo i gateways disabilitati a livello di tenant se attivi sul servizio
    /*if (!empty($selectedGatewaysIdentifiers) && !in_array($selectedGatewaysIdentifiers[0], $tenantGateways, true)) {
      $disabledSelectedIdentifier = $selectedGatewaysIdentifiers[0];
      $tenantGateways[$disabledSelectedIdentifier] = $selectedGateways[$disabledSelectedIdentifier];
    }*/

    $normalizedTenantGateways = [];
    foreach ($tenantGateways as $s) {
      $normalizedTenantGateways [$s['identifier']] = $s;
    }
    $tenantGateways = $normalizedTenantGateways;

    $availableGateways = $this->gatewayCollection->getAvailablePaymentGateways();
    $gatewaysChoice = [];
    foreach ($tenantGateways as $g) {
      $identifier = $g['identifier'];
      if (isset($availableGateways[$identifier])) {
        $gatewaysChoice[$availableGateways[$identifier]['name']] = $identifier;
      }
    }


    $builder
      ->add('paymentGatewayIdentifier', ChoiceType::class, [
        'choices' => $gatewaysChoice,
        'choice_attr' => function($choice, $key, $value) use ($availableGateways, $service) {
          // adds a class like attending_yes, attending_no, etc
          $g = $availableGateways[$choice]['handler'];
          $isGenericExternalPay = $g instanceof GenericExternalPay;
          $attr = [];
          if ($isGenericExternalPay) {
            $attr['class'] = 'external-pay-choice';
            $attr['data-tenant'] = $service->getEnte()->getId();
            $attr['data-service'] = $service->getId();
            $attr['data-identifier'] = $choice;
            $attr['data-url'] = $availableGateways[$choice]['url'];
          }
          return $attr;
        },
        'multiple' => false,
        'required' => false,
        'label' => 'steps.common.select_payment_gateway.label',
        'placeholder' => 'none',
      ])
    ;

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));

  }

  public function onPreSubmit(FormEvent $event): void
  {
    /** @var Servizio $service */
    $service = $event->getForm()->getData();
    $data = $event->getData();

  }


  public function getBlockPrefix(): string
  {
    return 'payment_data';
  }
}
