<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230716112915 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE job (id UUID NOT NULL, tenant_id UUID NOT NULL, type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, args JSON DEFAULT NULL, original_file_name VARCHAR(255) DEFAULT NULL, file_name VARCHAR(255) DEFAULT NULL, file_size NUMERIC(14, 2) DEFAULT NULL, mime_type VARCHAR(255) DEFAULT NULL, metadata JSON DEFAULT NULL, output TEXT DEFAULT NULL, running_output TEXT DEFAULT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, terminated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, hostname VARCHAR(255) DEFAULT NULL, retry INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE job');
    }
}
