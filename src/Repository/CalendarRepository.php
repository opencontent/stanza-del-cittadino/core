<?php

namespace App\Repository;

use App\Entity\Calendar;
use App\Entity\OperatoreUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Calendar>
 *
 * @method Calendar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Calendar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Calendar[]    findAll()
 * @method Calendar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalendarRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Calendar::class);
  }

  /**
   * @throws ORMException
   * @throws OptimisticLockException
   */
  public function add(Calendar $entity, bool $flush = true): void
  {
    $this->_em->persist($entity);
    if ($flush) {
      $this->_em->flush();
    }
  }

  /**
   * @throws ORMException
   * @throws OptimisticLockException
   */
  public function remove(Calendar $entity, bool $flush = true): void
  {
    $this->_em->remove($entity);
    if ($flush) {
      $this->_em->flush();
    }
  }

  public function findByModerator(OperatoreUser $operator)
  {
    return $this->createQueryBuilder('c')
      ->leftJoin('c.moderators', 'm')
      ->andWhere('m.id = :operator')
      ->setParameter('operator', $operator->getId())
      ->orderBy('c.title', 'ASC')
      ->getQuery()
      ->getResult();
  }

  // /**
  //  * @return UserCalendar[] Returns an array of UserCalendar objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('u')
          ->andWhere('u.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('u.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?UserCalendar
  {
      return $this->createQueryBuilder('u')
          ->andWhere('u.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
