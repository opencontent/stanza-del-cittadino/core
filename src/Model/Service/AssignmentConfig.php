<?php


namespace App\Model\Service;


use App\Model\Payment\PaymentPhase;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class AssignmentConfig implements \JsonSerializable
{

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Module field")
   * @Groups({"read"})
   */
  private string $field = '';

  /**
   * @Serializer\Type("array")
   * @OA\Property(description="Map of field values and realted user group id"})
   * @Groups({"read"})
   */
  private array $map = [];

  /**
   * @Serializer\Type("array")
   * @OA\Property(description="Map of custom field values and realted user group id"})
   * @Groups({"read"})
   */
  private array $customMap = [];

  public function getField(): string
  {
    return $this->field;
  }

  public function setField(string $field): void
  {
    $this->field = $field;
  }

  public function getMap(): array
  {
    return $this->map;
  }

  public function setMap(array $map): void
  {
    $this->map = $map;
  }

  public function hasMapItem($key): bool
  {
    return isset($this->map[$key]);
  }

  public function getItem($key): ?string
  {
    return $this->map[$key] ?? $this->customMap[$key] ?? null;
  }


  public function setCustomMap(array $map): void
  {
    $this->customMap = $map;
  }

  public function getCustomMap(): array
  {
    return $this->customMap;
  }


  /**
   * @return array
   */
  public function jsonSerialize(): ?array
  {
    return [
      'field' => $this->field,
      'map' => $this->map,
      'custom_map' => $this->customMap,
    ];
  }


  public static function fromArray($data = []): ?AssignmentConfig
  {
    $assignmentConfig = new self();
    $assignmentConfig->setField($data['field'] ?? 'null');
    $assignmentConfig->setMap($data['map'] ?? []);
    $assignmentConfig->setCustomMap($data['custom_map'] ?? []);

    return $assignmentConfig;
  }

}
