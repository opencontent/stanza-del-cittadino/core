export default function checkbox(ctx) {
  const getAttr = (attr) => {
    const attrs = [];

    Object.keys(attr).forEach((key) => {
      attrs.push(`${key}="${attr[key]}"`);
    });

    return attrs.join(" ");
  };

  return `
<div class="form-check checkbox">
  <label
    class="${ctx.input.labelClass} form-check-label"
    for="${ctx.instance.id}-${ctx.component.key}"
    id="l-${ctx.instance.id}-${ctx.component.key}"
  >
  <${ctx.input.type}
  ref="input"
  id="${ctx.instance.id}-${ctx.component.key}"
  aria-labelledby="l-${ctx.instance.id}-${ctx.component.key}"
  ${getAttr(ctx.input.attr)}
  ${(ctx.checked) ? 'checked=true' : ''}
  aria-required="${ctx.component.validate.required}"
  ${(ctx.component.description) ? `aria-describedby="d-${ctx.instance.id}-${ctx.component.key}"`: '' }
  >
     ${!(ctx.self.labelIsHidden()) ? `<span>${ctx.input.label}</span>` : ''}
    ${ctx.input.content ? ctx.input.content : ''}
    </${ctx.input.type}>
    ${(ctx.component.tooltip) ? `<i ref="tooltip" tabindex="0" class="${ctx.iconClass('question-sign')} text-muted" data-tooltip="${ctx.component.tooltip}"></i>` : ''}
</label>
</div> `
}
