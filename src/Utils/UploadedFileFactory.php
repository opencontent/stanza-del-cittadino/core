<?php

declare(strict_types=1);

namespace App\Utils;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedFileFactory
{
  public static function fromJsonString(string $content, ?string $fileName = null): UploadedFile
  {
    return self::fromString($content, 'application/json', $fileName);
  }

  public static function fromString(string $content, ?string $mimeType = null, ?string $fileName = null): UploadedFile
  {
    return self::createFile(
      $content,
      false,
      $mimeType,
      $fileName,
    );
  }

  public static function fromBase64String(
    string  $base64Content,
    ?string $mimeType = null,
    ?string $fileName = null
  ): UploadedFile {

    $matches = [];
    preg_match('/data:([^;]*);base64,(.*)/', $base64Content, $matches);
    if (isset($matches[1])) {
      $mimeType = $matches[1];
    }

    if (isset($matches[2])) {
      $base64Content = $matches[2];
    }

    return self::createFile(
      $base64Content,
      true,
      $mimeType,
      $fileName,
    );
  }

  private static function createFile(
    string  $content,
    bool    $requireBase64Decoding,
    ?string $mimeType = null,
    ?string $fileName = null
  ): UploadedFile {
    $filePath = tempnam(sys_get_temp_dir(), 'UploadedFile');

    $file = fopen($filePath, 'w');

    if ($requireBase64Decoding) {
      stream_filter_append($file, 'convert.base64-decode');
    }

    fwrite($file, $content);

    $meta_data = stream_get_meta_data($file);
    $path = $meta_data['uri'];
    if (empty($fileName)) {
      $fileName = basename($path);
    }

    fclose($file);

    return new UploadedFile(
      $path,
      $fileName,
      $mimeType,
      null,
      true,
    );
  }
}
