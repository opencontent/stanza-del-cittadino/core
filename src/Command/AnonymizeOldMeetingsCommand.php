<?php

namespace App\Command;

use App\Entity\Meeting;
use App\Utils\StringUtils;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AnonymizeOldMeetingsCommand extends Command
{
  protected static $defaultName = 'ocsdc:anonymize-old-meetings';
  protected static $defaultDescription = 'Anonymizes sensitive data of meetings that occurred more than X weeks ago.';

  private EntityManagerInterface $entityManager;

  protected function configure(): void
  {
    $this->addOption('weeks', 'w', InputOption::VALUE_OPTIONAL, 'Number of weeks, default is 6', 6);
  }

  public function __construct(EntityManagerInterface $entityManager)
  {
    parent::__construct();
    $this->entityManager = $entityManager;
  }

  /**
   * @throws Exception
   */
  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $io = new SymfonyStyle($input, $output);
    $io->title('Anonymizing old meetings');
    $weeks = $input->getOption('weeks');

    $thresholdDate = new \DateTime('-'. $weeks .' weeks');

    $meetingRepository = $this->entityManager->getRepository(Meeting::class);
    $meetings = $meetingRepository->createQueryBuilder('m')
      ->where('m.toTime < :threshold')
      ->andWhere('m.anonymizedAt IS NULL')
      ->setParameter('threshold', $thresholdDate)
      ->getQuery()
      ->getResult();

    if (empty($meetings)) {
      $io->success('No meetings to anonymize.');
      return Command::SUCCESS;
    }

    $now = new \DateTime();

    /** @var Meeting $meeting */
    foreach ($meetings as $meeting) {
      $meeting
        ->setName(StringUtils::maskData($meeting->getName()))
        ->setEmail(StringUtils::maskData($meeting->getEmail()))
        ->setPhoneNumber(StringUtils::maskData($meeting->getPhoneNumber()))
        ->setFiscalCode(StringUtils::maskData($meeting->getFiscalCode()))
        ->setUserMessage(StringUtils::maskData($meeting->getUserMessage()))
        ->setReason(StringUtils::maskData($meeting->getReason()))
        ->setMotivationOutcome(StringUtils::maskData($meeting->getMotivationOutcome()))
        ->setAnonymizedAt($now)
      ;
    }

    $this->entityManager->flush();

    $io->success(sprintf('%d meeting(s) anonymized successfully.', count($meetings)));
    return Command::SUCCESS;
  }
}
