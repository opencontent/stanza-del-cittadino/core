<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Entity\ScheduledAction;
use App\Entity\SciaPraticaEdilizia;
use App\Mapper\Giscom\GiscomStatusMapper;
use App\Services\DelayedGiscomAPIAdapterService;
use App\Services\GiscomAPIAdapterService;
use App\Services\GiscomAPIMapperService;
use GuzzleHttp\Psr7\Response;
use Tests\Base\AbstractAppTestCase;
use Exception;

class DelayedGiscomAPIAdapterServiceTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();
    $this->cleanDb(ScheduledAction::class);
  }

  public function testServiceExists(): void
  {
    $giscomApiAdapter = $this->container->get('ocsdc.giscom_api.adapter');
    $this->assertNotNull($giscomApiAdapter);

    $giscomApiAdapter = $this->container->get('ocsdc.giscom_api.adapter_delayed');
    $this->assertNotNull($giscomApiAdapter);
  }

  public function testServiceSendsPraticaToGISCOM(): void
  {
    $pratica = $this->setupPraticaScia([], true);
    $pratica->setStatus(SciaPraticaEdilizia::STATUS_REGISTERED);
    $this->em->persist($pratica);
    $this->em->flush();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(201, [], json_encode([
        'Id' => 1234,
        'Stato' => [
          'Codice' => GiscomStatusMapper::GISCOM_STATUS_PREISTRUTTORIA,
          'Note' => '',
        ],
      ])),
    ]);

    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $this->getMockLogger(),
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $delayedGiscomApiAdapter = new DelayedGiscomAPIAdapterService(
      $giscomApiAdapter,
      $this->em,
      $this->getMockLogger(),
      $this->container->get('ocsdc.schedule_action_service')
    );
    $delayedGiscomApiAdapter->sendPraticaToGiscom($pratica);
    $this->executeCron($delayedGiscomApiAdapter);
    /*
     * if we reached this point all is fine
     */
    $this->assertTrue(true);
  }

  public function testServiceLogsRemoteError(): void
  {
    $pratica = $this->setupPraticaScia([], true);
    $guzzleMock = $this->getMockGuzzleClient([new Response(400)]);

    $loggerMock = $this->getMockLogger();
    $loggerMock->expects($this->once())->method('info');
    $loggerMock->expects($this->once())->method('error');

    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $loggerMock,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $delayedGiscomApiAdapter = new DelayedGiscomAPIAdapterService(
      $giscomApiAdapter,
      $this->em,
      $this->getMockLogger(),
      $this->container->get('ocsdc.schedule_action_service')
    );
    $delayedGiscomApiAdapter->sendPraticaToGiscom($pratica);

    $exception = null;
    try {
      $this->executeCron($delayedGiscomApiAdapter);
    } catch (Exception $e) {
      $exception = $e;
    }

    $this->assertInstanceOf(Exception::class, $exception);

  }

  public function testServiceLogsRemoteResponse(): void
  {
    $pratica = $this->setupPraticaScia([], true);
    $pratica->setStatus(SciaPraticaEdilizia::STATUS_REGISTERED);
    $this->em->flush();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(201, [], json_encode([
        'Id' => 1234,
        'Stato' => [
          'Codice' => GiscomStatusMapper::GISCOM_STATUS_PREISTRUTTORIA,
          'Note' => '',
        ],
      ])),
      new Response(200, [], json_encode([
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
      ])),
    ]);

    $loggerMock = $this->getMockLogger();
    $loggerMock->expects($this->exactly(3))->method('info');

    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $loggerMock,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $delayedGiscomApiAdapter = new DelayedGiscomAPIAdapterService(
      $giscomApiAdapter,
      $this->em,
      $this->getMockLogger(),
      $this->container->get('ocsdc.schedule_action_service')
    );
    $delayedGiscomApiAdapter->sendPraticaToGiscom($pratica);
    $this->executeCron($delayedGiscomApiAdapter);
  }

  public function testServiceReadsRemoteCF(): void
  {
    $pratica = $this->setupPraticaScia();

    $guzzleMock = $this->getMockGuzzleClient([
      new Response(200, [], json_encode([
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
        $this->getCPSUserBaseData()['codiceFiscale'],
      ])),
    ]);

    $loggerMock = $this->getMockLogger();
    $loggerMock->expects($this->exactly(2))->method('info');

    $this->assertCount(0, $pratica->getRelatedCFs());

    $giscomApiAdapter = new GiscomAPIAdapterService(
      $guzzleMock,
      $this->em,
      $loggerMock,
      $this->getMockedGiscomMapper($this->em),
      $this->container->get('ocsdc.pratica_status_service'),
      $this->container->get('ocsdc.status_mapper.giscom')
    );
    $delayedGiscomApiAdapter = new DelayedGiscomAPIAdapterService(
      $giscomApiAdapter,
      $this->em,
      $this->getMockLogger(),
      $this->container->get('ocsdc.schedule_action_service')
    );
    $delayedGiscomApiAdapter->askRelatedCFsforPraticaToGiscom($pratica);
    $this->executeCron($delayedGiscomApiAdapter);

    $this->em->refresh($pratica);
    $this->assertCount(3, $pratica->getRelatedCFs());

  }

  private function executeCron(DelayedGiscomAPIAdapterService $delayedGiscomApiAdapter): void
  {
    $scheduleService = $this->container->get('ocsdc.schedule_action_service');
    $actions = $scheduleService->getActions();
    foreach ($actions as $action) {
      if ('ocsdc.giscom_api.adapter' == $action->getService()) {
        $delayedGiscomApiAdapter->executeScheduledAction($action);
        $scheduleService->markAsDone($action);
      }
    }
    $scheduleService->done();
  }

  private function getMockedGiscomMapper($em)
  {
    return new GiscomAPIMapperService($em);
  }
}
