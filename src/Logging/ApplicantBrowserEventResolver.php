<?php

declare(strict_types=1);

namespace App\Logging;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Pratica;
use App\Event\PraticaOnChangeStatusEvent;
use Symfony\Component\HttpFoundation\Request;

class ApplicantBrowserEventResolver
{
  public function getEventLogInfo(Event $event, $eventName)
  {
    if ($event instanceof PraticaOnChangeStatusEvent && $event->getNewStateIdentifier() == Pratica::STATUS_PRE_SUBMIT){
      return array(
        'description' => '[' . $event->getPratica()->getId() . '] ' . Request::createFromGlobals()->headers->get('User-Agent'),
        'type' => 'applicant.browser',
      );
    }

    return [];
  }

}
