## User Story  

- **Come** [tipo di utente],
- **voglio** [cosa desidera fare] 
- **così che** [risultato desiderato].  

_Una User Story è una descrizione breve e semplice di una funzionalità dal punto di vista dell’utente finale. Si concentra sul cosa l’utente vuole ottenere e sul perché è importante per lui, senza entrare nei dettagli tecnici di come sarà implementata._

## Descrizione del problema

Esempio: Quando [contesto specifico], il sistema [descrizione del comportamento errato].  

_Spiegare in quali condizioni si verifica il problema: se la issue che si apre riguarda un problema generale, descriverlo senza fare riferimenti a un cliente specifico e fare attenzione a nascondere dettagli in eventuali screenshot allegati, perché le issue devono essere Pubbliche._

_Se non si riesce a descrivere il ticket in termini generali, inserire il ticket come _confidential_ (icona a forma di occhio)_

_N.B. Uno screenshot aiuta sempre moltissimo, ritagliare in modo da escludere header/footer e sbianchettare le parti che contengono eventuali dati personali._


## Come riprodurre il problema  

1. Vai su [pagina o funzionalità].  
2. Esegui [azione specifica].  
3. Nota che [descrizione del risultato inatteso]. 

_Una buona spiegazione risponde di solito alle tre domande:_
- _cosa hai fatto?_
- _cosa ti aspettavi accadesse?_
- _cosa è invece accaduto?_


## Criteri di accettazione 

- [ ] Il sistema deve [comportamento corretto 1].  
- [ ] [Comportamento corretto 2].  
- [ ] [Altri criteri, se necessari].  

_I criteri di accettazione sono un insieme di condizioni specifiche e verificabili che una funzionalità o una correzione deve soddisfare per essere considerata completata con successo e accettata. Servono a definire chiaramente cosa è richiesto e a evitare ambiguità nel processo di sviluppo._

## Test Cases  

- **Scenario 1**:  
  - [ ] **Dato che** [condizione iniziale], **quando** [azione], **allora** [risultato atteso].  
- **Scenario 2**:  
  - [ ] **Dato che** [condizione iniziale], **quando** [azione], **allora** [risultato atteso].  

