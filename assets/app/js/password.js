!function ($) {

  'use strict';

  const language = document.documentElement.lang.toString();
  const Password = function (element, options) {
    this.options = options;
    this.$element = $(element);
    this.template = `<div id="pswd_info" style="display: none">
                     <ul class="list-unstyled">
                     <li class="length"><i class="fa fa-times"></i> ${Translator.trans('security.password_min_length', {}, 'messages', language)}</li>
                     <li class="number"><i class="fa fa-times"></i> ${Translator.trans('security.password_number_check', {}, 'messages', language)}</li>
                     <li class="capital"><i class="fa fa-times"></i> ${Translator.trans('security.password_upper_check', {}, 'messages', language)}</li>
                     <li class="special"><i class="fa fa-times"></i> ${Translator.trans('security.password_special_check', {}, 'messages', language)}</li>
                     <li></li>
                     <li class="meter"></li>
                     </ul>
                     </div>`;
    this.isShown = false;
    this.$infoContainer = $(this.template).appendTo(this.$element.parent());

    this.init();
  };

  Password.DEFAULTS = {
    message: 'Mostra/nascondi password',
    strengthMeter: true,
    validCssClass: 'text-success',
    invalidCssClass: 'text-error',
    minLength: 8,
    infoContainer: '#pswd_info',
    hierarchy: {
      '0': ['text-danger', 'Valutazione della complessità: pessima'],
      '10': ['text-danger', 'Valutazione della complessità: molto debole'],
      '20': ['text-warning', 'Valutazione della complessità: debole'],
      '30': ['text-info', 'Valutazione della complessità: buona'],
      '40': ['text-success', 'Valutazione della complessità: molto buona'],
      '50': ['text-success', 'Valutazione della complessità: ottima']
    }
  };

  Password.prototype.init = function () {
    const self = this;

    if (this.options.strengthMeter) {
      self.$infoContainer.parent().css('position', 'relative');

      this.$element.strengthMeter('text', {
        container: self.$infoContainer.find('.meter'),
        hierarchy: self.options.hierarchy
      });

      const setValid = function ($element) {
        $element.addClass(self.options.validCssClass).removeClass(self.options.invalidCssClass);
        $element.find('i').removeClass('fa-times').addClass('fa-check');
      };

      const setInvalid = function ($element) {
        $element.removeClass(self.options.validCssClass).addClass(self.options.invalidCssClass);
        $element.find('i').addClass('fa-times').removeClass('fa-check');
      };

      const validate = function () {
        const pswd = self.$element.val();
        if (pswd.length < self.options.minLength) {
          setInvalid(self.$infoContainer.find('.length'));
        } else {
          setValid(self.$infoContainer.find('.length'));
        }
        if (pswd.match(/[@$!%*#?&]/)) {
          setValid(self.$infoContainer.find('.special'));
        } else {
          setInvalid(self.$infoContainer.find('.special'));
        }
        if (pswd.match(/[A-Z]/)) {
          setValid(self.$infoContainer.find('.capital'));
        } else {
          setInvalid(self.$infoContainer.find('.capital'));
        }
        if (pswd.match(/\d/)) {
          setValid(self.$infoContainer.find('.number'));
        } else {
          setInvalid(self.$infoContainer.find('.number'));
        }
      };

      this.$element.keyup(function () {
        validate();
      }).change(function () {
          validate();
      }).focus(function () {
        self.$infoContainer.show();
      }).blur(function () {
        self.$infoContainer.hide();
      });
    }
  };


  const old = $.fn.password;

  $.fn.password = function (option, _relatedTarget) {
    return this.each(function () {
      let $this = $(this),
        data = $this.data('bs.password'),
        options = $.extend({}, Password.DEFAULTS, $this.data(), typeof option === 'object' && option);

      if (!data) {
        $this.data('bs.password', (data = new Password(this, options)));
      }

      if (typeof option === 'string') {
        data[option](_relatedTarget);
      }
    });
  };

  $.fn.password.Constructor = Password;

  $.fn.password.noConflict = function () {
    $.fn.password = old;
    return this;
  };

}(window.jQuery);
