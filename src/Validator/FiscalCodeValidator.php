<?php

namespace App\Validator;

use CodiceFiscale\Validator;
use DateTime;

class FiscalCodeValidator extends Validator
{
  public function isOlderThan18(): bool
  {
    $birthDate = $this->getBirthDate();
    if (!$birthDate) {
      return false; // Invalid fiscal code
    }

    $today = new DateTime();
    $age = $today->diff($birthDate)->y;

    return $age >= 18;
  }

}
