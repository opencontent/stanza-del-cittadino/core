<?php

declare(strict_types=1);

namespace App\Doctrine\DBAL;

use Doctrine\DBAL\Driver\Connection as DriverConnection;

interface ConnectionInterface extends DriverConnection {}
