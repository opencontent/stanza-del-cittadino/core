<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\NotificationSetting;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class NotificationSettingTest extends TestCase
{
  public function testConstructorInitializesDefaults(): void
  {
    $notificationSetting = new NotificationSetting();

    $this->assertInstanceOf(UuidInterface::class, $notificationSetting->getId());
    $this->assertTrue($notificationSetting->getEmailNotification());
  }

  public function testSetAndGetOwner(): void
  {
    $notificationSetting = new NotificationSetting();
    $owner = $this->createMock(User::class);

    $notificationSetting->setOwner($owner);
    $this->assertSame($owner, $notificationSetting->getOwner());
  }

  public function testSetAndGetObjectType(): void
  {
    $notificationSetting = new NotificationSetting();
    $objectType = NotificationSetting::NOTIFICATION_SETTING_CALENDAR_TYPE;

    $notificationSetting->setObjectType($objectType);
    $this->assertEquals($objectType, $notificationSetting->getObjectType());
  }

  public function testSetAndGetObjectId(): void
  {
    $notificationSetting = new NotificationSetting();
    $objectId = '123456';

    $notificationSetting->setObjectId($objectId);
    $this->assertEquals($objectId, $notificationSetting->getObjectId());
  }

  public function testSetAndGetEmailNotification(): void
  {
    $notificationSetting = new NotificationSetting();
    $this->assertTrue($notificationSetting->getEmailNotification());

    $notificationSetting->setEmailNotification(false);
    $this->assertFalse($notificationSetting->getEmailNotification());

    $notificationSetting->setEmailNotification(true);
    $this->assertTrue($notificationSetting->getEmailNotification());
  }

  public function testUUIDGenerationOnConstruct(): void
  {
    $notificationSetting1 = new NotificationSetting();
    $notificationSetting2 = new NotificationSetting();

    $this->assertNotNull($notificationSetting1->getId());
    $this->assertNotNull($notificationSetting2->getId());
    $this->assertNotEquals($notificationSetting1->getId(), $notificationSetting2->getId());

    // Ensure ID is a valid UUID
    $this->assertTrue(Uuid::isValid($notificationSetting1->getId()->toString()));
    $this->assertTrue(Uuid::isValid($notificationSetting2->getId()->toString()));
  }
}
