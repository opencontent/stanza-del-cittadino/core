<?php


namespace App\Form\Api;

use App\Entity\CodeGenerationStrategy;
use App\Utils\DateTimeUtils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CodeGenerationStrategyApiType extends AbstractType
{


  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('name', TextType::class, [
        'label' => 'general.nome',
        'required' => true
      ])
      ->add('prefix', TextType::class, [
        'label' => 'code_generation.strategies.prefix',
        'required' => false
      ])
      ->add('postfix', TextType::class, [
        'label' => 'code_generation.strategies.postfix',
        'required' => false
      ])
      ->add('counter_digit_length', NumberType::class, [
        'label' => 'code_generation.strategies.counter_digit_length',
        'attr' => ['min' => 1, 'max' => 9],
        'help' => 'code_generation.strategies.counter_digit_length_help',
        'required' => true
      ])
      ->add('temporal_reset', ChoiceType::class, [
        'label' => 'code_generation.strategies.temporal_reset',
        'required' => true,
        'help' => 'code_generation.strategies.temporal_reset_help',
        'choices' => [
          'code_generation.strategies.temporal_reset_choices.none' => DateTimeUtils::PERIOD_NONE,
          'code_generation.strategies.temporal_reset_choices.daily' => DateTimeUtils::PERIOD_DAILY,
          'code_generation.strategies.temporal_reset_choices.weekly' => DateTimeUtils::PERIOD_WEEKLY,
          'code_generation.strategies.temporal_reset_choices.monthly' => DateTimeUtils::PERIOD_MONTHLY,
          'code_generation.strategies.temporal_reset_choices.yearly' => DateTimeUtils::PERIOD_YEARLY,
        ]
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => CodeGenerationStrategy::class,
      'csrf_protection' => false
    ));
  }
}
