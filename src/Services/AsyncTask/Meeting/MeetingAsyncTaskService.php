<?php


namespace App\Services\AsyncTask\Meeting;


use App\Entity\Meeting;
use App\Entity\ScheduledAction;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use App\Services\InstanceService;
use App\Services\MailerService;
use App\Services\ScheduleActionService;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class MeetingAsyncTaskService implements ScheduledActionHandlerInterface
{

  public const SCHEDULED_MEETING_REMINDER = 'meeting_reminder';
  public const SCHEDULED_MEETING_DELETE_CANCELLED = 'meeting_delete_cancelled';
  private ScheduleActionService $scheduleActionService;
  private EntityManagerInterface $entityManager;
  private TranslatorInterface $translator;
  private Environment $templating;
  private InstanceService $instanceService;
  private MailerService $mailer;
  private string $defaultSender;
  private LoggerInterface $logger;


  /**
   * ReminderService constructor.
   * @param ScheduleActionService $scheduleActionService
   * @param EntityManagerInterface $entityManager
   * @param TranslatorInterface $translator
   * @param Environment $templating
   * @param InstanceService $instanceService
   * @param MailerService $mailer
   * @param LoggerInterface $logger
   * @param string $defaultSender
   */
  public function __construct(
    ScheduleActionService     $scheduleActionService,
    EntityManagerInterface    $entityManager,
    TranslatorInterface       $translator,
    Environment               $templating,
    InstanceService           $instanceService,
    MailerService             $mailer,
    LoggerInterface           $logger,
    string                    $defaultSender
  )
  {
    $this->scheduleActionService = $scheduleActionService;
    $this->entityManager = $entityManager;
    $this->translator = $translator;
    $this->templating = $templating;
    $this->instanceService = $instanceService;
    $this->mailer = $mailer;
    $this->defaultSender = $defaultSender;
    $this->logger = $logger;
  }


  /**
   * @param Meeting $meeting
   * @throws AlreadyScheduledException
   */
  public function createMeetingReminderAsync(Meeting $meeting): void
  {
    $now = new \DateTime();
    $reminderDate = $meeting->getReminderDate();
    if ($reminderDate > $now) {
      $params = serialize([
        'meeting' => $meeting->getId()
      ]);
      try {
        $this->logger->info('Meeting async task service: creating reminder async task ' . $meeting->getId(), [
          'now' => $now,
          'reminder_date' => $reminderDate,
        ]);

        $this->scheduleActionService->appendAction(
          'ocsdc.meeting.async_task',
          self::SCHEDULED_MEETING_REMINDER,
          $params,
          $reminderDate
        );
      } catch (\Exception $e) {
        $this->logger->error('Meeting async task service:' . $e->getMessage());
      }

    } else {
      $this->logger->info('Meeting async task service: creation of reminder async task form meeting '. $meeting->getId() .' skipped, reminder date is minor of current date ', [
        'now' => $now,
        'reminder_date' => $reminderDate,
      ]);
    }
  }

  /**
   * @param Meeting $meeting
   * @return void
   * @throws AlreadyScheduledException
   */
  public function createDeleteCancelledAsync(Meeting $meeting): void
  {
    try {
      $this->logger->info('Meeting async task service: creating delete cancelled async task ' . $meeting->getId());
      $executionDate = new \DateTime();
      $weeksInterval = DateInterval::createFromDateString('6 week');
      $executionDate->add($weeksInterval);
      $params = serialize([
        'meeting' => $meeting->getId()
      ]);
      $this->scheduleActionService->appendAction(
        'ocsdc.meeting.async_task',
        self::SCHEDULED_MEETING_DELETE_CANCELLED,
        $params,
        $executionDate
      );
    } catch (\Exception $e) {
      $this->logger->error('Meeting async task service:' . $e->getMessage());
    }
  }

  /**
   * @param ScheduledAction $action
   * @throws Exception
   */
  public function executeScheduledAction(ScheduledAction $action): void
  {
    $params = unserialize($action->getParams(), [self::class]);

    if ($action->getType() === self::SCHEDULED_MEETING_REMINDER) {
      /** @var Meeting $meeting */
      $meeting = $this->entityManager->getRepository(Meeting::class)->find($params['meeting']);
      if (!$meeting instanceof Meeting) {
        $this->scheduleActionService->markAsInvalid($action);
      }

      if ($meeting->getStatus() !== Meeting::STATUS_APPROVED) {
        $this->scheduleActionService->markAsDone($action);
        $this->logger->info('Meeting async task service: skipping meeting reminder for ' . $meeting->getId());
      } else {
        $this->logger->info('Meeting async task service: execute meeting reminder for ' . $meeting->getId());
        $this->sendPaymentReminder($meeting);
      }
    }

    if ($action->getType() === self::SCHEDULED_MEETING_DELETE_CANCELLED) {
      /** @var Meeting $meeting */
      $meeting = $this->entityManager->getRepository(Meeting::class)->find($params['meeting']);
      if (!$meeting instanceof Meeting) {
        $this->scheduleActionService->markAsInvalid($action);
      }

      if ($meeting->getStatus() === Meeting::STATUS_CANCELLED) {
        $this->deleteCancelledMeeting($meeting);
        $this->logger->info('Meeting async task service: delete cancelled meeting ' . $meeting->getId());
      } else {
        $this->scheduleActionService->markAsInvalid($action);
        $this->logger->info('Meeting async task service: skipping delete for ' . $meeting->getId());
      }
    }
  }

  public function deleteCancelledMeeting(Meeting $meeting): void
  {
    $this->entityManager->remove($meeting);
    $this->entityManager->flush();
  }

  /**
   * @param Meeting $meeting
   * @return void
   * @throws LoaderError
   * @throws RuntimeError
   * @throws SyntaxError
   */
  public function sendPaymentReminder(Meeting $meeting): void
  {

    if ($meeting->getCalendar()->getDisableNotifications()) {
      return;
    }


    if ($meeting->getEmail()) {

      $tenant = $this->instanceService->getCurrentInstance();
      $locale = $meeting->getLocale();
      $subject = $this->translator->trans('meetings.email.remind_meeting.subject', [], null, $locale);

      // Email nuova
      $message = $this->templating->render('Emails/Meeting/user/_remind_meeting.html.twig', [
        'meeting' => $meeting,
      ]);

      $this->mailer->dispatchMail(
        $this->defaultSender,
        $tenant->getName(),
        $meeting->getEmail(),
        $meeting->getName(),
        $message,
        $subject,
        $tenant,
        [],
        [],
        $meeting->getLocale()
      );
    }
  }
}
