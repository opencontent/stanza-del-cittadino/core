<?php


namespace App\ScheduledAction;


use App\Entity\Meeting;
use App\Entity\Pratica;
use App\Entity\ScheduledAction;
use App\Entity\Servizio;
use App\Entity\StatusChange;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use App\Services\InstanceService;
use App\Services\MailerService;
use App\Services\Manager\PraticaManager;
use App\Services\ModuloPdfBuilderService;
use App\Services\PraticaStatusService;
use App\Services\ScheduleActionService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class TacitOutcomeService implements ScheduledActionHandlerInterface
{

  public const SCHEDULED_APPLICATION_TACIT_OUTCOME = 'application_tacit_outcome';

  private ScheduleActionService $scheduleActionService;
  private EntityManagerInterface $entityManager;
  private TranslatorInterface $translator;
  private LoggerInterface $logger;
  private ModuloPdfBuilderService $moduloPdfBuilderService;
  private PraticaStatusService $praticaStatusService;


  /**
   * ReminderService constructor.
   * @param ScheduleActionService $scheduleActionService
   * @param EntityManagerInterface $entityManager
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   * @param ModuloPdfBuilderService $moduloPdfBuilderService
   * @param PraticaStatusService $praticaStatusService
   */
  public function __construct(
    ScheduleActionService   $scheduleActionService,
    EntityManagerInterface  $entityManager,
    TranslatorInterface     $translator,
    LoggerInterface         $logger,
    ModuloPdfBuilderService $moduloPdfBuilderService,
    PraticaStatusService    $praticaStatusService
  )
  {
    $this->scheduleActionService = $scheduleActionService;
    $this->entityManager = $entityManager;
    $this->translator = $translator;
    $this->logger = $logger;
    $this->moduloPdfBuilderService = $moduloPdfBuilderService;
    $this->praticaStatusService = $praticaStatusService;
  }


  /**
   * @param Pratica $application
   */
  public function createAsyncTask(Pratica $application): void
  {

    $now = new \DateTime();
    $tacitOutcomeDate = $application->getTacitOutcomeDate();

    if ($tacitOutcomeDate > $now) {
      $params = serialize([
        'application' => $application->getId()
      ]);
      try {
        $this->logger->info('Application tacit outcome: creating async task ' . $application->getId(), [
          'now' => $now,
          'tacit_outcome_date' => $tacitOutcomeDate,
        ]);

        $this->scheduleActionService->appendAction(
          'ocsdc.application.tacit_outcome',
          self::SCHEDULED_APPLICATION_TACIT_OUTCOME,
          $params,
          $tacitOutcomeDate
        );
      } catch (\Exception $e) {
        $this->logger->error('Application tacit outcome:' . $e->getMessage());
      }

    } else {
      $this->logger->info('Application tacit outcome: creation of async task for application ' . $application->getId() . ' skipped, tacit outcome date is minor of current date ', [
        'now' => $now,
        'tacit_outcome_date' => $tacitOutcomeDate,
      ]);
    }
  }

  /**
   * @param ScheduledAction $action
   * @throws Exception
   */
  public function executeScheduledAction(ScheduledAction $action): void
  {
    $params = unserialize($action->getParams(), [self::class]);
    if ($action->getType() === self::SCHEDULED_APPLICATION_TACIT_OUTCOME) {
      /** @var Pratica $application */
      $application = $this->entityManager->getRepository(Pratica::class)->find($params['application']);
      if (!$application instanceof Pratica) {
        $this->scheduleActionService->markAsInvalid($action);
      }

      $workflow = $application->getServizio()->getWorkflow();
      if (!in_array($workflow, [Servizio::WORKFLOW_SILENCE_CONSENT, Servizio::WORKFLOW_SILENCE_DENIAL], true)) {
        $this->scheduleActionService->markAsInvalid($action);
      }

      if ($application->isInFinalStates()) {
        $this->scheduleActionService->markAsDone($action);
        $this->logger->info('Application tacit outcome: skipping tacit outcome ' . $application->getId() . ' application is in final state' . $application->getStatusName());
      } else {
        $this->logger->info('Application tacit outcome: execute tacit outcome for ' . $application->getId());
        $this->tacitOutcome($application);
      }
    }
  }

  /**
   * @param Pratica $application
   * @return void
   * @throws Exception
   */
  private function tacitOutcome(Pratica $application): void
  {
    $workflow = $application->getServizio()->getWorkflow();
    if ($workflow === Servizio::WORKFLOW_SILENCE_CONSENT) {
      $newStatus = Pratica::STATUS_COMPLETE;
      $application->setEsito(true);
      $application->setMotivazioneEsito($this->translator->trans('pratica.tacit_outcome_consent'));
    } else {
      $newStatus = Pratica::STATUS_CANCELLED;
      $application->setEsito(false);
      $application->setMotivazioneEsito($this->translator->trans('pratica.tacit_outcome_denial'));
    }

    if ($application->getRispostaOperatore() === null) {
      $signedResponse = $this->moduloPdfBuilderService->createSignedResponseForPratica($application);
      $application->addRispostaOperatore($signedResponse);
    }

    $statusChange = new StatusChange();
    $statusChange->setEvento($application->getMotivazioneEsito());
    $this->praticaStatusService->setNewStatus(
      $application,
      $newStatus,
      $statusChange,
      true
    );
  }

}
