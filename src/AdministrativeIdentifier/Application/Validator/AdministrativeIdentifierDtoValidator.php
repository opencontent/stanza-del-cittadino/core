<?php

namespace App\AdministrativeIdentifier\Application\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdministrativeIdentifierDtoValidator
{
  private ValidatorInterface $validator;

  public function __construct(ValidatorInterface $validator)
  {
    $this->validator = $validator;
  }

  public function validate(object $dto): array
  {
    $violations = $this->validator->validate($dto);
    $errors = [];
    if (count($violations) > 0) {
      foreach ($violations as $violation) {
        $errors[$violation->getPropertyPath()] = $violation->getMessage();
      }
    }
    return $errors;
  }
}

