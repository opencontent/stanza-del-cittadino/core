<?php

namespace App\Utils;

class HaveIBeenPwned
{
  private const BASE_URI = 'https://api.pwnedpasswords.com';

  public static function idValid(string $password): bool
  {
    return self::countPwnedPassword($password) <= 0;
  }

  private static function countPwnedPassword(string $password): int
  {
    $hashedPassword = \strtoupper(\sha1($password));
    $firstFiveCharacters = \substr($hashedPassword, 0, 5);
    $hashes = self::request('/range/' . $firstFiveCharacters);

    foreach ($hashes as $line) {
      if (false !== strpos($line, ':')) {
        [$hash, $count] = \explode(':', $line);
        if ($firstFiveCharacters . \strtoupper($hash) === $hashedPassword) {
          return (int)$count;
        }
      }
    }

    return 0;
  }

  private static function request($path)
  {
    $url = self::BASE_URI . $path;
    $headers = [];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);

    $data = curl_exec($ch);

    if ($data === false) {
      $errorCode = curl_errno($ch) * -1;
      $errorMessage = curl_error($ch);
      curl_close($ch);
      throw new \Exception($errorMessage, $errorCode);
    }

    $info = curl_getinfo($ch);
    curl_close($ch);

    if ($info['download_content_length'] > 0) {
      $body = substr($data, -$info['download_content_length']);
    } else {
      $body = substr($data, $info['header_size']);
    }

    if ((int)$info['http_code'] > 299) {
      throw new \Exception("$url: Reponse code is " . $info['http_code'] . ' ' . json_encode($data));
    }

    $hashes = \str_replace("\r\n", \PHP_EOL, $body);
    return \explode(\PHP_EOL, $hashes);
  }
}
