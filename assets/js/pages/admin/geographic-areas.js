import '../../../styles/vendor/_leaflet.scss';
import {TextEditor} from "../../utils/TextEditor";
import Map from "../../utils/Map";
import Swal from 'sweetalert2/src/sweetalert2.js';
import "@sweetalert2/theme-bootstrap-4/bootstrap-4.min.css";
import tinymce from "tinymce";
const $language = document.documentElement.lang.toString();

$(document).ready(function () {
  const limitChars = 2000;
  TextEditor.init(
    (editor)  => {
      editor.on('input', function (event) {
        let numChars = tinymce.activeEditor.plugins.wordcount.body.getCharacterCount();
        //Update value
        let elm = event.srcElement.dataset.id
        $('.form-text text-muted').innerHTML = ''
        let html = `<small class='form-text text-muted'>${Translator.trans('servizio.max_limit_of', {}, 'messages', $language)} ${limitChars} ${Translator.trans('characters', {}, 'messages', $language)} (<span class='total-chars'>${numChars}</span> / <span class='max-chars'> ${limitChars} </span>)</small>`;
        $('.form-text.text-muted').remove()
        $('#'+elm).nextAll().after(html)
        //Check and Limit Characters
        if (numChars > limitChars) {
          event.preventDefault();
          return false;
        }
      });
    })
})


const $mapWrapper = $('#map');
const $geoFence = $('#geographic_area_geofence');
const center = [41.9027835, 12.4963655];
const $map = new Map($mapWrapper[0], center, {
  name: '#geographic_area_name_it',
  element: '#geographic_area_geofence'
});

const $importButton = $('#import-button');
const $importUrlField = $('#import-url-field');
//const $importDataField = $('#import-data-field');

if ($geoFence.val()) {
  $map.addGeojsonFeatures(JSON.parse($geoFence.val()));
}

$map.addDrawingLayer({
  nameEl: '#geographic_area_name_it',
  targetEl: '#geographic_area_geofence'
});

$importButton.on('click', function (e) {
  e.preventDefault();

  if (!$importUrlField.val()) {
    Swal.fire(
      'Attenzione!',
      'Devi inserire un valore valido nel campo url effettuare un import',
      'warning'
    );
  }

  $map.loadGeoJsonFromUrl($importUrlField.val());

})
