<?php

namespace App\EventListener;

use App\Entity\CPSUser;
use App\Services\TermsAcceptanceCheckerService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class TermsAcceptListener
{

  private RouterInterface $router;

  private TokenStorageInterface $tokenStorage;

  private TermsAcceptanceCheckerService $termsAcceptanceChecker;

  private array $termsAcceptRoutes = [
    'user_dashboard',
    'user_profile',
    'pratiche',
    'allegati_list_cpsuser',
    'folders_list_cpsuser',
    'subscriptions_list_cpsuser',
    'payments_list_cpsuser',
  ];

  public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage, TermsAcceptanceCheckerService $termsAcceptanceChecker)
  {
    $this->router = $router;
    $this->tokenStorage = $tokenStorage;
    $this->termsAcceptanceChecker = $termsAcceptanceChecker;
  }


  public function onKernelRequest(RequestEvent $event): void
  {

    $currentRoute = $event->getRequest()->get('_route');
    if (!in_array($currentRoute, $this->termsAcceptRoutes, true)){
      return;
    }

    $user = $this->getUser();
    if (!empty($currentRoute) && $currentRoute !== 'terms_accept' && $user instanceof CPSUser && !$user->isAnonymous() && !$this->termsAcceptanceChecker->checkIfUserHasAcceptedMandatoryTerms($user)) {
      $redirectParameters = ['r' => $event->getRequest()->getUri()];
      $redirectUrl = $this->router->generate('terms_accept', $redirectParameters);
      $event->setResponse(new RedirectResponse($redirectUrl));
    }
  }

  protected function getUser(): ?UserInterface
  {
    if (null === $token = $this->tokenStorage->getToken()) {
      return null;
    }

    if (!is_object($user = $token->getUser())) {
      return null;
    }

    return $user;
  }
}
