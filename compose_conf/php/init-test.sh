#!/bin/bash

[[ $DEBUG == 1 ]] && set -x

echo "==> Checking database availability"
#wait-for-it ${DB_HOST}:${DB_PORT}

# Rimuovo '-i comune di bugliano' perché i database è impostato nel file config/packages/test/doctrine.yaml
# Facendo così diminuiscono di molto gli errori durante creazione database e migrazioni

echo "==> Crate database"
bin/console doctrine:database:create -e test --if-not-exists
echo "==> Running migrations"
bin/console doctrine:migrations:migrate -e test --no-interaction

echo "==> First-time instance initialization"
echo " * admin user creation on comune-di-bugliano"
bin/console ocsdc:configure-instance -e test -i "comune-di-bugliano" --no-interaction --name="comune-di-bugliano" --code_adm="C_123" --siteurl="https://comune-di-test.it" --admin_name="Amministratore" --admin_lastname="Servizi" --admin_email="admin@localtest.me" --admin_username="admin" --admin_password="changeme"
