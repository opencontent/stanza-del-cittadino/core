<?php

namespace App\Security\OAuth;

use League\OAuth2\Client\Provider\AbstractProvider;

interface ProviderFactoryInterface
{
  public function getConfiguration(): ConfigurationInterface;

  public function getProviderIdentifier(): string;

  public function instanceProvider(): AbstractProvider;
}
