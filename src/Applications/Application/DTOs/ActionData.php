<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

class ActionData
{
  /**
   * @OA\Property(type="string", description="The action name")
   * @Serializer\Groups({"read", "write"})
   */
  public string $action;
  /**
   * @OA\Property(type="string", description="The action url")
   * @Serializer\Groups({"read", "write"})
   */
  public string $url;
}
