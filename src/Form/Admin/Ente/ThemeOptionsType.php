<?php


namespace App\Form\Admin\Ente;


use App\Model\DefaultProtocolSettings;
use App\Model\Mailer;
use App\Model\Tenant\ThemeOptions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ThemeOptionsType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('identifier', ChoiceType::class, [
        'label' => 'ente.theme_options.theme',
        'required' => true,
        'choices' => ThemeOptions::THEMES,
        'empty_data' => ThemeOptions::DEFAULT_THEME,
        'attr' => ['class' => 'theme-option'],
      ])
      ->add('light_top_header', CheckboxType::class, [
        'label' => 'ente.theme_options.light_top_header',
        'required' => false,
        'attr' => ['class' => 'theme-option'],
      ])
      ->add('light_center_header', CheckboxType::class, [
        'label' => 'ente.theme_options.light_center_header',
        'required' => false,
        'attr' => ['class' => 'theme-option'],
      ])
      ->add('light_navbar_header', CheckboxType::class, [
        'label' => 'ente.theme_options.light_navbar_header',
        'required' => false,
        'attr' => ['class' => 'theme-option'],
      ])
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => ThemeOptions::class,
      'csrf_protection' => false
    ));
  }
}
