<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Exception;
use App\DataFixtures\ORM\LoadData;
use App\Dto\Admin;
use App\Entity\AdminUser;
use App\Entity\OperatoreUser;
use App\Entity\User;
use App\Utils\StringUtils;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PropertyAccess\PropertyAccess;

class InvalidateUserPasswordCommand extends Command
{

  protected static $defaultName = 'ocsdc:user-secure:invalidate-password';
  protected static $defaultDescription = 'Forces users that have a certain password to change it';
  private EntityManagerInterface $entityManager;

  private UserPasswordHasherInterface $passwordHasher;

  public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
  {
    $this->entityManager = $entityManager;
    parent::__construct();
    $this->passwordHasher = $passwordHasher;
  }

  protected function configure(): void
  {
    $this->addOption('run', null, InputOption::VALUE_NONE, 'Dry run')
      ->addOption('password', null, InputOption::VALUE_REQUIRED, 'The password to invalidate');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {

    $symfonyStyle = new SymfonyStyle($input, $output);
    try {
      $run = $input->getOption('run');
      $password = $input->getOption('password');

      $usersToUpdate = [];

      // Recupero gli admin da aggiornare
      $admins = $this->entityManager->getRepository(AdminUser::class)->findAll();
      foreach ($admins as $admin) {
        if ($this->passwordHasher->isPasswordValid($admin, $password)) {
          $id = (string)$admin->getId();
          $usersToUpdate [$id] = $admin->getUsername() . ' (Admin)';
        }
      }

      // Recupero gli operatori da aggiornare
      $operators = $this->entityManager->getRepository(OperatoreUser::class)->findAll();
      foreach ($operators as $operator) {
        if ($this->passwordHasher->isPasswordValid($operator, $password)) {
          $id = (string)$operator->getId();
          $usersToUpdate [$id] = $operator->getUsername() . ' (Operator)';
        }
      }

      $symfonyStyle->note('Users to update: ' . count($usersToUpdate));
      if ($usersToUpdate !== []) {
        $symfonyStyle->comment(implode(', ', $usersToUpdate));
      }

      if ($run) {
        $symfonyStyle->note('Run updates');
        $this->entityManager->createQuery("UPDATE ". User::class . " u SET u.lastChangePassword = '1970-01-01 01:00:00' WHERE u.id IN (:ids)")
          ->setParameter(':ids', array_keys($usersToUpdate), Connection::PARAM_STR_ARRAY)
          ->execute();
      }

    } catch (Exception $exception) {
      $symfonyStyle->error('Error: ' . $exception->getMessage());
      return (int) Command::FAILURE;
    }

    return (int) Command::SUCCESS;
  }
}
