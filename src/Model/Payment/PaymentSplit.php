<?php

namespace App\Model\Payment;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class PaymentSplit
{
  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment split code")
   * @Groups({"read", "write", "kafka"})
   */
  private $code;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Payment split amount")
   * @Groups({"read", "write", "kafka"})
   */
  private $amount;

  /**
   * @var array
   * @OA\Property(description="Payment split meta")
   * @Serializer\Type("array")
   * @Groups({"read", "write", "kafka"})
   */
  private $meta;

  /**
   * @return mixed
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * @param mixed $code
   */
  public function setCode($code): void
  {
    $this->code = $code;
  }

  /**
   * @return mixed
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * @param mixed $amount
   */
  public function setAmount($amount): void
  {
    $this->amount = $amount;
  }

  /**
   * @return array
   */
  public function getMeta(): array
  {
    return $this->meta;
  }

  /**
   * @param array $meta
   */
  public function setMeta(array $meta): void
  {
    $this->meta = $meta;
  }

  public static function init($code, $amount, $meta): self
  {
    $paymentSplit = new self();
    $paymentSplit->setCode($code);
    $paymentSplit->setAmount($amount);
    $paymentSplit->setMeta($meta);
    return $paymentSplit;
  }
}
