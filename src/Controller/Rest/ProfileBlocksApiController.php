<?php

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Entity\CPSUser;
use App\Entity\ProfileBlock;
use App\Model\Api\PagedList;
use App\Model\LinksPagedList;
use App\Model\MetaPagedList;
use App\Security\Voters\UserVoter;
use App\Utils\ApiUtils;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\NotFoundProblem;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ProfileBlocksApiController
 * @Route("/users")
 */
class ProfileBlocksApiController extends AbstractFOSRestController
{

  private EntityManagerInterface $entityManager;

  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  /**
   * List all user's profile blocks
   * @Rest\Get("/{id}/profile-blocks", name="profile_blocks_api_list")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *        name="id",
   *        in="path",
   *        @OA\Schema(
   *            type="string"
   *        ),
   *        required=true,
   *        description="Uuid of the user"
   *    )
   *
   * @OA\Parameter(
   *       in="query",
   *       name="keys",
   *       style="form",
   *       explode=false,
   *       required=false,
   *       description="List of keys to request profile block for.",
   *       @OA\Schema(
   *         type="array",
   *         @OA\Items(type="string")
   *       )
   *   )
   *
   * @OA\Parameter(
   *        name="offset",
   *        in="query",
   *        @OA\Schema(
   *            type="string"
   *        ),
   *        required=false,
   *        description="Offset of the query"
   *    )
   *
   * @OA\Parameter(
   *        name="limit",
   *        in="query",
   *        @OA\Schema(
   *            type="string"
   *        ),
   *        required=false,
   *        description="Limit of the query"
   *    )
   *
   * @OA\Response(
   *      response=200,
   *      description="Retrieve list of profile blocks",
   *      @OA\JsonContent(
   *         type="object",
   *         @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *         @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *         @OA\Property(property="data", type="array",
   *            @OA\Items(ref=@Model(type=ProfileBlock::class, groups={"read"}))
   *         )
   *      )
   *   )
   *
   * @OA\Response(
   * response=401,
   * description="Access denied"
   * )
   *
   * @OA\Response(
   * response=400,
   * description="Bad request"
   * )
   *
   * @OA\Response(
   * response=404,
   * description="Not found"
   * )
   *
   * @OA\Tag(name="users")
   * @param Request $request
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function getProfileBlocksAction(Request $request, $id): View
  {

    try {
      $offset = $request->query->getInt('offset');
      $limit = $request->query->getInt('limit', 10);
      $keysParameter = $request->get('keys', false);

      if ($limit > 100) {
        throw new ApiProblemException(
          new BadRequestProblem("Limit parameter is too high, max accepted value is 100.")
        );
      }

      $user = $this->fetchUser($id);
      $this->denyAccessUnlessGranted(UserVoter::VIEW, $user);

      $parameters = ['id' => $id, 'offset' => $offset, 'limit' => $limit];
      $criteria = ['user_id' => $id];
      if ($keysParameter) {
        $parameters['keys'] = $keysParameter;
        $criteria['keys'] = explode(',', $keysParameter);
      }


      $profileBlockRepository = $this->entityManager->getRepository(ProfileBlock::class);
      $meta = new MetaPagedList();
      $meta->setParameter($parameters);
      $meta->setCount($profileBlockRepository->getProfileBlocks($criteria, true));
      $links = new LinksPagedList();
      $self = $this->generateUrl('profile_blocks_api_list', $meta->getParameter(), UrlGeneratorInterface::ABSOLUTE_URL);
      $links->setSelf($self);
      $result = new PagedList($meta, $links, $profileBlockRepository->getProfileBlocks($criteria, false, 'updatedAt', 'DESC', $offset, $limit));

      return $this->view($result, Response::HTTP_OK);

    } catch (ApiProblemException $e) {
      $this->logger->error('profile_blocks_api_list', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('profile_blocks_api_list', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => AbstractApiController::MESSAGE_INTERNAL_SERVER_ERROR]));
    }
  }

  /**
   * Delete a profile block entry, a profile block entry can only be deleted by the owner
   * @Rest\Delete("/{id}/profile-blocks/{profile_block_id}", name="profile_block_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="users")
   *
   * @param $id
   * @param $profile_block_id
   * @return View
   * @throws ApiProblemException
   */
  public function deleteProfileBlockAction($id, $profile_block_id): View
  {

    try {

      $user = $this->fetchUser($id);
      $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

      $repository = $this->entityManager->getRepository(ProfileBlock::class);
      $item = $repository->find($profile_block_id);

      if (!$item instanceof ProfileBlock) {
        throw new ApiProblemException(
          new NotFoundProblem("Profile block with id {$profile_block_id} not found")
        );
      }

      $this->entityManager->remove($item);
      $this->entityManager->flush();
      return $this->view(null, Response::HTTP_NO_CONTENT);

    } catch (ApiProblemException $e) {
      $this->logger->error('profile_block_api_delete', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('profile_block_api_delete', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  /**
   * @throws ApiProblemException
   */
  private function fetchUser(string $id): CPSUser
  {
    ApiUtils::isValidUuid($id);

    $repository = $this->entityManager->getRepository(CPSUser::class);
    $user = $repository->find($id);
    if (!$user instanceof CPSUser) {
      throw new ApiProblemException(
        new NotFoundProblem("User {$id} not found")
      );
    }

    return $user;
  }

}
