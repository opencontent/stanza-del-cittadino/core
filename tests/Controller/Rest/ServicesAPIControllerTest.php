<?php

declare(strict_types=1);

namespace Tests\Controller\Rest;

use App\Services\InstanceService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\Helpers\ApiHelper;

class ServicesAPIControllerTest extends KernelTestCase
{
  protected ?string $token = null;

  /**
   * @throws GuzzleException
   */
  protected function setUp(): void
  {
    parent::setUp();
    $this->token = ApiHelper::getAdminJwtToken();

  }

  /**
   * @throws GuzzleException
   */
  public function testPostServiceAction()
  {
    self::bootKernel();
    $instanceService = self::$container->get(InstanceService::class);

    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];


    $data = [
      'name' => 'test' . date('Y-m-d H:i:s'),
      'tenant' => '9ac1109c-558e-456d-a2f9-e3aa587ca8c1',
      'topics' => 'asd',
      'short_description' => 'Lorem ipsum dolor sit amet',
      'description' => 'Lorem ipsum dolor sit amet',
      'howto' => '<p>Lorem ipsum dolor sit amet</p>',
      'who' => '<p>Lorem ipsum dolor sit amet</p>',
      'special_cases' => '',
      'more_info' => '<p>Lorem ipsum dolor sit amet</p>',
      'compilation_info' => '',
      'final_indications' => 'Lorem ipsum dolor sit amet',
      'coverage' => null,
      'response_type' => null,
      'flow_steps' => [
        [
          'identifier' => '5f47883304fc3d0020d84f49',
          'title' => null,
          'type' => 'formio',
          'description' => null,
          'guide' => null,
          'parameters' => [
            'formio_id' => '5f47883304fc3d0020d84f49',
          ],
        ],
      ],
      'io_parameters' => null,
      'protocol_required' => true,
      'protocollo_parameters' => null,
      'payment_required' => false,
      'payment_parameters' => null,
      'sticky' => true,
      'status' => 1,
      'access_level' => 3000,
      'login_suggested' => false,
      'scheduled_from' => null,
      'scheduled_to' => null,
      'service_group' => null,
      'allow_reopening' => false,
      'workflow' => 0,
      'integrations' => [
        'trigger' => '1900',
        'action' => 'subscriptions',
      ],
      'recipients_id' => [
        '245255ec-f1f1-4f9a-ba95-eabfc8110096',
        '848ebf79-355c-48b6-9ab8-3dc40ff136c7',
      ],
    ];

    $request = new Request(
      'POST',
      ApiHelper::API_BASE_URL . '/api/services',
      $headers,
      json_encode($data)
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    $responseData = json_decode($response->getBody()->getContents(), true);
    $this->assertArrayHasKey('id', $responseData);

    return $responseData['id'];
  }
}
