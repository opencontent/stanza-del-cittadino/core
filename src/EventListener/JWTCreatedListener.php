<?php

namespace App\EventListener;

use App\Services\InstanceService;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User as CoreUser;

class JWTCreatedListener
{

  /**
   * @var InstanceService
   */
  private InstanceService $instanceService;
  private string $stackIdentifier;
  private RouterInterface $router;

  /**
   * @param InstanceService $instanceService
   * @param string $stackIdentifier
   */
  public function __construct(InstanceService $instanceService, RouterInterface $router, string $stackIdentifier)
  {
    $this->instanceService = $instanceService;

    $this->stackIdentifier = $stackIdentifier;
    $this->router = $router;
  }

  /**
   * @param JWTCreatedEvent $event
   *
   * @return void
   */
  public function onJWTCreated(JWTCreatedEvent $event): void
  {
    // Aggiungo il kid, server per la validazione del token tramite jwks
    $header = $event->getHeader();
    $header['kid'] = $this->stackIdentifier;
    $event->setHeader($header);

    $payload = $event->getData();
    // Se ho richiesto il jwt per un utente in memory non ho l'id
    if ($event->getUser() instanceof CoreUser\User) {
      $payload['id'] = Uuid::uuid4();
    } else {
      $payload['id'] = $event->getUser()->getId();
    }
    $payload['tenant_id'] = $this->instanceService->getCurrentInstance()->getId();

    // Todo: creare una rotta adhoc o inserire in metodo nel InstancesProvider
    $issuer = str_replace('/metrics', '', $this->router->generate('sdc_metrics', [], UrlGeneratorInterface::ABSOLUTE_URL));
    $payload['iss'] = $issuer;

    $event->setData($payload);
  }
}
