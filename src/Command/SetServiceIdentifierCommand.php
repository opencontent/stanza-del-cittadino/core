<?php

namespace App\Command;

use App\Entity\Servizio;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetServiceIdentifierCommand extends Command
{
  protected static $defaultName = 'ocsdc:services:set-identifier';
  protected static $defaultDescription = 'Set the identifier of a service, if identifier is already set on an another service this service will be updated with null identifier.';

  private EntityManagerInterface $entityManager;

  protected function configure(): void
  {
    $this->addArgument('identifier', InputArgument::REQUIRED, 'The identifier to set, if null is passed as identifier the target service will be updated with null identifier');
    $this->addArgument('service-id', InputArgument::REQUIRED, 'The uuid of the target service');
  }

  public function __construct(EntityManagerInterface $entityManager)
  {
    parent::__construct();
    $this->entityManager = $entityManager;
  }

  /**
   * @throws Exception
   */
  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $io = new SymfonyStyle($input, $output);
    $io->title('Setting service identifier');
    $identifier = $input->getArgument('identifier');
    $serviceId = $input->getArgument('service-id');


    $repository = $this->entityManager->getRepository(Servizio::class);

    if (!Uuid::isValid($serviceId)) {
      $io->error("Service id " . $serviceId . " must is a valid UUID.");
      return Command::FAILURE;
    }

    $targetService = $repository->find($serviceId);
    if (!$targetService instanceof Servizio) {
      $io->error("Service with uuid " . $serviceId . " does not exist.");
      return Command::FAILURE;
    }

    $serviceWithIdentifier = $repository->findOneBy(['identifier' => $identifier]);
    if ($serviceWithIdentifier instanceof Servizio) {
      $io->info("Identifier already set on this service: " . $serviceWithIdentifier->getName() . " that will be updated to null.");

      $serviceWithIdentifier->setIdentifier(null);
      $this->entityManager->persist($serviceWithIdentifier);
    }

    if (empty($identifier) || strtolower($identifier) === 'null') {
      $identifier = null;
    }
    $targetService->setIdentifier($identifier);
    $this->entityManager->persist($targetService);
    $this->entityManager->flush();

    $io->success(sprintf('Service %s succesfully updated with identifier %s', $targetService->getName(), $identifier));
    return Command::SUCCESS;
  }
}
