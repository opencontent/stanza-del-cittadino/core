<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Calendar;
use App\Entity\Meeting;
use App\Entity\OpeningHour;
use App\Entity\Pratica;
use App\Entity\CPSUser;
use DateTime;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class MeetingTest extends TestCase
{
  private Meeting $meeting;

  protected function setUp(): void
  {
    $this->meeting = new Meeting();
  }

  public function testMeetingIdIsGenerated(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->meeting->getId());
  }

  public function testSetGetCalendar(): void
  {
    $calendar = $this->createMock(Calendar::class);
    $this->meeting->setCalendar($calendar);
    $this->assertSame($calendar, $this->meeting->getCalendar());
  }

  public function testSetGetOpeningHour(): void
  {
    $openingHour = $this->createMock(OpeningHour::class);
    $this->meeting->setOpeningHour($openingHour);
    $this->assertSame($openingHour, $this->meeting->getOpeningHour());
  }

  public function testSetGetEmail(): void
  {
    $email = 'test@example.com';
    $this->meeting->setEmail($email);
    $this->assertSame($email, $this->meeting->getEmail());
  }

  public function testSetGetPhoneNumber(): void
  {
    $phoneNumber = '1234567890';
    $this->meeting->setPhoneNumber($phoneNumber);
    $this->assertSame($phoneNumber, $this->meeting->getPhoneNumber());
  }

  public function testSetGetFiscalCode(): void
  {
    $fiscalCode = '1234567890123456';
    $this->meeting->setFiscalCode($fiscalCode);
    $this->assertSame($fiscalCode, $this->meeting->getFiscalCode());
  }

  public function testSetGetName(): void
  {
    $name = 'John Doe';
    $this->meeting->setName($name);
    $this->assertSame($name, $this->meeting->getName());
  }

  public function testSetGetUser(): void
  {
    $user = $this->createMock(CPSUser::class);
    $this->meeting->setUser($user);
    $this->assertSame($user, $this->meeting->getUser());
  }

  public function testSetGetFromTime(): void
  {
    $fromTime = new DateTime('now');
    $this->meeting->setFromTime($fromTime);
    $this->assertSame($fromTime, $this->meeting->getFromTime());
  }

  public function testSetGetToTime(): void
  {
    $toTime = new DateTime('tomorrow');
    $this->meeting->setToTime($toTime);
    $this->assertSame($toTime, $this->meeting->getToTime());
  }

  public function testIsOutdated(): void
  {
    $pastTime = new DateTime('yesterday');
    $this->meeting->setToTime($pastTime);
    $this->assertTrue($this->meeting->isOutdated());

    $futureTime = new DateTime('tomorrow');
    $this->meeting->setToTime($futureTime);
    $this->assertFalse($this->meeting->isOutdated());
  }

  public function testSetGetStatus(): void
  {
    $status = Meeting::STATUS_APPROVED;
    $this->meeting->setStatus($status);
    $this->assertSame($status, $this->meeting->getStatus());
  }

  public function testSetGetUserMessage(): void
  {
    $userMessage = 'This is a user message.';
    $this->meeting->setUserMessage($userMessage);
    $this->assertSame($userMessage, $this->meeting->getUserMessage());
  }

  public function testSetGetReason(): void
  {
    $reason = 'Reason for meeting.';
    $this->meeting->setReason($reason);
    $this->assertSame($reason, $this->meeting->getReason());
  }

  public function testSetGetMotivationOutcome(): void
  {
    $motivationOutcome = 'Outcome of the meeting.';
    $this->meeting->setMotivationOutcome($motivationOutcome);
    $this->assertSame($motivationOutcome, $this->meeting->getMotivationOutcome());
  }

  public function testSetGetVideoconferenceLink(): void
  {
    $videoconferenceLink = 'https://example.com/meeting';
    $this->meeting->setVideoconferenceLink($videoconferenceLink);
    $this->assertSame($videoconferenceLink, $this->meeting->getVideoconferenceLink());
  }

  public function testSetGetRescheduled(): void
  {
    $rescheduled = 2;
    $this->meeting->setRescheduled($rescheduled);
    $this->assertSame($rescheduled, $this->meeting->getRescheduled());
  }

  public function testAddGetApplications(): void
  {
    $application = $this->createMock(Pratica::class);
    $this->meeting->addApplication($application);
    $this->assertContains($application, $this->meeting->getApplications());
  }

  public function testGetCalendarInfo(): void
  {
    $calendar = $this->createMock(Calendar::class);
    $calendar->method('getId')->willReturn(Uuid::uuid4());
    $calendar->method('getTitle')->willReturn('Test Calendar');

    $this->meeting->setCalendar($calendar);
    $calendarInfo = $this->meeting->getCalendarInfo();

    $this->assertArrayHasKey('id', $calendarInfo);
    $this->assertArrayHasKey('title', $calendarInfo);
    $this->assertEquals($calendar->getId(), $calendarInfo['id']);
    $this->assertEquals('Test Calendar', $calendarInfo['title']);
  }

  public function testGetApplicationId(): void
  {
    $application = $this->createMock(Pratica::class);
    $application->method('getId')->willReturn('application_id');

    $this->meeting->addApplication($application);
    $this->assertEquals('application_id', $this->meeting->getApplicationId());
  }

  public function testGetUserId(): void
  {
    $user = $this->createMock(CPSUser::class);
    $user->method('getId')->willReturn('user_id');

    $this->meeting->setUser($user);
    $this->assertEquals('user_id', $this->meeting->getUserId());
  }

  public function testGetExpirationTime(): void
  {
    $draftExpiration = new DateTime('tomorrow');
    $this->meeting->setDraftExpiration($draftExpiration);
    $this->assertSame($draftExpiration, $this->meeting->getExpirationTime());
  }

  public function testGetFirstAvailableSlot(): void
  {
    $startTime = new DateTime('10:00');
    $endTime = new DateTime('11:00');

    $this->meeting->setFirstAvailableStartTime($startTime);
    $this->meeting->setFirstAvailableEndTime($endTime);

    $this->assertEquals('10:00-11:00', $this->meeting->getFirstAvailableSlot());
  }

  public function testGetStatusName(): void
  {
    $this->meeting->setStatus(Meeting::STATUS_APPROVED);
    $this->assertEquals('status_approved', $this->meeting->getStatusName());
  }
}
