<?php

namespace App\Security\OAuth;

trait ConfigurationAwareTrait
{
  protected ConfigurationInterface $configuration;

  public function getConfiguration(): ConfigurationInterface
  {
    return $this->configuration;
  }

  public function setConfiguration(ConfigurationInterface $configuration): void
  {
    $this->configuration = $configuration;
  }
}
