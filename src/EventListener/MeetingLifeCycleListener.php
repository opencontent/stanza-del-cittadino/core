<?php


namespace App\EventListener;


use App\Entity\Meeting;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\Services\AsyncTask\Meeting\MeetingAsyncTaskService;
use App\Services\MeetingService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Twig\Error\Error;

class MeetingLifeCycleListener
{
  private MeetingService $meetingService;
  private TranslatorInterface $translator;
  private MeetingAsyncTaskService $meetingAsyncTaskService;
  private LoggerInterface $logger;

  public function __construct(MeetingService $meetingService, TranslatorInterface $translator, MeetingAsyncTaskService $meetingAsyncTaskService, LoggerInterface $logger)
  {
    $this->meetingService = $meetingService;
    $this->translator = $translator;
    $this->meetingAsyncTaskService = $meetingAsyncTaskService;
    $this->logger = $logger;
  }

  /**
   * Set createdAt and UpdatedAt and openingHour
   *
   * @param LifecycleEventArgs $args
   * @throws \Exception
   */
  public function prePersist(LifecycleEventArgs $args): void
  {
    $meeting = $args->getObject();
    if ($meeting instanceof Meeting) {
      $errors = $this->meetingService->getMeetingErrors($meeting);
      if (!empty($errors)) {
        throw new ValidatorException($this->translator->trans('meetings.error.invalid_meeting') . ': ' . implode(', ', $errors));
      }
      $meeting->setRescheduled(0);
    }
  }

  /**
   * Sends email to citizen, calendar's moderators and calendar's contact when a new meeting is created
   *
   * @param LifecycleEventArgs $args
   * @throws Error
   */
  public function postPersist(LifecycleEventArgs $args): void
  {
    $meeting = $args->getObject();
    if ($meeting instanceof Meeting) {

      if ($meeting->getStatus() !== Meeting::STATUS_DRAFT) {
        $this->meetingService->sendEmailNewMeeting($meeting);
      }

      try {
        $this->logger->info('Meeting reminder service: cal on post persist on meeting ' . $meeting->getId() . 'with status ' . $meeting->getStatus());
        if ($meeting->getStatus() === Meeting::STATUS_APPROVED) {
          $this->meetingAsyncTaskService->createMeetingReminderAsync($meeting);
        }
      } catch (AlreadyScheduledException $exception) {
        $this->logger->error('Meeting reminder service: error creating async task for meeting ' . $meeting->getId() . 'with status ' . $meeting->getStatus());
      }
    }

  }

  /**
   * Sends email when meeting changes
   * @param PreUpdateEventArgs $args
   * @throws Error
   * @throws \Exception
   */
  public function preUpdate(PreUpdateEventArgs $args): void
  {
    $meeting = $args->getObject();
    if ($meeting instanceof Meeting) {

      if ($meeting->getAnonymizedAt()) {
        return;
      }

      $changeSet = $args->getEntityChangeSet();
      $draft = $meeting->getStatus() === Meeting::STATUS_DRAFT || ($args->hasChangedField('status') && $changeSet['status'][0] == Meeting::STATUS_DRAFT);
      $errors = $this->meetingService->getMeetingErrors($meeting);
      if (!empty($errors)) {
        throw new ValidatorException($this->translator->trans('meetings.error.invalid_meeting') . ': ' . implode(', ', $errors));
      }

      if (!$draft && ($args->hasChangedField('fromTime') || $args->hasChangedField('toTime'))) {
        $meeting->setRescheduled($meeting->getRescheduled() + 1);
      }

      if ($draft && in_array($meeting->getStatus(), [Meeting::STATUS_PENDING, Meeting::STATUS_APPROVED])) {
        $this->meetingService->sendEmailNewMeeting($meeting);
      } else {
        $this->meetingService->sendEmailUpdatedMeeting($meeting, $args->getEntityChangeSet());
      }
    }
  }

  /**
   * Sends email to citizen when meeting is removed
   *
   * @param LifecycleEventArgs $args
   * @throws Error
   */
  public function postRemove(LifecycleEventArgs $args): void
  {
    $meeting = $args->getObject();
    if ($meeting instanceof Meeting) {
      $this->meetingService->sendEmailRemovedMeeting($meeting);
    }
  }

  /**
   *
   * @param PostUpdateEventArgs $event
   */
  public function postUpdate(PostUpdateEventArgs $event): void
  {
    $meeting = $event->getObject();
    if ($meeting instanceof Meeting) {
      if ($meeting->getStatus() == Meeting::STATUS_APPROVED) {
        try {
          $this->meetingAsyncTaskService->createMeetingReminderAsync($meeting);
        } catch (AlreadyScheduledException $exception) {
          $this->logger->error('Meeting async task service: error creating reminder async task for meeting ' . $meeting->getId() . 'with status ' . $meeting->getStatus());
        }
      }
      if ($meeting->getStatus() === Meeting::STATUS_CANCELLED) {
        try {
          $this->meetingAsyncTaskService->createDeleteCancelledAsync($meeting);
        } catch (AlreadyScheduledException $exception) {
          $this->logger->error('Meeting async task service: error creating delete cancelled async task for meeting ' . $meeting->getId() . 'with status ' . $meeting->getStatus());
        }
      }
    }
  }
}
