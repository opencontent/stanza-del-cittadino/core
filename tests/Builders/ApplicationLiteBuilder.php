<?php

declare(strict_types=1);

namespace Tests\Builders;

use App\Applications\Exceptions\ApplicationLiteException;
use App\Entity\ApplicationLite;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Servizio;

class ApplicationLiteBuilder
{
  private ?string $externalId = null;
  private string $subject = 'Default Subject';
  private int $status = 2000;
  private Servizio $servizio;
  private CPSUser $applicant;
  private CPSUser $beneficiary;

  public static function createAsExternal(): self
  {
    $self = new self();
    $self->externalId = uniqid('external-id_');

    return $self;
  }

  private function __construct()
  {
    $this->servizio = ServizioBuilder::createAsApplicationLiteService()->build();
    $this->applicant = new CPSUser();
  }

  public function withExternalId(string $externalId): self
  {
    $this->externalId = $externalId;

    return $this;
  }

  public function withSubject(string $subject): self
  {
    $this->subject = $subject;

    return $this;
  }

  public function withStatus(int $status): self
  {
    $this->status = $status;

    return $this;
  }

  public function withServizio(Servizio $servizio): self
  {
    $this->servizio = $servizio;

    return $this;
  }

  public function withApplicant(CPSUser $applicant): self
  {
    $this->applicant = $applicant;

    return $this;
  }

  public function withBeneficiary(CPSUser $beneficiary): self
  {
    $this->beneficiary = $beneficiary;

    return $this;
  }

  /** @throws ApplicationLiteException */
  public function build(): ApplicationLite
  {
    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');

    if (null !== $this->externalId) {

      $application = ApplicationLite::createAsExternal(
        $this->externalId,
        $this->servizio,
        $this->applicant,
        $this->subject,
        $this->status,
        $ente,
      );

    } else {

      $application = ApplicationLite::create(
        $this->servizio,
        $this->applicant,
        $this->subject,
        $this->status,
        $ente,
      );
    }

    $application->setBeneficiary($this->beneficiary);

    return $application;
  }
}
