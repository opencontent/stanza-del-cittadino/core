<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241115162634 extends AbstractMigration
{
  public function getDescription(): string
  {
    return 'Create external_actions on pratica table';
  }

  public function up(Schema $schema): void
  {
    $this->addSql('ALTER TABLE pratica ADD external_actions JSONB DEFAULT NULL');
  }

  public function down(Schema $schema): void
  {
    $this->addSql('ALTER TABLE pratica DROP external_actions');
  }
}
