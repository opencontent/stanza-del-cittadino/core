<?php

namespace App\Controller\Rest;


use App\Entity\CodeGenerationStrategy;
use App\Entity\User;
use App\Form\Api\CodeGenerationStrategyApiType;
use App\Model\Api\PagedList;
use App\Services\Manager\CodeCounterManager;
use App\Utils\FormUtils;
use DateTime;
use App\Model\LinksPagedList;
use App\Model\MetaPagedList;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\NotFoundProblem;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class CodeGenerationStrategiesAPIController
 * @package App\Controller
 * @Route("/code-generation")
 */
class CodeGenerationStrategiesAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;

  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  /**
   * List all strategies
   * @Rest\Get("/strategies", name="strategies_api_list")
   *
   * @OA\Parameter(
   *       name="offset",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Offset of the query"
   *   )
   * @OA\Parameter(
   *       name="limit",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Limit of the query"
   *   )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of code generation strategies",
   *     @OA\JsonContent(
   *        type="object",
   *        @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *        @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *        @OA\Property(property="data", type="array",
   *           @OA\Items(ref=@Model(type=CodeGenerationStrategy::class, groups={"read"}))
   *        )
   *     )
   *  )
   *
   * @OA\Tag(name="code-generation")
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function getStrategiesAction(Request $request): View
  {

    $offset = $request->query->getInt('offset', 0);
    $limit = $request->query->getInt('limit', 10);

    try {

      if ($limit > 100) {
        return $this->view(["Limit parameter is too high"], Response::HTTP_BAD_REQUEST);
      }

      $repo = $this->entityManager->getRepository(CodeGenerationStrategy::class);

      $meta = new MetaPagedList();
      $meta->setParameter(['offset' => $offset, 'limit' => $limit]);
      $meta->setCount($repo->getStrategies([], true));
      $links = new LinksPagedList();
      $self = $this->generateUrl('strategies_api_list', $meta->getParameter(), UrlGeneratorInterface::ABSOLUTE_URL);
      $links->setSelf($self);
      $result = new PagedList($meta, $links, $repo->getStrategies([], false, 'name', 'ASC', $offset, $limit));

      return $this->view($result, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('strategy_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('strategy_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  /**
   * Retrieve a Strategy by id
   * @Rest\Get("/strategies/{id}", name="strategy_api_get")
   *
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a code generation strategy",
   *     @Model(type=CodeGenerationStrategy::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Strategy not found"
   * )
   * @OA\Tag(name="code-generation")
   *
   * @param Request $request
   * @param string $id
   * @return View
   * @throws ApiProblemException
   */
  public function getStrategyAction(Request $request, $id): View
  {
    try {
      $repository = $this->entityManager->getRepository(CodeGenerationStrategy::class);
      $result = $repository->find($id);

      if (!$result instanceof CodeGenerationStrategy) {
        throw new ApiProblemException(
          new NotFoundProblem("Code generation strategy {$id} not found")
        );
      }


    } catch (ApiProblemException $e) {
      $this->logger->error('strategy_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('strategy_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($result, Response::HTTP_OK)->setContext($context);
  }

  /**
   * Create a code generation strategy
   * @Rest\Post("/strategies", name="strategies_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The strategy to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=CodeGenerationStrategy::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a Strategy"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *      response=403,
   *      description="Forbidden"
   *  )
   *
   * @OA\Tag(name="code-generation")
   *
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function postStrategyAction(Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    try {
      $item = new CodeGenerationStrategy();
      $form = $this->createForm(CodeGenerationStrategyApiType::class, $item);
      $this->processForm($request, $form);

      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $this->entityManager->persist($item);
      $this->entityManager->flush();

    } catch (ApiProblemException $e) {
      $this->logger->error('strategies_api_post', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('strategies_api_post', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }

    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($item, Response::HTTP_CREATED)->setContext($context);
  }

  /**
   * Edit full Strategy
   * @Rest\Put("/strategies/{id}", name="strategies_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The strategy to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=CodeGenerationStrategy::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full Strategy"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=403,
   *    description="Forbidden"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="code-generation")
   *
   * @param $id
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function putStrategyAction($id, Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    try {
      $repository = $this->getDoctrine()->getRepository(CodeGenerationStrategy::class);
      $item = $repository->find($id);
      if (!$item instanceof CodeGenerationStrategy) {
        throw new ApiProblemException(
          new NotFoundProblem("Strategy {$id} not found")
        );
      }

      $form = $this->createForm(CodeGenerationStrategyApiType::class, $item);
      $this->processForm($request, $form);

      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'put_validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (ApiProblemException $e) {
      $this->logger->error('strategies_api_put', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('strategies_api_put', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }

    return $this->view(["Object Modified Successfully"], Response::HTTP_OK);
  }

  /**
   * Patch a Strategy
   * @Rest\Patch("/strategies/{id}", name="strategies_api_patch")
   *
   * @Security(name="Bearer")
   *
   *
   * @OA\RequestBody(
   *     description="The strategy to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=CodeGenerationStrategy::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch a strategy"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=403,
   *    description="Forbidden"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="code-generation")
   *
   * @param $id
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function patchStrategyAction($id, Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    try {
      $repository = $this->getDoctrine()->getRepository(CodeGenerationStrategy::class);
      $item = $repository->find($id);
      if (!$item instanceof CodeGenerationStrategy) {
        throw new ApiProblemException(
          new NotFoundProblem("Strategy {$id} not found")
        );
      }

      $form = $this->createForm(CodeGenerationStrategyApiType::class, $item);
      $this->processForm($request, $form);

      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'patch_validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }


      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (ApiProblemException $e) {
      $this->logger->error('strategies_api_put', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('strategies_api_put', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_OK);
  }

  /**
   * Delete a strategy
   * @Rest\Delete("/strategies/{id}", name="strategies_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="code-generation")
   *
   * @Method("DELETE")
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function deleteStrategyAction($id): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    try {
      $repository = $this->entityManager->getRepository(CodeGenerationStrategy::class);
      $item = $repository->find($id);
      if (!$item instanceof CodeGenerationStrategy) {
        throw new ApiProblemException(
          new NotFoundProblem("Strategy {$id} not found")
        );
      }
      $this->entityManager->remove($item);
      $this->entityManager->flush();

      return $this->view(null, Response::HTTP_NO_CONTENT);

    } catch (ApiProblemException $e) {
      $this->logger->error('strategies_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('strategies_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => AbstractApiController::MESSAGE_INTERNAL_SERVER_ERROR]));
    }
  }

  /**
   * Retreive a code based on a code generation strategy and a date
   * @Rest\Post("/codes", name="code_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application to patch | Register integration request | Register integration answer",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="code_generation_strategy_id", type="string", format="uuid", description="Uuid of the code generation strategy to use for generation"),
   *             @OA\Property(property="prefix", type="string", description="Override the default prefix of the Strategy"),
   *             @OA\Property(property="postfix", type="string", description="Override the default postfix of the Strategy"),
   *             @OA\Property(property="day", type="string", format="Y-m-d", description="The day for which a code is requested"),
   *         )
   *     )
   * )
   *
   *
   * @OA\Response(
   *     response=201,
   *     description="Retrive a code"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="code-generation")
   *
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function postCodeAction(Request $request, CodeCounterManager $counterManager): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    try {

      $cgsId = $request->request->get('code_generation_strategy_id');
      $prefix = $request->request->get('prefix');
      $postfix = $request->request->get('postfix');
      $day = $request->request->get('day');

      $repository = $this->entityManager->getRepository(CodeGenerationStrategy::class);
      $cgs = $repository->find($cgsId);
      if (!$cgs instanceof CodeGenerationStrategy) {
        throw new ApiProblemException(
          new NotFoundProblem("Strategy {$cgsId} not found")
        );
      }

      $dayDate = null;
      if ($day) {
        $dayDate = DateTime::createFromFormat('Y-m-d', $day);
        if (!$dayDate || ($dayDate->format('Y-m-d') !== $day)) {
          throw new ApiProblemException(
            new BadRequestProblem('Parameter day must be in this format: yyyy-mm-dd')
          );
        }
      }

      $code = $counterManager->getNextCode($cgs, $dayDate, $prefix, $postfix);


    } catch (ApiProblemException $e) {
      $this->logger->error('code_api_post', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('code_api_post', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }

    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($code, Response::HTTP_OK)->setContext($context);
  }

  /**
   * @param Request $request
   * @param FormInterface $form
   * @throws \JsonException
   */
  private function processForm(Request $request, FormInterface $form): void
  {
    $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

    $clearMissing = $request->getMethod() != 'PATCH';
    $form->submit($data, $clearMissing);
  }
}
