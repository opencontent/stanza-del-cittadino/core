<?php

namespace App\Event;

use App\Entity\Pratica;
use Symfony\Contracts\EventDispatcher\Event;

class ApplicationBackofficeUpdatedEvent extends Event
{

  public const NAME = 'ocsdc.application.backoffice_updated';

  private Pratica $application;

  public function __construct(Pratica $application)
  {
    $this->application = $application;
  }

  public function getApplication(): Pratica
  {
    return $this->application;
  }



}
