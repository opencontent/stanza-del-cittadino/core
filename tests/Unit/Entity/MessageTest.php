<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\CPSUser;
use App\Entity\Message;
use App\Entity\Pratica;
use App\Entity\User;
use App\Entity\AllegatoMessaggio;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class MessageTest extends TestCase
{
  public function testConstructorInitializesDefaults(): void
  {
    $message = new Message();

    $this->assertInstanceOf(UuidInterface::class, $message->getId());
    $this->assertIsInt($message->getCreatedAt());
    $this->assertTrue($message->isProtocolRequired());
    $this->assertInstanceOf(ArrayCollection::class, $message->getAttachments());
  }

  public function testSetAndGetMessage(): void
  {
    $message = new Message();
    $content = 'This is a test message';
    $message->setMessage($content);
    $this->assertEquals($content, $message->getMessage());
  }

  public function testSetAndGetSubject(): void
  {
    $message = new Message();
    $subject = 'Test Subject';
    $message->setSubject($subject);
    $this->assertEquals($subject, $message->getSubject());
  }

  public function testSetAndGetAuthor(): void
  {
    $message = new Message();
    $author = $this->createMock(User::class);
    $message->setAuthor($author);
    $this->assertSame($author, $message->getAuthor());
  }

  public function testSetAndGetApplication(): void
  {
    $message = new Message();
    $application = $this->createMock(Pratica::class);
    $message->setApplication($application);
    $this->assertSame($application, $message->getApplication());
  }

  public function testSetAndGetVisibility(): void
  {
    $message = new Message();
    $visibility = Message::VISIBILITY_APPLICANT;
    $message->setVisibility($visibility);
    $this->assertEquals($visibility, $message->getVisibility());
  }

  public function testSetAndGetEmail(): void
  {
    $message = new Message();
    $email = 'test@example.com';
    $message->setEmail($email);
    $this->assertEquals($email, $message->getEmail());
  }

  public function testSetAndGetTimestamps(): void
  {
    $message = new Message();
    $time = time();

    $message->setSentAt($time);
    $this->assertEquals($time, $message->getSentAt());

    $message->setReadAt($time);
    $this->assertEquals($time, $message->getReadAt());

    $message->setClickedAt($time);
    $this->assertEquals($time, $message->getClickedAt());
  }

  public function testAddAndRemoveAttachment(): void
  {
    $message = new Message();
    $attachment = $this->createMock(AllegatoMessaggio::class);

    $message->addAttachment($attachment);
    $this->assertCount(1, $message->getAttachments());
    $this->assertTrue($message->getAttachments()->contains($attachment));

    $message->removeAttachment($attachment);
    $this->assertCount(0, $message->getAttachments());
  }

  public function testGeneratedDocument(): void
  {
    $message = new Message();
    $document = $this->createMock(AllegatoMessaggio::class);

    $message->addGeneratedDocument($document);
    $this->assertSame($document, $message->getGeneratedDocument());
  }

  public function testSetAndGetProtocolRequired(): void
  {
    $message = new Message();
    $this->assertTrue($message->isProtocolRequired());

    $message->setProtocolRequired(false);
    $this->assertFalse($message->isProtocolRequired());
  }

  public function testSetAndGetProtocolledAt(): void
  {
    $message = new Message();
    $time = time();
    $message->setProtocolledAt($time);
    $this->assertEquals($time, $message->getProtocolledAt());
  }

  public function testSetAndGetProtocolNumber(): void
  {
    $message = new Message();
    $protocolNumber = '12345';
    $message->setProtocolNumber($protocolNumber);
    $this->assertEquals($protocolNumber, $message->getProtocolNumber());
  }

  public function testSetAndGetCallToActions(): void
  {
    $message = new Message();
    $callToActions = ['action1', 'action2'];
    $message->setCallToAction($callToActions);
    $this->assertEquals($callToActions, $message->getCallToAction());
  }

  public function testSetAndGetExternalId(): void
  {
    $message = new Message();
    $externalId = 'external_123';
    $message->setExternalId($externalId);
    $this->assertEquals($externalId, $message->getExternalId());
  }

  public function testIsInbound(): void
  {
    $message = new Message();
    $author = $this->createMock(User::class);
    $message->setAuthor($author);

    $this->assertFalse($message->isInbound());

    $cpsUser = $this->createMock(CPSUser::class);
    $message->setAuthor($cpsUser);
    $this->assertTrue($message->isInbound());
  }

  public function testGetReceivers(): void
  {
    $message = new Message();
    $application = $this->createMock(Pratica::class);
    $user = $this->createMock(User::class);
    $operator = $this->createMock(User::class);

    $application->method('getUser')->willReturn($user);
    $application->method('getOperatore')->willReturn($operator);
    $message->setApplication($application);
    $message->setVisibility(Message::VISIBILITY_APPLICANT);
    $message->setAuthor($operator);

    $receivers = $message->getReceivers();
    $this->assertContains($user, $receivers);

    $message->setAuthor($user);
    $receivers = $message->getReceivers();
    $this->assertContains($operator, $receivers);
  }
}
