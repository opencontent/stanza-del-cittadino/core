<?php

namespace App\Security\Voters;

use App\Security\OAuth\StorableProviderFactoryInterface;
use App\Security\OAuth\ProviderFactoryInterface;
use App\Security\OAuthAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class OAuthClientVoter extends Voter
{
  const READ = 'read_oauth_client';

  const EDIT = 'edit_oauth_client';

  private Security $security;

  private string $loginRoute;

  private ProviderFactoryInterface $providerFactory;

  public function __construct(Security $security, ProviderFactoryInterface $providerFactory, $loginRoute)
  {
    $this->security = $security;
    $this->providerFactory = $providerFactory;
    $this->loginRoute = $loginRoute;
  }

  protected function supports($attribute, $subject): bool
  {
    return $attribute === self::READ
      || $attribute === self::EDIT;
  }

  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    if ($attribute === self::EDIT && !$this->providerFactory instanceof StorableProviderFactoryInterface) {
      return false;
    }

    return $this->security->isGranted('ROLE_ADMIN')
      && $this->loginRoute === OAuthAuthenticator::LOGIN_ROUTE;
  }

}
