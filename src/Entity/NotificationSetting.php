<?php

namespace App\Entity;

use App\Repository\NotificationSettingRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass=NotificationSettingRepository::class)
 */
class NotificationSetting
{
  use TimestampableEntity;

  public const NOTIFICATION_SETTING_ALL_TYPE = '*';
  public const NOTIFICATION_SETTING_CALENDAR_TYPE = 'calendar';

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity=User::class, inversedBy="notificationSettings")
   * @ORM\JoinColumn(nullable=false)
   */
  private ?User $owner;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private ?string $objectType;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private ?string $objectId;

  /**
   * @ORM\Column(type="boolean")
   */
  private bool $emailNotification = true;

  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
  }

  public function getId()
  {
    return $this->id;
  }

  public function getOwner(): ?User
  {
    return $this->owner;
  }

  public function setOwner(?User $owner): self
  {
    $this->owner = $owner;

    return $this;
  }

  public function getObjectType(): ?string
  {
    return $this->objectType;
  }

  public function setObjectType(string $objectType): self
  {
    $this->objectType = $objectType;

    return $this;
  }

  public function getObjectId(): ?string
  {
    return $this->objectId;
  }

  public function setObjectId(string $objectId): self
  {
    $this->objectId = $objectId;

    return $this;
  }

  public function getEmailNotification(): ?bool
  {
    return $this->emailNotification;
  }

  public function setEmailNotification(bool $emailNotification): self
  {
    $this->emailNotification = $emailNotification;

    return $this;
  }
}
