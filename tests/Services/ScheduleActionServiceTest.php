<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Entity\ScheduledAction;
use App\Services\ScheduleActionService;
use Tests\Base\AbstractAppTestCase;

class ScheduleActionServiceTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();
    $this->cleanDb(ScheduledAction::class);
  }

  public function testServiceExists(): void
  {
    $service = $this->container->get('ocsdc.schedule_action_service');
    $this->assertNotNull($service);
    $this->assertEquals(ScheduleActionService::class, get_class($service));
  }

  public function testAppendAction(): void
  {
    $service = $this->container->get('ocsdc.schedule_action_service');
    $service->appendAction('test', 'test', 'test');

    $items = $this->em->getRepository(ScheduledAction::class)->findAll();
    $this->assertEquals(count($items), 1);
  }

  public function testCannotReAppendSameAction(): void
  {
    $this->expectException(\App\ScheduledAction\Exception\AlreadyScheduledException::class);

    $service = $this->container->get('ocsdc.schedule_action_service');
    $service->appendAction('test', 'test', 'test');
    $service->appendAction('test', 'test', 'test');
  }

  public function testMarkAsDone(): void
  {
    $service = $this->container->get('ocsdc.schedule_action_service');
    $service->appendAction('test', 'test', 'test');
    $items = $this->em->getRepository(ScheduledAction::class)->findAll();
    foreach ($items as $item) {
      $service->markAsDone($item);
    }
    $service->done();

    $items = $this->em->getRepository(ScheduledAction::class)->findAll();
    $this->assertEquals(count($items), 0);
  }

  public function testMarkAsInvalid(): void
  {
    $this->testMarkAsDone();
  }

  public function testGetActions(): void
  {
    $service = $this->container->get('ocsdc.schedule_action_service');
    $service->appendAction('test', 'test', 'test');

    $items = $this->em->getRepository(ScheduledAction::class)->findAll();
    $this->assertEquals(count($items), count($service->getActions()));
  }
}
