<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240404130209 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs;
        $this->addSql('ALTER TABLE ente ADD ipa_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ente ADD cadastral_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ente ADD aoo_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ente ALTER codice_meccanografico DROP NOT NULL');
        $this->addSql('ALTER TABLE ente ALTER codice_amministrativo DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_197E17226954DFB3 ON ente (ipa_code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_197E1722A8CDCB20 ON ente (cadastral_code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_197E1722112D4951 ON ente (aoo_code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_197E17226954DFB3');
        $this->addSql('DROP INDEX UNIQ_197E1722A8CDCB20');
        $this->addSql('DROP INDEX UNIQ_197E1722112D4951');
        $this->addSql('ALTER TABLE ente DROP ipa_code');
        $this->addSql('ALTER TABLE ente DROP cadastral_code');
        $this->addSql('ALTER TABLE ente DROP aoo_code');
        $this->addSql('ALTER TABLE ente ALTER codice_meccanografico SET NOT NULL');
        $this->addSql('ALTER TABLE ente ALTER codice_amministrativo SET NOT NULL');
    }
}
