<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240704181432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratica DROP CONSTRAINT fk_448253ac9eaac4fb');
        $this->addSql('ALTER TABLE servizio_erogatori DROP CONSTRAINT fk_995b50e39eaac4fb');
        $this->addSql('ALTER TABLE erogatore_ente DROP CONSTRAINT fk_1b52a1c19eaac4fb');
        $this->addSql('DROP TABLE servizio_erogatori');
        $this->addSql('DROP TABLE erogatore');
        $this->addSql('DROP TABLE erogatore_ente');
        $this->addSql('DROP INDEX idx_448253ac9eaac4fb');
        $this->addSql('ALTER TABLE pratica DROP erogatore_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE servizio_erogatori (servizio_id UUID NOT NULL, erogatore_id UUID NOT NULL, PRIMARY KEY(servizio_id, erogatore_id))');
        $this->addSql('CREATE INDEX idx_995b50e39eaac4fb ON servizio_erogatori (erogatore_id)');
        $this->addSql('CREATE INDEX idx_995b50e35513f0b4 ON servizio_erogatori (servizio_id)');
        $this->addSql('CREATE TABLE erogatore (id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE erogatore_ente (erogatore_id UUID NOT NULL, ente_id UUID NOT NULL, PRIMARY KEY(erogatore_id, ente_id))');
        $this->addSql('CREATE INDEX idx_1b52a1c19eaac4fb ON erogatore_ente (erogatore_id)');
        $this->addSql('CREATE INDEX idx_1b52a1c1efb68f0a ON erogatore_ente (ente_id)');
        $this->addSql('ALTER TABLE servizio_erogatori ADD CONSTRAINT fk_995b50e35513f0b4 FOREIGN KEY (servizio_id) REFERENCES servizio (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE servizio_erogatori ADD CONSTRAINT fk_995b50e39eaac4fb FOREIGN KEY (erogatore_id) REFERENCES erogatore (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE erogatore_ente ADD CONSTRAINT fk_1b52a1c19eaac4fb FOREIGN KEY (erogatore_id) REFERENCES erogatore (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE erogatore_ente ADD CONSTRAINT fk_1b52a1c1efb68f0a FOREIGN KEY (ente_id) REFERENCES ente (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pratica ADD erogatore_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE pratica ADD CONSTRAINT fk_448253ac9eaac4fb FOREIGN KEY (erogatore_id) REFERENCES erogatore (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_448253ac9eaac4fb ON pratica (erogatore_id)');
    }
}
