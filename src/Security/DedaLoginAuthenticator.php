<?php

namespace App\Security;

use App\Services\InstanceService;
use App\Services\UserSessionService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DedaLoginAuthenticator extends OpenLoginAuthenticator
{
  public const LOGIN_ROUTE = 'login_deda';

  private SessionInterface $session;

  /**
   * OpenLoginAuthenticator constructor.
   * @param UrlGeneratorInterface $urlGenerator
   * @param $loginRoute
   * @param UserSessionService $userSessionService
   * @param InstanceService $instanceService
   * @param JWTTokenManagerInterface $JWTTokenManager
   * @param SessionInterface $session
   * @param LoggerInterface $logger
   */
  public function __construct(
    UrlGeneratorInterface     $urlGenerator,
                              $loginRoute,
    UserSessionService        $userSessionService,
    InstanceService           $instanceService,
    JWTTokenManagerInterface  $JWTTokenManager,
    SessionInterface          $session,
    LoggerInterface           $logger
  )
  {
    parent::__construct($urlGenerator, $loginRoute, $userSessionService, $instanceService, $JWTTokenManager, $logger);
    $this->session = $session;
  }

  public function supports(Request $request): bool
  {
    try {
      $this->checkLoginRoute();
    } catch (\Exception $e) {
      return false;
    }

    if ($this->session->has('DedaLoginUserData')){
      $request->headers->set('X-Forwarded-User', $this->session->get('DedaLoginUserData'));
      $this->session->remove('DedaLoginUserData');
    }
    return $request->attributes->get('_route') === self::LOGIN_ROUTE && $this->checkHeaderUserData($request);
  }


  protected function getLoginRouteSupported(): array
  {
    return [self::LOGIN_ROUTE];
  }
}
