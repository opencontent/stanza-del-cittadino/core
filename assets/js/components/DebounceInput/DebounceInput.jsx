import React, { useEffect, useState } from "react";
import {Input} from "design-react-kit";

export const DebounceInput = ({onInputValueChange,labelName}) => {
  const [inputValue, setInputValue] = useState(null);
  const [debouncedInputValue, setDebouncedInputValue] = useState(null);

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };


  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setDebouncedInputValue(inputValue);
      onInputValueChange(inputValue)
    }, 1000);
    return () => clearTimeout(timeoutId);
  }, [inputValue, 1000]);


  return <Input wrapperClassName={'mb-0 pb-0 custom-label'} label={labelName} type="search" value={inputValue} placeholder={labelName} onChange={handleInputChange} id={'searchFilter'} />;
};
