<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class ValidateAppSetupCommand extends Command
{
  protected static $defaultName = 'ocsdc:validate-setup';
  protected static $defaultDescription = 'This command validate the setup of the App checking environment variables, YAML file validity (recursive), permissions, and dependencies.';
  private LoggerInterface $logger;

  public function __construct(LoggerInterface $logger)
  {
    parent::__construct();
    $this->logger = $logger;
  }
  protected function configure(): void
  {
    $this
      ->setDescription(self::$defaultDescription)
      ->setHelp('This command checks environment variables, YAML file validity (recursive), permissions, and dependencies.');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $io = new SymfonyStyle($input, $output);

    $io->title('Validating app Setup');

    // 1. Check environment variables
    $requiredEnv = ['APP_ENV', 'DB_HOST', 'DB_PORT', 'DB_NAME', 'DB_USER', 'DB_PASSWORD'];
    foreach ($requiredEnv as $env) {
      if (!getenv($env)) {
        $errorMsg = "Environment variable {$env} is not set.";
        $io->error($errorMsg);
        $this->logger->error($errorMsg);
        return Command::FAILURE;
      }
    }
    $io->success('Environment variables are set.');


    // 2. Validate YAML files
    $io->section('Validating YAML and YML Files');
    $directoriesToScan = ['config'];
    $yamlErrors = [];
    foreach ($directoriesToScan as $directory) {
      if (!is_dir($directory)) {
        $io->warning("Directory {$directory} does not exist. Skipping YAML validation for this directory.");
        continue;
      }

      // Scan recursively for .yaml and .ym files
      $files = $this->findFiles($directory, '/\.(yaml|yml)$/');
      foreach ($files as $file) {
        try {
          Yaml::parseFile($file, Yaml::PARSE_CUSTOM_TAGS);
          //$io->success("Valid YAML/YML: {$file}");
        } catch (ParseException $e) {
          $yamlErrors[] = "Invalid YAML/YM in {$file}: " . $e->getMessage();
        }
      }
    }

    if (!empty($yamlErrors)) {
      foreach ($yamlErrors as $error) {
        $io->error($error);
        $this->logger->error($error);
      }
      return Command::FAILURE;
    }

    // 3. Check permissions for key directories
    $directories = ['var/log', 'var/cache'];
    foreach ($directories as $dir) {
      if (!is_writable($dir)) {
        $errorMsg = "Directory {$dir} is not writable.";
        $io->error($errorMsg);
        $this->logger->error($errorMsg);
        return Command::FAILURE;
      }
    }
    $io->success('Directory permissions are valid.');

    // 4. Check if dependencies are installed
    if (!file_exists('vendor/autoload.php')) {
      $errorMsg = 'Composer dependencies are not installed. Run `composer install`.';
      $io->error($errorMsg);
      $this->logger->error($errorMsg);
      return Command::FAILURE;
    }
    $io->success('Dependencies are installed.');
    $io->success('All checks passed. App setup is valid.');
    return Command::SUCCESS;
  }

  private function findFiles(string $directory, string $pattern): array
  {
    $files = [];
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory));
    $regexIterator = new RegexIterator($iterator, $pattern);
    foreach ($regexIterator as $file) {
      $files[] = $file->getPathname();
    }
    return $files;
  }
}
