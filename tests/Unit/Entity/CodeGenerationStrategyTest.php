<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\CodeCounter;
use App\Entity\CodeGenerationStrategy;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;

/**
 * @covers \App\Entity\CodeGenerationStrategy
 */
class CodeGenerationStrategyTest extends TestCase
{
  private CodeGenerationStrategy $codeGenerationStrategy;

  protected function setUp(): void
  {
    $this->codeGenerationStrategy = new CodeGenerationStrategy();
  }

  public function testConstructorInitializesId(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->codeGenerationStrategy->getId());
    $this->assertNotNull($this->codeGenerationStrategy->getId());
  }

  public function testConstructorInitializesCodeCounters(): void
  {
    $this->assertInstanceOf(Collection::class, $this->codeGenerationStrategy->getCodeCounters());
    $this->assertCount(0, $this->codeGenerationStrategy->getCodeCounters());
  }

  public function testGetAndSetName(): void
  {
    $name = 'Test Strategy';
    $this->codeGenerationStrategy->setName($name);
    $this->assertEquals($name, $this->codeGenerationStrategy->getName());
  }

  public function testGetAndSetPrefix(): void
  {
    $prefix = 'PRE';
    $this->codeGenerationStrategy->setPrefix($prefix);
    $this->assertEquals($prefix, $this->codeGenerationStrategy->getPrefix());

    $this->codeGenerationStrategy->setPrefix(null);
    $this->assertNull($this->codeGenerationStrategy->getPrefix());
  }

  public function testGetAndSetPostfix(): void
  {
    $postfix = 'POST';
    $this->codeGenerationStrategy->setPostfix($postfix);
    $this->assertEquals($postfix, $this->codeGenerationStrategy->getPostfix());

    $this->codeGenerationStrategy->setPostfix(null);
    $this->assertNull($this->codeGenerationStrategy->getPostfix());
  }

  public function testGetAndSetCounterDigitLength(): void
  {
    $length = 4;
    $this->codeGenerationStrategy->setCounterDigitLength($length);
    $this->assertEquals($length, $this->codeGenerationStrategy->getCounterDigitLength());
  }

  public function testGetMaxCounterValue(): void
  {
    $this->codeGenerationStrategy->setCounterDigitLength(3);
    $this->assertEquals(999, $this->codeGenerationStrategy->getMaxCounterValue());

    $this->codeGenerationStrategy->setCounterDigitLength(0);
    $this->assertEquals(0, $this->codeGenerationStrategy->getMaxCounterValue());

    $this->codeGenerationStrategy->setCounterDigitLength(-1);
    $this->assertEquals(0, $this->codeGenerationStrategy->getMaxCounterValue());
  }

  public function testGetAndSetTemporalReset(): void
  {
    $temporalReset = 'daily';
    $this->codeGenerationStrategy->setTemporalReset($temporalReset);
    $this->assertEquals($temporalReset, $this->codeGenerationStrategy->getTemporalReset());
  }

  public function testAddAndRemoveCodeCounter(): void
  {
    $codeCounter = $this->createMock(CodeCounter::class);

    $this->codeGenerationStrategy->addCodeCounter($codeCounter);
    $this->assertCount(1, $this->codeGenerationStrategy->getCodeCounters());
    $this->assertTrue($this->codeGenerationStrategy->getCodeCounters()->contains($codeCounter));

    $this->codeGenerationStrategy->removeCodeCounter($codeCounter);
    $this->assertCount(0, $this->codeGenerationStrategy->getCodeCounters());
    $this->assertFalse($this->codeGenerationStrategy->getCodeCounters()->contains($codeCounter));
  }

  public function testRemoveCodeCounterWhenNotInCollection(): void
  {
    $codeCounter = $this->createMock(CodeCounter::class);
    $this->assertCount(0, $this->codeGenerationStrategy->getCodeCounters());

    $this->codeGenerationStrategy->removeCodeCounter($codeCounter);
    $this->assertCount(0, $this->codeGenerationStrategy->getCodeCounters());
  }
}
