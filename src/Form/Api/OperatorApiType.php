<?php

namespace App\Form\Api;

use App\Dto\Operator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperatorApiType extends AbstractType
{
  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    /** @var Operator $operator */
    $operator = $builder->getData();

    $builder
      ->add('username', TextType::class)
      ->add('nome', TextType::class)
      ->add('cognome', TextType::class)
      ->add('email', EmailType::class, [
        'required' => !$operator->isSystemUser()
      ])
      ->add('system_user', CheckboxType::class, [
        'required' => false
      ])
      ->add('password', TextType::class)
      ->add('enabled_services_ids', CollectionType::class, [
        'entry_type' => TextType::class,
        'allow_add' => true,
        'allow_delete' => true
      ])
    ;
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Dto\Operator',
      'csrf_protection' => false,
      'allow_extra_fields' => true
    ));
  }

}
