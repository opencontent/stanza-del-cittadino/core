/* global L */
import '../../styles/vendor/_leaflet.scss';
import 'leaflet';
import ThemeColors from "../constant/theme-colors";


export class ApplicationMap {

  static init() {
    const lon = parseFloat($('#map').data('lon'));
    const lat = parseFloat($('#map').data('lat'));
    const geojsonFeatures = $('#map').data('areas');

    const map = L.map('map').setView([lat, lon], 8);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    const markerIcon = L.icon({
      iconUrl: '/bundles/app/images/marker-icon.png',
      shadowUrl: '/bundles/app/images/marker-shadow.png',
      iconSize:    [25, 41],
      iconAnchor:  [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize:  [41, 41]
    });

    const themeColorsValue = Object.values(ThemeColors);
    const group = new L.featureGroup;

    L.marker([lat, lon], {icon: markerIcon}).addTo(group);
    if (geojsonFeatures.length > 0) {
      geojsonFeatures.forEach((value, key) => {
        //console.log(value, key)
        L.geoJSON(value, {
          style: {
            color: themeColorsValue[key],
            weight: 3,
            opacity: 0.65,
          }
        }).addTo(group);
      })
    }

    group.addTo(map);
    //map.fitBounds(group.getBounds());


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      e.target // newly activated tab
      e.relatedTarget // previous active tab
      map.invalidateSize();
    })
  }
}
