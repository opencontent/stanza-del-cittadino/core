<?php

declare(strict_types=1);

namespace Tests\Unit\Serializer;

interface SerializerTestInterface
{
  public function getSchema(): string;
}
