<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Applications\Exceptions\ApplicationLiteException;
use App\Entity\ApplicationLite;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Servizio;
use DateTime;
use PHPUnit\Framework\TestCase;
use Tests\Builders\ServizioBuilder;

/**
 * @group unit
 * @group ApplicationLite
 */
class ApplicationLiteTest extends TestCase
{
  private Servizio $service;
  private CPSUser $applicant;

  protected function setUp(): void
  {
    $this->service = ServizioBuilder::createAsApplicationLiteService()->build();
    $this->applicant = $this->createMock(CPSUser::class);
  }

  public function testCreate(): void
  {
    $subject = 'Test Subject';
    $status = 2000;

    $applicationLite = ApplicationLite::create(
      $this->service,
      $this->applicant,
      $subject,
      $status,
      new Ente(),
    );

    $this->assertInstanceOf(ApplicationLite::class, $applicationLite);
    $this->assertEquals($subject, $applicationLite->subject());
    $this->assertEquals($status, $applicationLite->status());
    $this->assertSame($this->applicant, $applicationLite->applicant());
  }

  /** @throws ApplicationLiteException */
  public function testCreateAsExternalWithValidStatus(): void
  {
    $externalId = 'ext-123';
    $subject = 'Test External Subject';
    $status = 2000;

    $applicationLite = ApplicationLite::createAsExternal(
      $externalId,
      $this->service,
      $this->applicant,
      $subject,
      $status,
      new Ente(),
    );

    $this->assertInstanceOf(ApplicationLite::class, $applicationLite);
    $this->assertEquals($externalId, $applicationLite->externalId());
    $this->assertEquals($subject, $applicationLite->subject());
    $this->assertEquals($status, $applicationLite->status());
    $this->assertSame($this->applicant, $applicationLite->applicant());
  }

  public function testCreateAsExternalWithInvalidStatusThrowsException(): void
  {
    $externalId = 'ext-123';
    $subject = 'Test External Subject';
    $invalidStatus = 9999;

    $this->expectException(ApplicationLiteException::class);
    $this->expectExceptionMessage("ApplicationLite status is invalid: {$invalidStatus}");

    ApplicationLite::createAsExternal(
      $externalId,
      $this->service,
      $this->applicant,
      $subject,
      $invalidStatus,
      new Ente(),
    );
  }

  /** @throws ApplicationLiteException */
  public function testUpdateWithValidStatus(): void
  {
    $status = 2000;
    $newStatus = 7000;

    $applicationLite = ApplicationLite::create(
      $this->service,
      $this->applicant,
      'Test Subject',
      $status,
      new Ente(),
    );

    $applicationLite->update($newStatus, new DateTime());

    $this->assertEquals($newStatus, $applicationLite->status());
  }

  public function testUpdateWithInvalidStatusThrowsException(): void
  {
    $status = 2000;
    $invalidStatus = 1234;

    $applicationLite = ApplicationLite::create(
      $this->service,
      $this->applicant,
      'Test Subject',
      $status,
      new Ente(),
    );

    $this->expectException(ApplicationLiteException::class);
    $this->expectExceptionMessage("ApplicationLite status is invalid: {$invalidStatus}");

    $applicationLite->update($invalidStatus, new DateTime());
  }
}
