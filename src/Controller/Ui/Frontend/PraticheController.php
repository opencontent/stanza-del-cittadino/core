<?php

namespace App\Controller\Ui\Frontend;

use App\Entity\Allegato;
use App\Entity\CPSUser;
use App\Entity\GiscomPratica;
use App\Entity\Message;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use App\Entity\Servizio;
use App\Exception\ApplicationException;
use App\Form\ApplicationMessageType;
use App\Form\Base\PraticaFlow;
use App\Form\PraticaFlowRegistry;
use App\FormIO\ExpressionValidator;
use App\Handlers\Servizio\ForbiddenAccessException;
use App\Handlers\Servizio\ServizioHandlerRegistry;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Security\Voters\ApplicationVoter;
use App\Services\BreadcrumbsService;
use App\Services\FormServerApiAdapterService;
use App\Services\KafkaService;
use App\Services\Manager\MessageManager;
use App\Services\Manager\PdndManager;
use App\Services\Manager\PraticaManager;
use App\Services\Manager\ServiceManager;
use App\Services\ModuloPdfBuilderService;
use App\Services\PaymentService;
use App\Services\PraticaStatusService;
use App\Utils\iCalUtils;
use App\Utils\StringUtils;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Flagception\Manager\FeatureManagerInterface;
use JsonException;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class PraticheController
 *
 * @package App\Controller
 * @Route("/pratiche")
 */
class PraticheController extends AbstractController
{
  private PraticaStatusService $praticaStatusService;
  private ModuloPdfBuilderService $pdfBuilderService;
  private ExpressionValidator $expressionValidator;
  private LoggerInterface $logger;
  private TranslatorInterface $translator;
  private FeatureManagerInterface $featureManager;
  private PraticaManager $praticaManager;
  private FormServerApiAdapterService $formServerService;
  private EntityManagerInterface $entityManager;
  private BreadcrumbsService $breadcrumbsService;
  private ServizioHandlerRegistry $servizioHandlerRegistry;
  private MessageManager $messageManager;
  private ServiceManager $serviceManager;

  public function __construct(
    PraticaStatusService        $praticaStatusService,
    ModuloPdfBuilderService     $moduloPdfBuilderService,
    ExpressionValidator         $validator,
    LoggerInterface             $logger,
    TranslatorInterface         $translator,
    FeatureManagerInterface     $featureManager,
    PraticaManager              $praticaManager,
    FormServerApiAdapterService $formServerService,
    EntityManagerInterface      $entityManager,
    BreadcrumbsService          $breadcrumbsService,
    ServizioHandlerRegistry     $servizioHandlerRegistry,
    MessageManager              $messageManager,
    ServiceManager              $serviceManager
  )
  {
    $this->praticaStatusService = $praticaStatusService;
    $this->pdfBuilderService = $moduloPdfBuilderService;
    $this->expressionValidator = $validator;
    $this->logger = $logger;
    $this->translator = $translator;
    $this->featureManager = $featureManager;
    $this->praticaManager = $praticaManager;
    $this->formServerService = $formServerService;
    $this->entityManager = $entityManager;
    $this->breadcrumbsService = $breadcrumbsService;
    $this->servizioHandlerRegistry = $servizioHandlerRegistry;
    $this->messageManager = $messageManager;
    $this->serviceManager = $serviceManager;
  }

  /**
   * @Route("/", name="pratiche")
   */
  public function indexAction(): Response
  {
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem(
      ucfirst($this->translator->trans('personal_area')),
      'user_dashboard'
    );
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem($this->translator->trans('nav.pratiche'), 'pratiche');
    /** @var CPSUser $user */
    $user = $this->getUser();
    /** @var PraticaRepository $repo */
    $repo = $this->entityManager->getRepository(Pratica::class);
    $pratiche = $repo->count(
      array('user' => $user)
    );
    $pratichePending = $repo->findEvidencePraticaForUser($user);

    if (empty($pratichePending)) {
      $pratichePending = $repo->findPendingPraticaForUser($user);
    }

    $pending = [];
    foreach ($pratichePending as $p) {

      if (!isset($pending[$p->getServizio()->getSlug()])) {

        $pending[$p->getServizio()->getSlug()]['name'] = $p->getServizio()->getName();
        $pending[$p->getServizio()->getSlug()]['slug'] = $p->getServizio()->getSlug();
        $pending[$p->getServizio()->getSlug()]['applications'][] = $p;

      } elseif (count($pending[$p->getServizio()->getSlug()]) < 7) {

        $pending[$p->getServizio()->getSlug()]['applications'][] = $p;

      }

    }

    return $this->render('Pratiche/index.html.twig', [
      'user' => $user,
      'pratiche' => $pratiche,
      'title' => 'lista_pratiche',
      'pending' => $pending,
    ]);
  }

  /**
   * @Route("/list", name="pratiche_list", methods={"GET", "POST"})
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception
   */
  public function listAction(Request $request): Response
  {

    if ($this->featureManager->isActive('feature_personal_area_v2')) {
      return $this->redirectToRoute('pratiche');
    }

    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem(
      ucfirst($this->translator->trans('personal_area')),
      'user_dashboard'
    );
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem('nav.pratiche', 'pratiche');
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem('breadcrumbs.list', 'pratiche_list');
    /** @var CPSUser $user */
    $user = $this->getUser();
    $criteria = [
      'user' => $user,
    ];
    $serviceSlug = $request->query->get('service', false);
    if ($serviceSlug) {
      $serviceRepo = $this->entityManager->getRepository(Servizio::class);
      $service = $serviceRepo->findOneBy(['slug' => $serviceSlug]);
      if ($service instanceof Servizio) {
        $criteria ['servizio'] = $service;
      }
    }

    /** @var PraticaRepository $repo */
    $repo = $this->entityManager->getRepository(Pratica::class);
    $pratiche = $repo->findBy(
      $criteria,
      ['latestStatusChangeTimestamp' => 'DESC']
    );

    $services = [];
    $applications = [
      'draft' => [],
      'pending' => [],
      'completed' => [],
      'cancelled' => [],
      'integration' => [],
      'payment_pending' => [],
      'withdrawn' => [],
    ];

    /** @var Pratica $p */
    foreach ($pratiche as $p) {

      if (!isset($services[$p->getServizio()->getSlug()])) {
        $services[$p->getServizio()->getSlug()] = $p->getServizio()->getName();
      }

      if ($p->getStatus() == Pratica::STATUS_DRAFT) {
        $applications['draft'][] = $p;
      } elseif (in_array($p->getStatus(), [
        Pratica::STATUS_PRE_SUBMIT,
        Pratica::STATUS_SUBMITTED,
        Pratica::STATUS_REGISTERED,
        Pratica::STATUS_PENDING,
        Pratica::STATUS_PENDING_AFTER_INTEGRATION,
        Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE,
        Pratica::STATUS_REQUEST_INTEGRATION,
        Pratica::STATUS_REGISTERED_AFTER_INTEGRATION,
        Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE,
      ])) {
        $applications['pending'][] = $p;
      } elseif ($p->getStatus() == Pratica::STATUS_COMPLETE) {
        $applications['completed'][] = $p;
      } elseif ($p->getStatus() == Pratica::STATUS_CANCELLED) {
        $applications['cancelled'][] = $p;
      } elseif (in_array(
        $p->getStatus(),
        [Pratica::STATUS_DRAFT_FOR_INTEGRATION, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION]
      )) {
        $applications['integration'][] = $p;
      } elseif (in_array($p->getStatus(), [Pratica::STATUS_PAYMENT_PENDING, Pratica::STATUS_PAYMENT_OUTCOME_PENDING])) {
        $applications['payment_pending'][] = $p;
      } elseif ($p->getStatus() == Pratica::STATUS_WITHDRAW) {
        $applications['withdrawn'][] = $p;
      }
    }

    $praticheRelated = $repo->findRelatedPraticaForUser($user);
    if (count($praticheRelated) > 0) {
      $applications['related'] = $praticheRelated;
    }

    foreach ($applications as $k => $v) {
      if (empty($v)) {
        unset($applications[$k]);
      }
    }

    return $this->render('Pratiche/list.html.twig', [
      'user' => $user,
      'title' => 'lista_pratiche',
      'tab_pratiche' => $applications,
      'service_slug' => $serviceSlug,
      'services' => $services,
    ]);
  }

  /**
   * @Route("/{servizio}/draft", name="pratiche_list_draft")
   * @ParamConverter("servizio", class="App\Entity\Servizio", options={"mapping": {"servizio": "slug"}})
   *
   */
  public function listDraftByServiceAction(Servizio $servizio): Response
  {
    $user = $this->getUser();
    $repo = $this->entityManager->getRepository(Pratica::class);
    $pratiche = $repo->findBy(
      array(
        'user' => $user,
        'servizio' => $servizio,
        'status' => [
          Pratica::STATUS_DRAFT,
          Pratica::STATUS_DRAFT_FOR_INTEGRATION,
        ],
      ),
      array('creationTime' => 'ASC')
    );

    return $this->render('Pratiche/listDraftByService.html.twig', [
      'user' => $user,
      'pratiche' => $pratiche,
      'title' => 'bozze_servizio',
      'msg' => array(
        'type' => 'warning',
        'text' => 'msg_bozze_servizio',
      ),
    ]);
  }

  /**
   * @Route("/compila/{pratica}", name="pratiche_compila")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   */
  public function compilaAction(
    Request $request,
    Pratica $pratica,
    PraticaFlowRegistry $praticaFlowRegistry,
    FeatureManagerInterface $featureManager,
    PdndManager $pdndManager
  ): Response {

    if ($pratica->getStatus() !== Pratica::STATUS_DRAFT_FOR_INTEGRATION
      && $pratica->getStatus() !== Pratica::STATUS_DRAFT
      && $pratica->getStatus() !== Pratica::STATUS_PAYMENT_PENDING
      && !$pratica->hasStampsToPay()) {
      return $this->redirectToRoute(
        'pratiche_show',
        ['pratica' => $pratica->getId()],
      );
    }

    $handler = $this->servizioHandlerRegistry->getByName($pratica->getServizio()->getHandler());
    try {
      $handler->canAccess($pratica->getServizio());
    } catch (ForbiddenAccessException $e) {
      $this->addFlash('warning', $this->translator->trans($e->getMessage(), $e->getParameters()));

      return $this->redirectToRoute('pratiche');
    }

    $user = $this->getUser();
    $this->denyAccessUnlessGranted(
      ApplicationVoter::COMPILE,
      $pratica,
      "User can not compile application {$pratica->getId()}",
    );

    $this->breadcrumbsService->generateCompileApplicationBreadcrumbs($pratica->getServizio(), $request);

    /** @var PraticaFlow $praticaFlowService */
    $praticaFlowService = $praticaFlowRegistry->getByName($pratica->getServizio()->getPraticaFlowServiceName());

    if ($pratica->getServizio()->isPaymentRequired()) {
      $praticaFlowService->setPaymentRequired(true);
    }

    $pratica->setLocale($request->getLocale());

    $praticaFlowService->setInstanceKey($user->getId());

    $praticaFlowService->bind($pratica);

    if (null === $pratica->getInstanceId()) {
      $pratica->setInstanceId($praticaFlowService->getInstanceId());
    }

    $form = $praticaFlowService->createForm();
    if ($praticaFlowService->isValid($form)) {

      $currentStep = $praticaFlowService->getCurrentStepNumber();
      $praticaFlowService->saveCurrentStepData($form);
      $pratica->setLastCompiledStep($currentStep);

      if ($praticaFlowService->nextStep()) {

        $this->entityManager->flush();
        $form = $praticaFlowService->createForm();
        $this->praticaManager->saveAndDispatchEvent($pratica);
      } else {

        $this->entityManager->refresh($pratica);
        $pratica->setGeneratedSubject();

        $this->praticaManager->saveAndDispatchEvent($pratica);

        try {
          $this->praticaManager->fetchOrCreateBeneficiary($pratica);
          $praticaFlowService->onFlowCompleted($pratica);

          $this->praticaManager->collectProfileBlocks($pratica);

          $praticaFlowService->getDataManager()->drop($praticaFlowService);
          $praticaFlowService->reset();

          return $this->redirectToRoute(
            'pratiche_show',
            ['pratica' => $pratica->getId()],
          );
        } catch (Exception $exception) {
          $this->logger->error(
            "Unable to submit application {$pratica->getId()}: {$exception->getMessage()} --- {$exception->getTraceAsString()}",
          );
          $this->addFlash('error', $this->translator->trans('operatori.submit_error'));
        }
      }
    }

    $lastIntegrationMessage = null;

    $template = 'Pratiche/compila.html.twig';
    if ($pratica->getServizio()->isLegacy()) {
      $template = 'Pratiche/compileLegacy.html.twig';
      $repo = $this->entityManager->getRepository(Pratica::class);
      $requestIntegrationMessages = $repo->findStatusMessagesByStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);
      $lastIntegrationMessage = end($requestIntegrationMessages);
    }

    $pdndServiceConfigs = [];
    if ($featureManager->isActive('feature_pdnd')) {
      $pdndServiceConfigs = $pdndManager->getActiveServiceConfigs($pratica->getServizio());
    }

    return $this->render($template, [
      'form' => $form->createView(),
      'pratica' => $praticaFlowService->getFormData(),
      'integration_request_message' => $lastIntegrationMessage,
      'flow' => $praticaFlowService,
      'user' => $user,
      'pdnd_configs' => $pdndServiceConfigs,
    ]);
  }

  /**
   * @Route("/draft/{pratica}", name="pratiche_draft")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   *
   * @return Response
   *
   * @throws JsonException
   */
  public function draftAction(Request $request, Pratica $pratica)
  {
    if (Pratica::STATUS_DRAFT !== $pratica->getStatus()) {
      return new JsonResponse(['status' => 'Application status is not valid'], Response::HTTP_BAD_REQUEST);
    }

    $service = $pratica->getServizio();
    $schema = null;
    $result = $this->formServerService->getSchema($service);
    if ('success' === $result['status']) {
      $schema = $result['schema'];
    }

    $flatSchema = $this->praticaManager->flatSchema($schema);
    $cleanedData = StringUtils::cleanData($request->request->all());
    $flatData = $this->praticaManager->arrayFlat($cleanedData);

    $data = [
      'data' => $cleanedData,
      'flattened' => $flatData,
      'schema' => $flatSchema,
    ];

    try {
      $pratica->setDematerializedForms($data);

      $this->praticaManager->saveAndDispatchEvent($pratica);

      return new JsonResponse(['status' => 'ok']);

    } catch (ValidatorException $e) {

      $this->logger->error(
        "Received invalid dematerialized data for application {$pratica->getId()}: {$e->getMessage()}",
      );

      return new JsonResponse(['status' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

    } catch (Exception $e) {
      $this->logger->error("An error occurred while saving application {$pratica->getId()}: {$e->getMessage()}");

      return new JsonResponse(['status' => 'error'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * @Route("/{pratica}/calendar_event/{type}", name="pratiche_get_meeting_event")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   */
  public function downloadPraticaMeetingEventAction(Request $request, Pratica $pratica, ?string $type): Response
  {
    $this->denyAccessUnlessGranted(
      ApplicationVoter::VIEW,
      $pratica,
      "User can not read application {$pratica->getId()}"
    );

    try {
      $calendarEvent = new iCalUtils($pratica);

      return $calendarEvent->getMeetingEvent($type);
    } catch (Exception $e) {
      $this->logger->error(
        "An error occurred while trying to download the calendar event for the application {$pratica->getId()}: {$e->getMessage()}"
      );

      return new Response('error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * @Route("/{pratica}", name="pratiche_show")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   *
   * @throws Exception
   */
  public function showAction(Request $request, Pratica $pratica, PaymentService $paymentService): Response
  {
    if ($pratica->getStatus() == Pratica::STATUS_DRAFT) {
      return $this->redirectToRoute(
        'pratiche_compila',
        ['pratica' => $pratica->getId()]
      );
    }

    /** @var CPSUser $user */
    $user = $this->getUser();
    $this->denyAccessUnlessGranted(
      ApplicationVoter::VIEW,
      $pratica,
      "User can not read application {$pratica->getId()}"
    );

    $this->breadcrumbsService->generateServiceBreadcrumbs($pratica->getServizio(), $request);

    $result = [
      'pratica' => $pratica,
      'user' => $user,
      'formserver_url' => $this->getParameter('formserver_public_url'),
      'can_compile' => $this->isGranted(ApplicationVoter::COMPILE, $pratica),
      'can_withdraw' => $this->isGranted(ApplicationVoter::WITHDRAW, $pratica),
      'servizio' => $pratica->getServizio(),
      'servizi_correlati' => $this->serviceManager->getRelatedServices($pratica->getServizio()),
      'history' => $this->praticaManager->getApplicationHistory($pratica, false),
    ];

    if (!in_array($pratica->getPaymentType(), [MyPay::IDENTIFIER, Bollo::IDENTIFIER]) && $pratica->getServizio()->isPaymentRequired()) {
      $payment = $paymentService->getPaymentStatusByApplication($pratica);
      $result['payment'] = $payment;
    }


    $template = 'Pratiche/show.html.twig';
    if ($pratica->getServizio()->isLegacy()) {
      if ($pratica instanceof GiscomPratica) {
        $allegati = [];
        $attachments = $pratica->getAllegati();
        if (count($attachments) > 0) {

          /** @var Allegato $a */
          foreach ($attachments as $a) {
            $allegati[$a->getId()] = [
              'numero_protocollo' => $a->getNumeroProtocollo(),
              'id_documento_protocollo' => $a->getIdDocumentoProtocollo(),
              'description' => $a->getDescription(),
            ];
          }
        }

        $result['allegati'] = $allegati;
      }

      $template = 'Pratiche/showLegacy.html.twig';
    }

    return $this->render($template, $result);
  }

  /**
   * @Route("/{pratica}/detail", name="pratica_show_detail")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   */
  public function detailAction(Request $request, Pratica $pratica): Response
  {

    if ($pratica instanceof GiscomPratica) {
      return $this->redirectToRoute('pratiche_show', ['pratica' => $pratica]);
    }

    if (!$this->featureManager->isActive('feature_application_detail')) {
      return $this->redirectToRoute('pratiche_show', ['pratica' => $pratica]);
    }

    /** @var CPSUser $user */
    $user = $this->getUser();
    $this->denyAccessUnlessGranted(
      ApplicationVoter::VIEW,
      $pratica,
      "User can not read application {$pratica->getId()}"
    );
    $tab = $request->query->get('tab', false);

    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem(
      ucfirst($this->translator->trans('personal_area')),
      'user_dashboard'
    );
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem($this->translator->trans('nav.pratiche'), 'pratiche');
    $this->breadcrumbsService->getBreadcrumbs()->addItem($pratica->getServizio()->getName());

    $attachments = $this->entityManager->getRepository(Pratica::class)->getMessageAttachments(
      ['visibility' => Message::VISIBILITY_APPLICANT, 'author' => $pratica->getUser()->getId()],
      $pratica
    );

    $message = new Message();
    $message->setApplication($pratica);
    $message->setAuthor($user);

    $messageForm = $this->createForm(ApplicationMessageType::class, $message);
    $messageForm->handleRequest($request);

    if ($messageForm->isSubmitted() && $messageForm->isValid()) {
      /** @var Message $message */
      $message = $messageForm->getData();

      $detailUrl = $this->praticaManager->getApplicationDetailUrl($pratica, ['tab' => 'note']);
      $callToActions = [
        ['label' => 'view', 'link' => $detailUrl,],
        ['label' => 'reply', 'link' => $detailUrl,],
      ];

      $message->setProtocolRequired(false);
      $message->setVisibility(Message::VISIBILITY_APPLICANT);
      $message->setCallToAction($callToActions);

      try {
        $this->messageManager->save($message);
      } catch (BadRequestHttpException $e) {
        $this->addFlash('error', $e->getMessage());
      } catch (Exception $e) {
        $this->logger->error("Unable to save message for application {$pratica->getId()}: {$e->getMessage()}");
        $this->addFlash('error', $this->translator->trans('operatori.messaggi.save_error'));
      }

      return $this->redirectToRoute('pratica_show_detail', ['pratica' => $pratica, 'tab' => 'note']);
    }

    $repository = $this->entityManager->getRepository(Pratica::class);
    $praticheRecenti = $repository->findRecentlySubmittedPraticheByUser($pratica, $user, 5);

    // Recupero l'id del messaggio associato all'ultimo cambio di stato di richiesta integrazione
    $messages = $repository->findStatusMessagesByStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);
    $lastIntegrationMessage = end($messages);

    $result = [
      'pratiche_recenti' => $praticheRecenti,
      'applications_in_folder' => $repository->getApplicationsInFolder($pratica),
      'attachments_count' => $this->praticaManager->countAttachments($pratica),
      'messageAttachments' => $attachments,
      'messageForm' => $messageForm->createView(),
      'tab' => $tab,
      'pratica' => $pratica,
      'user' => $user,
      'formserver_url' => $this->getParameter('formserver_public_url'),
      'can_compile' => $this->isGranted(ApplicationVoter::COMPILE, $pratica),
      'can_withdraw' => $this->isGranted(ApplicationVoter::WITHDRAW, $pratica),
      'meetings' => $repository->findOrderedMeetings($pratica),
      'module_files' => $this->praticaManager->getGroupedModuleFiles($pratica),
      'last_owner_message' => $repository->getLastMessageByApplicationOwner($pratica),
      'integration_request_message' => $lastIntegrationMessage,
      'isModuleUpdating' => $this->praticaManager->isModuleUpdating($pratica),
      'history' => $this->praticaManager->getApplicationHistory($pratica, false),
      'has_gateway_with_digital_stamps' => $this->serviceManager->hasGatewayWithDigitalStamps($pratica->getServizio()),
    ];


    return $this->render('Pratiche/detail.html.twig', $result);
  }

  /**
   * @Route("/{pratica}/withdraw", name="pratiche_withdraw")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   * @throws Exception
   *
   * @deprecated Deprecated since 2.48.0, use Api transition instead
   */
  public function withdrawAction(Request $request, Pratica $pratica): RedirectResponse
  {
    if ($this->isGranted(ApplicationVoter::WITHDRAW, $pratica)) {
      try {
        $withdrawAttachment = $this->pdfBuilderService->createWithdrawForPratica($pratica);
        $pratica->addAllegato($withdrawAttachment);
        $this->praticaStatusService->setNewStatus(
          $pratica,
          Pratica::STATUS_WITHDRAW
        );

        $this->addFlash(
          'success',
          $this->translator->trans('operatori.pratica_annullata', ['%id%' => $pratica->getId()])
        );

        return $this->redirectToRoute('pratiche_list');
      } catch (Exception $ex) {
        $this->addFlash('error', $this->translator->trans('operatori.error_withdraw', ['%id%' => $pratica->getId()]));
        $this->logger->error("Unable to withdraw application {$pratica->getId()}: {$ex->getMessage()}");
      }
    }

    return $this->redirectToRoute(
      'pratica_show_detail', ['pratica' => $pratica->getId()]
    );
  }

  /**
   * @Route("/{pratica}/payment-callback", name="pratiche_payment_callback")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   *
   * @throws Exception
   */
  public function paymentCallbackAction(Request $request, Pratica $pratica): RedirectResponse
  {
    $this->denyAccessUnlessGranted(
      ApplicationVoter::VIEW,
      $pratica,
      "User can not read application {$pratica->getId()}"
    );

    $parameters = [
      'pratica' => $pratica,
    ];

    switch ($pratica->getPaymentType()) {
      case MyPay::IDENTIFIER:
        $parameters['payment_outcome'] = $request->get('esito', 'KO');
        if ($parameters['payment_outcome'] === 'OK') {
          // Se ho esito positivo la pratica va in attesa di esito
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_PAYMENT_OUTCOME_PENDING
          );
        }
        break;
      case Bollo::IDENTIFIER:
        // Niente da fare
        break;
      default:
        // Pratiche con nuovo sistema di pagamento
        // La pratica viene comunque inviata, in caso di errore verrà mostrato il link all'area pagamenti
        // se l'esito non viene fornito viene cominicato di verificarne lo stato via email
        $this->praticaManager->finalizeSubmission($pratica);
        if ($request->query->has('payment')) {
          $payment = $request->get('payment');
          $parameters['payment'] = $payment;
          $parameters['payment_outcome'] = $payment;
        }
        break;
    }

    return $this->redirectToRoute('pratiche_show', $parameters);
  }

  /**
   * @Route("/{pratica}/delete", name="pratiche_delete")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   */
  public function deleteAction(
    Pratica $pratica,
    PraticaManager $praticaManager
  ): RedirectResponse {

    $this->denyAccessUnlessGranted(
      ApplicationVoter::DELETE,
      $pratica,
      "User can not delete application {$pratica->getId()}",
    );

    try {

      $praticaManager->delete($pratica);

    } catch (ApplicationException $e) {

      throw new UnauthorizedHttpException($e->getMessage());
    }

    return $this->redirectToRoute('pratiche_list');
  }

  /**
   * @Route("/formio/validate/{servizio}", name="formio_validate")
   * @ParamConverter("servizio", class="App\Entity\Servizio", options={"mapping": {"servizio": "slug"}})
   */
  public function formioValidateAction(Request $request, Servizio $servizio): JsonResponse
  {
    $validator = $this->expressionValidator;

    $errors = $validator->validateData(
      $servizio,
      $request->getContent()
    );

    $response = ['status' => 'OK', 'errors' => null];
    if (!empty($errors)) {
      $response = ['status' => 'KO', 'errors' => $errors];
    }

    return new JsonResponse($response, Response::HTTP_OK);
  }
}
