#!/usr/bin/env bash
# vim: autoindent tabstop=2 shiftwidth=2 expandtab softtabstop=2 fileencoding=utf-8

[[ -n $DEBUG ]] && set -x

#ENV='PROD'
max_retry=${MAX_RETRY:-10}
number_of_scheduled_actions=${MAX_ACTIONS:-10}
sleep_time=${SLEEP_TIME:-2}
max_executions=${MAX_EXECUTIONS:-100}
max_execution_time=${MAX_EXECUTION_TIME:-1800}

swarm_service=${SWARM_SERVICE:?'Missing swarm service variable'}
swarm_stack=${SWARM_STACK:?'Missing swarm stack variable'}

pushgw=${PUSHGW_ENDPOINT:-http://172.18.0.1:9091}
aggate=${AGGATE_ENDPOINT:-http://172.18.0.1:9080}
docker_endpoint=${DOCKER_API:-http://dockersocket:2375}

exit_required=0			# breaker of the infinite loop
function graceful_exit {
  echo "$(date +'%F %H:%M:%S') Got termination signal (SIG_QUIT)"
	exit_required=1
}
function graceful_kill {
	echo "$(date +'%F %H:%M:%S') Got KILL signal"
	exit_required=1
}
function graceful_term {
	echo "$(date +'%F %H:%M:%S') Got TERM signal"
	exit_required=1
}

trap graceful_exit SIGQUIT
trap graceful_term SIGTERM
trap graceful_kill SIGKILL

# send a metric to prometheus pushgateway
function promSend {
  local name=$1
  local metric_type=$2
  local value=$3
  local tenant=$4
  local instance=$5
	local job=$6
  local env=${ENV:-unknown}

  cat << EOF | curl --connect-timeout 5 --max-time 5 -s -o /dev/null --data-binary @- ${pushgw}/metrics/job/${job}/env/${env}/tenant/${tenant}/swarm_stack/${swarm_stack}/swarm_service/${swarm_service}/instance/$instance
# TYPE $name $metric_type
$name $value
EOF
  if [[ $? -gt 0 ]]; then
    echo "Error sending metric to pushgw @172.18.0.1:9091: /job/${job}/env/${env}/tenant/${tenant}"
  fi
}

function promCount {
  local name=$1
  local metric_type=$2
  local value=$3
  local tenant=$4
  local instance=$5
	local job=$6
  local env=${ENV:-unknown}

  cat << EOF | curl --connect-timeout 5 --max-time 5 -s -o /dev/null --data-binary @- ${aggate}/metrics/
# TYPE $name $metric_type
$name{job="${job}", env="${env}", tenant="${tenant}", swarm_stack="${swarm_stack}", swarm_service="${swarm_service}", instance="$instance"} $value
EOF
  if [[ $? -gt 0 ]]; then
    echo "Error sending metric to aggate @172.18.0.1:9080: /job/${job}/env/${env}/tenant/${tenant}"
  fi
}

function safeRun()
{
  local exit_status

  #echo "Running command safely: max_execution_time=${max_execution_time}"
  timeout ${max_execution_time} $*
  exit_status=$?
  if [[ $exit_status -gt 0 ]]; then
    echo "$line_prefix Command exited with status $exit_status"
  fi
}

sleep 30


# se passo al worker un ID sto staticamente facendo un calcolo
# di quanti nodi faccio girare
# altrimenti sto cercando di renderli autonomi in base al numero di
# replicas impostate nel deploy
if [[ -n $WORKER_NUM ]]; then
  available_workers=$WORKER_NUM
fi
if [[ -n $WORKER_ID ]]; then
  slot_id=$WORKER_ID
else
  # la ricerca si basa sul fatto che l'$HOSTNAME del container è uguale ai primi 12 caratteri del ContainerID
  # sapendo questo posso prendere il task ID elencando tra tutti i task quello che
  # combacia con l'HOSTNAME del container
  tasks_description=$(http GET "${docker_endpoint}/v1.24/tasks?filters={\"service\":[\"${swarm_stack}_${swarm_service}\"],\"desired-state\":[\"running\"]}")
  if [[ $? -gt 0 ]]; then
    echo "Cannot get tasks_description from swarm API, cannot continue"
    echo "Task description: $tasks_description"
    exit 1
  fi
  slot_id=$(echo $tasks_description | jq -r ".[]| select(.Status.ContainerStatus.ContainerID | startswith(\"$HOSTNAME\")) | .Slot")
  if [[ -z $slot_id ]]; then
    echo "Cannot get my slot id looking for tasks matching my hostname '$HOSTNAME'..."
    echo "Task description: $tasks_description"
    exit 1
  fi

  # numero di tasks che sono previsti, solo se non già passati dal worker da env
  if [[ -z $available_workers ]]; then
    available_workers=$(echo $tasks_description | jq -r ".[].Slot" | wc -l)
  fi

  echo "[$slot_id/$available_workers] Workers setup: $available_workers workers scheduled. I'm worker $slot_id"
fi

if [[ -z $slot_id ]]; then
  echo "Cannot find slot_id"
  exit 1
fi

if [[ -z $available_workers ]]; then
  echo "Cannot find workers number"
  exit 2
fi

if [[ $slot_id -gt $available_workers ]]; then
  echo "Slot ID is greater than available_workers, something wrong is happening, retry later"
  sleep 30
  exit 3
fi

# tenant della stanza che devono essere serviti
tenants=( $(./bin/tenants) )

# numero di tenant
tenants_total=${#tenants[@]}

if [[ $tenants_total -lt 1 ]]; then
  echo "Cannot find tenants"
  exit 1
fi

# trovo il numero di tenant che dovrà servire ogni container
# arrotondando per eccesso se non sono multipli del numero di workers

if [[ $tenants_total -lt $available_workers ]]; then
  echo "Warning: $tenants_total tenants for $available_workers workers, reduce your workers pool"
fi

slot_modulo=$(( $tenants_total % $available_workers ))
if [[ $slot_modulo -gt 0 ]]; then
  slot_size=$(( $tenants_total / $available_workers + 1 ))
else
  slot_size=$(( $tenants_total / $available_workers ))
fi


# qui potrei elencare il contenuto di tutti gli workers
#for((i=0; i < ${#tenants[@]}; i+=slot_size)); do
#  part=( "${tenants[@]:i:slot_size}" )
#  echo "Elements in slot $((i+1)) group: ${part[*]}"
#done

# trovo i tenant che deve servire questo container

# slot iniziano ad 1, mentre l'array conta da 0
slot_counter=$(( slot_id - 1 ))

# individuo gli estremi dell'intervallo da prendere dall'array dei tenant
begin=$(( slot_counter * slot_size - 1 ))
end=$(( begin + slot_size - 1 ))
if (( begin < 0 )); then
  begin=0
fi

# slice dell'array
mytenants=( "${tenants[@]:begin:slot_size}" )

line_prefix="[${slot_id}/${available_workers}]"
#                 real interval values              vvvvvv     vvvvvvvv

#echo "$line_prefix DEBUG tenants:"

echo "$line_prefix [DEBUG] $slot_size/$tenants_total tenants in charge ($((begin+1)):$((end+1))): ${mytenants[*]}"

# small randomization starting time
sleep $(( ( RANDOM % 10 )  + 1 ))

let -i executions_counter=0
#let -a cache_cleared=()
echo "$line_prefix Worker starting infinite loop on assigned tenants"
while (( exit_required != 1)); do


  if [[ -n $HC_PING_UUID ]]; then
    curl -fsS -m 10 --retry 5 -o /dev/null https://hc-ping.com/${HC_PING_UUID}
  fi

  for ((j=0; j < ${#mytenants[@]}; j++)); do

    tenant=${mytenants[j]}
    echo "$line_prefix ==> $tenant -----------------------------------------------------------------------"

    # svuotiamo la cache una tantum all'inizio
    if [[ ${cache_cleared[$j]} -eq 1 ]]; then
      echo "$line_prefix Cache clear not required for tenant '${tenant}', skipped."
    else
      #./bin/console --no-interaction -e prod --instance $tenant cache:clear
      # come sopra e' troppo lento, proviamo metodo basso livello
      echo "$line_prefix Cache cleared for tenant '${tenant} (setting flag to 1)'."
      [[ -d var/cache/${tenant} ]] && rm -rf var/cache/${tenant} && cache_cleared[$j]=1
    fi

		BEGIN=$(date +%s)
    safeRun ./bin/console --no-interaction -e prod --instance $tenant ocsdc:delete-draft-meetings

		if [[ $? -eq 0 ]]; then
			promSend job_exit_code gauge 0 $tenant www2 delete-draft-meetings
		else
			promCount job_errors_total counter 1 $tenant www2 delete-draft-meetings
			promSend job_exit_code gauge 1 $tenant www2 delete-draft-meetings
		fi
		promCount job_total counter 1 $tenant www2 delete-draft-meetings
		END=$(date +%s)
		promSend job_last_run_seconds counter ${END} $tenant www2 delete-draft-meetings
		promSend job_duration_seconds counter $((${END} - ${BEGIN})) $tenant www2 delete-draft-meetings

		BEGIN=$(date +%s)
		safeRun ./bin/console --no-interaction -e prod --instance $tenant o:s:e -o 60 --max-retry $max_retry --count $number_of_scheduled_actions;

		if [[ $? -eq 0 ]]; then
			promSend job_exit_code gauge 0 $tenant www2 scheduled-actions
		else
			promCount job_errors_total counter 1 $tenant www2 scheduled-actions
			promSend job_exit_code gauge 1 $tenant www2 scheduled-actions
		fi
		promCount job_total counter 1 $tenant www2 scheduled-actions
		END=$(date +%s)
		promSend job_last_run_seconds counter ${END} $tenant www2 scheduled-actions
		promSend job_duration_seconds counter $((${END} - ${BEGIN})) $tenant www2 scheduled-actions


		BEGIN=$(date +%s)
		safeRun ./bin/console --no-interaction -e prod --instance $tenant ocsdc:create_subscription_payment_drafts;

		if [[ $? -eq 0 ]]; then
			promSend job_exit_code gauge 0 $tenant www2 create_subscription_payment_drafts
		else
			promCount job_errors_total counter 1 $tenant www2 create_subscription_payment_drafts
			promSend job_exit_code gauge 1 $tenant www2 create_subscription_payment_drafts
		fi
		promCount job_total counter 1 $tenant www2 create_subscription_payment_drafts
		END=$(date +%s)
		promSend job_last_run_seconds counter ${END} $tenant www2 create_subscription_payment_drafts
		promSend job_duration_seconds counter $((${END} - ${BEGIN})) $tenant www2 create_subscription_payment_drafts


    if (( exit_required == 1 )); then
			echo "$line_prefix $(date +'%F %H:%M:%S') Gracefully finished jobs on tenant ${tenant}, now exiting..."
			break
		fi

		echo "${line_prefix} sleeping..."
  	sleep $sleep_time

  done

  if (( executions_counter >= max_executions )); then
   	echo "$line_prefix $(date +'%F %H:%M:%S') Maximum executions limit reached, gracefully finished jobs on tenant ${tenant}, now exiting..."
   	break
  else
   	executions_counter=$(( executions_counter + 1 ))
  fi

done

echo -n "$line_prefix Worker exited from infinite loop @ "
echo $(date +'%F %H:%M:%S')
