<?php


namespace App\Form\Base;


use App\Model\Payment\PaymentPhase;
use App\Model\Service\PaymentConfig;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PaymentConfigType extends AbstractType
{

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('id', TextType::class, [
        'label' => false,
        'required' => true
      ])
      ->add('phase', ChoiceType::class, [
        'required' => true,
        'choices' => [
          PaymentPhase::PHASE_REQUEST => PaymentPhase::PHASE_REQUEST,
          PaymentPhase::PHASE_RELEASE => PaymentPhase::PHASE_RELEASE,
        ]
      ]);
  }


  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => PaymentConfig::class,
      'csrf_protection' => false
    ));
  }
}
