<?php

declare(strict_types=1);

namespace App\Handlers\Servizio;

use Exception;
use Throwable;
class ForbiddenAccessException extends Exception
{
  private $parameters = [];

  public function __construct($message = "", $parameters = [], $code = 0, Throwable $previous = null)
  {
    $this->parameters = $parameters;
    parent::__construct($message, $code, $previous);
  }

  public function getParameters(): array
  {
    return $this->parameters;
  }

}
