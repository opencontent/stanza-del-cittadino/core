<?php

namespace App\Services;

use App\Entity\Ente;
use App\Entity\Servizio;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class InstanceService
{

  private string $instance;

  private EntityManagerInterface $entityManager;

  /**
   * TermsAcceptanceCheckerService constructor.
   * @param EntityManagerInterface $doctrine
   * @param string $instance
   */
  public function __construct(EntityManagerInterface $doctrine, string $instance)
  {
    $this->entityManager = $doctrine;
    $this->instance = $instance;
  }

  public function getInstanceIdentifier(): string
  {
    return $this->instance;
  }

  public function hasInstance(): bool
  {
    return !empty($this->instance);
  }

  /**
   * @return Ente
   * @throws RuntimeException
   */
  public function getCurrentInstance(): Ente
  {
    if (empty($this->instance)) {
      throw new RuntimeException("Ente not configured");
    }

    $repo = $this->entityManager->getRepository(Ente::class);
    $ente = $repo->findOneBy(array('slug' => $this->instance));
    if (!$ente instanceof Ente) {
      throw new RuntimeException("Ente $this->instance not found");
    }

    return $ente;
  }

  /**
   * @return array
   */
  public function getServices(): array
  {
    return $this->getCurrentInstance()->getServices()->toArray();
  }
}
