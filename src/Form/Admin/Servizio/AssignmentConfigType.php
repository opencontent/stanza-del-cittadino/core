<?php


namespace App\Form\Admin\Servizio;

use App\Form\I18n\AbstractI18nType;
use App\Form\I18n\I18nDataMapperInterface;
use App\Model\Service\AssignmentConfig;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Servizio;

class AssignmentConfigType extends AbstractI18nType
{
  private RequestStack $requestStack;
  private EntityManagerInterface $entityManager;

  /**
   * @param I18nDataMapperInterface $dataMapper
   * @param $locale
   * @param $locales
   * @param RequestStack $requestStack
   * @param EntityManagerInterface $entityManager
   */
  public function __construct(I18nDataMapperInterface $dataMapper, $locale, $locales, RequestStack $requestStack, EntityManagerInterface $entityManager)
  {
    parent::__construct($dataMapper, $locale, $locales);
    $this->requestStack = $requestStack;
    $this->entityManager = $entityManager;
  }

  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));

  }

  public function onPreSubmit(FormEvent $event): void
  {
    /** @var Servizio $service */
    $service = $event->getForm()->getData();
    $request = $this->requestStack->getCurrentRequest()->request;

    $assignment = (array)$request->get('assignment');

    $customAssignment = $request->get('custom_assignment');

    if (!empty($customAssignment)) {
      foreach ($customAssignment as $c) {
        $assignment['custom_map'][$c['value']] = $c['office'];
      }
    }

    $service->setAssignmentConfig(AssignmentConfig::fromArray($assignment));
    $this->entityManager->persist($service);
    $this->entityManager->flush();
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => Servizio::class
    ));
    $this->configureTranslationOptions($resolver);
  }

  public function getBlockPrefix(): string
  {
    return 'assignment_config';
  }
}
