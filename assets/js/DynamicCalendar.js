import datepickerFactory from "jquery-datepicker";
import ionRangeSlider from "ion-rangeslider";
import "ion-rangeslider/css/ion.rangeSlider.min.css";
import Base from "formiojs/components/_classes/component/Component";
import editForm from "./DynamicCalendar/DynamicCalendar.form";
import moment from "moment";
import { i18nDatepicker } from "./translations/i18n-datepicker";
import { CountDownTimer } from "./components/Countdown";
import _ from "lodash";
import Swal from 'sweetalert2/src/sweetalert2.js'
import Auth from "./rest/auth/Auth";
datepickerFactory($);

export default class FormioCalendar extends Base {
  constructor(component, options, data) {
    super(component, options, data);
    this.date = false;
    this.slot = false;
    this.available_slot = false;
    this.container = false;
    this.calendar = null;
    this.available_days = [];
    this.meeting = null;
    this.meeting_expiration_time = null;
    this.opening_hour = null;
    this.min_duration = null;
    this.first_available_date = null;
    this.first_available_start_time = null;
    this.first_available_end_time = null;
    this.first_availability_updated_at = null;
    this.$language = document.documentElement.lang.toString();
    this.countdown = null;
    this.multipleDate = false;
    this.selectedNextSlots = [];
    this.prevDateSelected = null;
    this.auth = new Auth();
    this.loaderTpl = `<div id="loader" class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw"></i><span class="sr-only">${Translator.trans(
      "loading",
      {},
      "messages",
      this.$language
    )}</span></div>`;
  }

  static schema() {
    return Base.schema({
      type: "dynamic_calendar",
    });
  }

  static builderInfo = {
    title: "Dynamic Calendar",
    group: "basic",
    icon: "calendar-plus-o",
    weight: 70,
    schema: FormioCalendar.schema(),
  };

  static editForm = editForm;

  /**
   * Render returns an html string of the fully rendered component.
   *
   * @param children - If this class is extended, the sub string is passed as children.
   * @returns {string}
   */
  render(children) {
    // To make this dynamic, we could call this.renderTemplate('templatename', {}).
    let calendarClass = "";
    let content = this.renderTemplate("input", {
      input: {
        type: "input",
        ref: `${this.component.key}-selected`,
        attr: {
          id: `${this.component.key}`,
          class: "form-control",
          type: "hidden",
        },
      },
    });

    let translateLabel = this.options.i18n.resources[this.$language]

    if (this.component.multiple) {
    return super.render(`
      <div id="calendar-container-${this.id}" class="slot-calendar">
          <div class="row">

              <div class="col-12">
                  <h6>${translateLabel?.translation[this.component.label] || this.component.label}</h6>

                  <div class="date-picker"></div>
                  <div id="date-picker-print" class="mt-3 d-print-block d-preview-calendar"></div>
                  <div class="mb-3" id="selectedNextDate"></div>
                  <p id="label-${this.id}" class="d-none">${Translator.trans('calendar_formio.select_day_to_add_booking', {}, 'messages', this.$language)}</p>
              </div>
              <div id="modal-${this.id}" class="modal modal-calendar">
                <div class="modal-content modal-content-calendar">
                <div class="modal-header-calendar">
                    <div  id="close-modal-${this.id}" class="close-calendar">&times;</div>
                    <h5 class="modal-title mb-1">${Translator.trans("calendar_formio.add_booking", {}, "messages", this.$language)}</h5>
                    <div class="title-xsmall u-grey-dark">${Translator.trans("calendar_formio.select_slot_and_confirm", {}, "messages", this.$language)}</div>
                    </div>
                    <div class="modal-body-calendar">
                    <div class="row mx-0" id="slot-picker"></div>
                    <div id="slot-picker" class="mt-3 d-print-block d-preview-calendar"></div>
                    <div id="range-picker" class="my-1"></div>
                    ${content}
                    </div>
                    <div class="modal-footer modal-footer-calendar"><button id="close-btn-modal-${this.id}" type="button" class="btn btn-secondary">${Translator.trans("close", {}, "messages", this.$language)}</button><button id="save-btn-modal-${this.id}" type="button" class="btn btn-primary">${Translator.trans("meetings.modal.confirm", {}, "messages", this.$language)}</button></div>
                </div>
            </div>
          </div>
      </div>

      
      <div class="mt-3 d-print-none d-preview-none" id="draft-expiration-dynamic-container">
      <div id="draft-expiration-dynamic" class="d-print-none d-preview-none"></div>
      <span id="draft-expiration-dynamic-countdown" class="font-weight-bolder"></span>
      </div>

      
    <style>
     /* Modal Styles */
        .modal-calendar {
            display: none;
            background-color: rgba(0, 0, 0, 0.5);
        }

        .modal-content-calendar {
            border-radius: 0.3rem;
            width: 100%;
            max-width: 600px;
            height: 75%;
            margin: 6rem auto;
        }

        .modal-header-calendar {
          padding: 1.25rem;
          position: relative;
        }

        .modal-body-calendar {
          padding: 0 1.25rem;
          overflow-y: auto;
          flex-grow: 1;
        }

        .close-calendar {
            color: #aaa;
            font-size: 28px;
            font-weight: bold;
            position: absolute;
            right: 20px;
        }

        .close-calendar:hover,
        .close-calendar:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
        #selectedNextDate .it-list {
          list-style-type: none !important;
        }
        .calendar-last-selection {
          background-color: #F2F7FC;
        }
      </style>
      `);
    }

    // Calling super.render will wrap it html as a component.
    return super.render(`
        <div id="calendar-container-${this.id}" class="slot-calendar">
            <div class="row">
                <div class="col-12 col-md-6">
                    <h6>${translateLabel?.translation[this.component.label] || this.component.label}</h6>
                    <div class="date-picker"></div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row" id="slot-picker"></div>
                </div>
            </div>
        </div>

        <div id="slot-picker" class="mt-3 d-print-block d-preview-calendar"></div>
        <div id="range-picker" class="my-5"></div>
        ${content}
        <div id="date-picker-print" class="mt-3 d-print-block d-preview-calendar"></div>
        
        <div class="mb-5" id="selectedNextDate"></div>
        <div class="mt-3 d-print-none d-preview-none" id="draft-expiration-dynamic-container">
        <div id="draft-expiration-dynamic" class="d-print-none d-preview-none"></div>
        <span id="draft-expiration-dynamic-countdown" class="font-weight-bolder"></span>
        </div>
        `);
  }

  /**
   * After the html string has been mounted into the dom, the dom element is returned here. Use refs to find specific
   * elements to attach functionality to.
   *
   * @param element
   * @returns {Promise}
   */
  attach(element) {
    let self = this,
      calendarID
    if(this.component.calendarId){
      calendarID = typeof this.component.calendarId === 'object' ? this.component.calendarId : [this.component.calendarId]
    }

    if (this.calendarIdSelected && !(_.isEqual(this.calendarIdSelected, calendarID))) {
      if (this.component.multiple && this.selectedNextSlots && this.selectedNextSlots.length > 0) {
        this.selectedNextSlots.map((day, index) => {
          self.removeNextData(index);
        })
        this.selectedNextSlots = [];
      }
    }

    this.container = $(`#calendar-container-${this.id}`);

    // override default values calendar
    $.datepicker.regional[self.$language] = i18nDatepicker[self.$language];
    $.datepicker.setDefaults($.datepicker.regional[self.$language]);

      if (calendarID && calendarID.length &&  !this.disabled) {
        if (this.component.multiple) {
          const label = $(`#label-${this.id}`)
          if (label) {
            label.removeClass("d-none");
          }
        }
        
        let date = moment().format("YYYY-MM-DD")
      $.ajax(self.getMonthlyAvailabilitiesUrl(date), {
        dataType: "json", // type of response data
        beforeSend: function () {
          self.container.find(".date-picker").append(self.loaderTpl);
        },
        success: function (data, status, xhr) {
          // success callback function
          self.getFirstAvailableSlot(data);

          $("#loader").remove();
          self.calendar = self.container.find(".date-picker").datepicker({
            minDate: new Date(
              data.data.sort((a, b) => a.date.localeCompare(b.date))[0]
            ),
            firstDay: undefined,
            dateFormat: "dd-mm-yy",
            onSelect: function (dateText) {
              if (self.component.multiple) {
                self.openModal();
              }
              
              if (dateText !== self.date) {
                // If date changed, reset slot choice
                self.available_slot = false;
                self.slot = false;
                self.opening_hour = false;
                self.min_duration = false;
                self.updateValue();
              }
              self.date = dateText;
              self.getDaySlots();

              $("#range-picker").html("");
              self.renderData();
            },
            beforeShowDay: function (date) {
              const string = jQuery.datepicker.formatDate("yy-mm-dd", date);
              if(Object.keys(self.available_days).length !== 0 && Array.isArray(self.available_days)){
                if (self.available_days.some((e) => e.available === false && e.date === string)) {
                  return [
                    false,
                    "disabled",
                    Translator.trans("calendar_formio.unavailable", {}, "messages", self.$language),
                  ];
                } else {
                  return [self.available_days.some((e) => e.date === string)];
                }
              }else{
                if (data.data.some((e) => e.available === false && e.date === string)) {
                  return [
                    false,
                    "disabled",
                    Translator.trans("calendar_formio.unavailable", {}, "messages", self.$language),
                  ];
                } else {
                  return [data.data.some((e) => e.date === string)];
                }
              }
            },
            onChangeMonthYear: function (year, month, inst){
              const event = new Date(year, month - 1, 1);
              const date = moment(event).format("YYYY-MM-DD")

              $.ajax(self.getMonthlyAvailabilitiesUrl(date), {
                dataType: "json", // type of response data
                beforeSend: function () {
                  self.container.find(".date-picker").append(self.loaderTpl);
                },
                success: function (data, status, xhr) {
                  const prevFirstAvailable = self.first_available_date
                  self.getFirstAvailableSlot(data);
                  self.available_days = data.data

                  $("#loader").remove();
                  self.calendar = self.container.find(".date-picker").datepicker({
                    minDate: new Date(
                      data.data.sort((a, b) => a.date.localeCompare(b.date))[0]
                    ),
                    firstDay: self.component.multiple ? undefined : 1,
                    dateFormat: "dd-mm-yy",
                  });
                  
                  let parsedDate = null

                  if (prevFirstAvailable !== self.first_available_date) {
                    parsedDate = moment(self.first_available_date).format("DD-MM-YYYY");
                  }else{
                    parsedDate = moment(date).format("DD-MM-YYYY");
                  }
                  if(!self.component.multiple) {
                    self.date = parsedDate;
                  }
                  self.calendar.datepicker("setDate", parsedDate);
                  self.getDaySlots(parsedDate);
                },
                error: function (jqXhr, textStatus, errorMessage) {
                  // error callback
                  Swal.fire(
                    `${Translator.trans('error_message_detail', {}, 'messages', self.$language)}`,
                    `${Translator.trans('calendar_formio.availability_error', {}, 'messages', self.$language)}`,
                    'error'
                  );
                },
                complete: function () {
                  //Auto-click current selected day
                  var dayActive = $("a.ui-state-active");
                  if (!self.date && dayActive.length > 0 && !self.component.multiple) {
                    dayActive.click();
                  }
                  self.renderData();
                },
              })}
          });

          if (self.date) {

            let parsedDate = moment(self.date, "DD-MM-YYYY");
            self.calendar.datepicker("setDate", parsedDate.toDate());
          } else if (self.first_available_date && !self.component.multiple) {
            self.calendar.datepicker("setDate", moment(self.first_available_date, 'YYYY-MM-DD').toDate());
          }
        },
        error: function (jqXhr, textStatus, errorMessage) {
          // error callback
          Swal.fire(
            `${Translator.trans('error_message_detail', {}, 'messages', self.$language)}`,
            `${Translator.trans('calendar_formio.availability_error', {}, 'messages', self.$language)}`,
            'error'
          );
        },
        complete: function () {
          //Auto-click current selected day
          var dayActive = $("a.ui-state-active");
          if (!self.date && dayActive.length > 0 && !self.component.multiple) {
            dayActive.click();
          }
          self.renderData();
        },
      });
    } else {
      this.renderData()
    }

    const save_btn = $(`#save-btn-modal-${this.id}`);
    const close_btn = $(`#close-btn-modal-${this.id}`);
    const close = $(`#close-modal-${this.id}`);
    
    if (save_btn && close_btn && close) {
      save_btn.on("click", function (e) {
        self.createOrUpdateMeeting(null, true);
        self.renderData();
        self.closeModal();
      })
  
      close_btn.on("click", function (e) {
        self.closeModal();
      })
  
      close.on("click", function (e) {
        self.closeModal();
      })
    }


    // Allow basic component functionality to attach like field logic and tooltips.
    return super.attach(element);
  }

  /**
   * Get the value of the component from the dom elements.
   *
   */
  getValue() {
    if (!(this.date && this.slot && this.meeting && this.opening_hour) && !(this.selectedNextSlots && this.selectedNextSlots.length > 0)) {
      // Unset value (needed for calendars with 'required' option"
      return null;
    }
    let meeting_id = this.meeting ? this.meeting : "";
    let opening_hour = this.opening_hour ? this.opening_hour : "";

    if (this.component.multiple) {
      return this.selectedNextSlots
    }
    return {
      "date": moment(this.date, "DD-MM-YYYY").format("YYYY-MM-DD"),
      "slot": this.slot,
      "calendar_id": this.calendarIdSelected,
      "meeting_id": meeting_id,
      "opening_hour_id": opening_hour,
    }
  }

  /**
   * Set the value of the component into the dom elements.
   *
   * @param val
   */
  setValue(val) {
    if (!val) {
      return;
    }
    let value = val
    if (Array.isArray(val)) {
      value = val[0]
      this.selectedNextSlots = [];
      val.map((v,i) => {
          this.selectedNextSlots.push({
            ...v,
            date: v.date
          })
      })
    } else {
      if ( typeof value === 'object' ) {
        this.date = moment(value.date, "YYYY-MM-DD").format("DD-MM-YYYY");
        this.slot = value.slot;
        this.meeting = value.meeting_id;
        this.meeting_expiration_time = null;
        this.opening_hour = value.opening_hour_id ? value.opening_hour_id : "";
      } else {
        let explodedValue = value
          .replace(")", "")
          .replace(" (", " @ ")
          .replace(/\//g, "-")
          .split(" @ ");
        let explodedCalendar = explodedValue[2].split("#");
        this.date = explodedValue[0];
        this.slot = explodedValue[1];
        this.meeting = explodedCalendar[1];
        this.meeting_expiration_time = null;
        this.opening_hour =
          explodedCalendar.length === 3 ? explodedCalendar[2] : "";
      }
    }
    this.renderData()
  }

  renderData() {
    if (this.date && this.slot) {
      let slotText = this.slot ? ` ${Translator.trans("calendar_formio.at_hours", {}, "messages", this.$language)} ${this.slot}` : "";
      $(`#label-${this.id}`).html(`${Translator.trans(
        this.selectedNextSlots.length > 0 ? 'calendar_formio.select_day_to_add_new_booking' : 'calendar_formio.select_day_to_add_booking',
        {},
          "messages",
          this.$language
        )}`);
      $("#date-picker-print").html(
        `<b>${Translator.trans(
          this.selectedNextSlots.length > 1 ? "calendar_formio.days_selected" : "calendar_formio.day_selected",
          {},
          "messages",
          this.$language
        )}: </b> 
        <div class="it-list-wrapper${this.component.multiple ? " d-none" : ""}">
          <ul class="it-list">
            <li>
            <div class="list-item">
              <div class="it-right-zone">
                <span class="text pl-4">${this.date} ${slotText}</span>
              </div>
            </div>
          </li>
          </ul>
        </div>`
      );
    } else if (this.component.multiple && this.disabled) {
      $("#date-picker-print").html(
        `<b>${Translator.trans(
          this.selectedNextSlots.length > 1? "calendar_formio.days_selected" : "calendar_formio.day_selected",
          {},
          "messages",
          this.$language
        )}: </b> `)
    }
    if (this.meeting_expiration_time) {
      $("#draft-expiration-dynamic").html(
        `<i>${Translator.trans(
          "calendar_formio.draft_expiration_text",
          {},
          "messages",
          this.$language
        )}</i>`
      );
      $("#draft-expiration-dynamic-container").addClass("alert alert-info");
      if (!this.countdown) {
        this.countdown = new CountDownTimer(this.meeting_expiration_time, "draft-expiration-dynamic-countdown");
      }
    }
    this.renderNextDate()
  }

  /**
   * Get first available slot from calendar
   * @param availabilities
   */
  getFirstAvailableSlot(availabilities) {
    availabilities = availabilities.data.sort((a, b) =>
      a.date.localeCompare(b.date)
    );
    $(availabilities).each((i, e) => {
      if (e.available) {
        this.first_available_date = e.date;
        return false;
      }
    });
    if (this.first_available_date !== null) {
      let self = this

      $.ajax(
        self.getDailyAvailabilitiesUrl(this.first_available_date, true),
        {
          dataType: "json",
          success: function (data, status, xhr) {
            // success callback function
            let slot = data.data[0];
            self.first_available_start_time = slot.start_time;
            self.first_available_end_time = slot.end_time;
            self.first_availability_updated_at = moment().format();
          },
          error: function (jqXhr, textStatus, errorMessage) {
            // error callback
            console.log("Impossibile selezionare prima disponibilità");
          },
        }
      );
    }
  }

  getDaySlots(d) {
    let self = this,
      step = this.component.range_step ? this.component.range_step : 1,
      html = "",
      parsedDate = moment(d ?? self.date, "DD-MM-YYYY");

    this.container.find("#slot-picker").html(html);

    $.ajax(self.getDailyAvailabilitiesUrl(parsedDate), {
      dataType: "json", // type of response data
      beforeSend: function () {
        self.container
          .find("#slot-picker")
          .append(`<div class="col-12">${self.loaderTpl}</div>`);
        self.container
          .find("#range-picker")
          .append(`<div class="col-12">${self.loaderTpl}</div>`);
      },
      success: function (data, status, xhr) {
        // success callback function
        $("#loader").remove();
        var countAllElmAvailables = data.data.filter(function (item) {
          return item.availability;
        }).length;

        let lastSelectedSlotAvailable = undefined;
        if (self.selectedNextSlots && self.selectedNextSlots.length > 0 && countAllElmAvailables > 0) {
          const targetSlot = self.selectedNextSlots.at(-1).slot.split("-");
          const start = targetSlot[0];
          const end = targetSlot[1];
          data.data.forEach(slot => {
            if (slot.start_time <= start && slot.end_time >= end) {
              lastSelectedSlotAvailable = slot;
              return false
            }
          })
        }

        if (countAllElmAvailables === 0) {
          self.container.find("#slot-picker")
            .html(`<div class="callout warning">
                            <div class="callout-title"><svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-help-circle"></use></svg>${Translator.trans(
              "warning",
              {},
              "messages",
              self.$language
            )}</div>
                            <p>${Translator.trans(
              "calendar_formio.no_availability_error",
              {},
              "messages",
              self.$language
            )}</p>
                            </div>`);
        } else {
          if (self.selectedNextSlots && self.selectedNextSlots.length > 0) {
            const targetSlot = self.selectedNextSlots.at(-1).slot.split("-");
            const start = targetSlot[0];
            const end = targetSlot[1];
            let element = {
              availability: lastSelectedSlotAvailable ? true : false,
              calendar_id: lastSelectedSlotAvailable?.calendar_id,
              date: data.data[0].date,
              end_time: end,
              min_duration: lastSelectedSlotAvailable?.min_duration,
              opening_hour_id: lastSelectedSlotAvailable?.opening_hour_id,
              slots_available: lastSelectedSlotAvailable?.slots_available,
              start_time: start
            }
            var cssClass = "available";
            var ariaDisabled = false;
            if (!element.availability) {
              cssClass = "disabled";
              ariaDisabled = true;
            }
            let key = `${element.start_time}-${element.end_time}`;
            if (key === self.available_slot) {
              cssClass = `${cssClass} active`;
            }
            let op_hour = element.opening_hour || element.opening_hour_id;
            let min_duration = element.min_duration;
  
            html = html.concat(
              `<div class="col-12 mx-0 mb-2 p-0"><div class="calendar-last-selection col-12 pt-2">
              <small class="pl-2 mb-2 d-flex"><b>
              <svg class="icon icon icon-sm mr-1"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-clock"></use></svg>
              ${Translator.trans(
                "calendar_formio.last_selection",
                {},
                "messages",
                self.$language
              )}</b></small><div class="col-6"><button type="button" data-calendar_id="${element.calendar_id}" data-available_slots="${key}" data-opening_hour="${op_hour}" data-min_duration="${min_duration}" class="btn btn-ora p-0 ${cssClass}" ${
                ariaDisabled ? 'tabindex="-1"' : ""
              } aria-disabled="${ariaDisabled}">${key}</button></div></div></div>
              <div class="col-12 mt-2 mb-2"><small class="pl-2 d-flex"><b>${Translator.trans("calendar_formio.other_timetables",{},"messages",self.$language)}</b></small></div>`
            );
          } 
          $(data.data).each(function (index, element) {
            var cssClass = "available";
            var ariaDisabled = false;
            if (!element.availability) {
              cssClass = "disabled";
              ariaDisabled = true;
            }

            let key = `${element.start_time}-${element.end_time}`;
            if (key === self.available_slot) {
              cssClass = `${cssClass} active`;
            }
            let op_hour = element.opening_hour || element.opening_hour_id;
            let min_duration = element.min_duration;

            html = html.concat(
              `<div class="col-6"><button type="button" data-calendar_id="${element.calendar_id}" data-available_slots="${key}" data-opening_hour="${op_hour}" data-min_duration="${min_duration}" class="btn btn-ora p-0 ${cssClass}" ${
                ariaDisabled ? 'tabindex="-1"' : ""
              } aria-disabled="${ariaDisabled}">${key}</button></div>`
            );
          });
          self.container
            .find("#slot-picker")
            .html(
              `<div class="col-12"><h6>${Translator.trans(
                "calendar_formio.availability_hours",
                {},
                "messages",
                self.$language
              )} ${self.date}</h6></div>${html}`
            );

          $(".btn-ora.available").on("click", function (e) {
            e.preventDefault();
            $(".btn-ora.active").removeClass("active");
            $(this).addClass("active");
            self.available_slot = $(this).data("available_slots");
            self.opening_hour = $(this).data("opening_hour");
            self.min_duration = $(this).data("min_duration");
            self.calendarIdSelected = $(this).data("calendar_id")

            let slots = self.available_slot.split("-");
            let start = slots[0].replace(":", "");
            let end = slots[1].replace(":", "");

            $("#range-picker").html(
              `<div class="my-2">
                                   <label for="range">${Translator.trans(
                "calendar_formio.range_picker_description",
                {},
                "messages",
                self.$language
              )}</label>
                                   <input type="text" class="mt-2" id="range" value=""/>
                                </div>
                                <div class="row mt-3">
                                   <div class="col-6">
                                       <label for="from-range">${Translator.trans(
                "calendar_formio.from",
                {},
                "messages",
                self.$language
              )}</label>
                                       <input id="from-range" type="time" value="${
                slots[0]
              }">
                                   </div>
                                   <div class="col-6">
                                       <label for="from-range">${Translator.trans(
                "calendar_formio.to",
                {},
                "messages",
                self.$language
              )}</label>
                                       <input id="to-range" type="time" value="${
                slots[1]
              }">
                                   </div>
                                </div>`
            );

            let range = $("#range");
            range.ionRangeSlider({
              skin: "modern",
              type: "double",
              grid: false,
              drag_interval: true,
              force_edges: true,
              step: step * 60000,
              min_interval: self.min_duration * 60 * 1000,
              min: moment(start, "HHmm").valueOf(),
              max: moment(end, "HHmm").valueOf(),
              from: moment(start, "HHmm").valueOf(),
              to: moment(end, "HHmm").valueOf(),
              prettify: function (num) {
                return moment(num).format("HH:mm");
              },
              onStart: function (data) {
                // fired on pointer release
                self.slot = `${data.from_pretty}-${data.to_pretty}`;
                $("#from-range").val(data.from_pretty);
                $("#to-range").val(data.to_pretty);
                if (!self.component.multiple) {
                  self.createOrUpdateMeeting();
                } else {
                  const save_btn = $(`#save-btn-modal-${self.id}`)
                  if (save_btn) {
                    save_btn.removeAttr('disabled');
                  }
                }
              },
              onUpdate: function (data) {
                self.slot = `${data.from_pretty}-${data.to_pretty}`;
                if (!self.component.multiple) {
                  self.createOrUpdateMeeting();
                } else {
                  const save_btn = $(`#save-btn-modal-${self.id}`)
                  if (save_btn) {
                    save_btn.removeAttr('disabled');
                  }
                }
              },
              onFinish: function (data) {
                // fired on pointer release
                self.slot = `${data.from_pretty}-${data.to_pretty}`;
                $("#from-range").val(data.from_pretty);
                $("#to-range").val(data.to_pretty);
                if (!self.component.multiple) {
                  self.createOrUpdateMeeting();
                } else {
                  const save_btn = $(`#save-btn-modal-${self.id}`)
                  if (save_btn) {
                    save_btn.removeAttr('disabled');
                  }
                }
              },
            });

            var instance = range.data("ionRangeSlider");
            $("#from-range").on("blur", function () {
              if ($(this).val() < slots[0] || $(this).val() > slots[1]) {
                Swal.fire(
                  `${Translator.trans(
                    "calendar_formio.from_range_error_value",
                    {},
                    "messages",
                    self.$language
                  )}`,
                  `${$(this).val()} ${Translator.trans(
                    "calendar_formio.from_range_error_not_valid",
                    {},
                    "messages",
                    self.$language
                  )}`,
                  'error'
                );
                $(this).val(slots[0]);
              } else {
                instance.update({
                  from: moment($(this).val(), "HHmm").valueOf(),
                });
              }
            });

            $("#to-range").on("blur", function () {
              if ($(this).val() < slots[0] || $(this).val() > slots[1]) {
                Swal.fire(
                  `${Translator.trans(
                    "calendar_formio.from_range_error_value",
                    {},
                    "messages",
                    self.$language
                  )}`,
                  `${$(this).val()} ${Translator.trans(
                    "calendar_formio.from_range_error_not_valid",
                    {},
                    "messages",
                    self.$language
                  )}`,
                  'error'
                );
                $(this).val(slots[1]);
              } else {
                instance.update({
                  to: moment($(this).val(), "HHmm").valueOf(),
                });
              }
            });

            self.updateValue();

            $("#date-picker-print").addClass("d-preview-calendar-none");
          });
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        // error callback
        Swal.fire(
          `${Translator.trans('error_message_detail', {}, 'messages', self.$language)}`,
          `${Translator.trans('calendar_formio.availability_error', {}, 'messages', self.$language)}`,
          'error'
        );
      },
      complete: function () {
        //Click available hour button only is visible for auto selection
        var btnHourActive = $(".btn-ora.available.active");
        if (btnHourActive.length > 0 && btnHourActive.is(":visible")) {
          btnHourActive.click();
        }
      },
    });
  }

  createOrUpdateMeeting(date, addNewMeeting) {
    let self = this,
      location = window.location,
      explodedPath = location.pathname.split("/");

    $.ajax(
      `${location.origin}/${explodedPath[1]}/${explodedPath[2]}/meetings/new-draft`,
      {
        method: "POST",
        data: {
          date: date ? date : self.date,
          slot: self.slot,
          calendar: self.calendarIdSelected || this.component.calendarId,
          opening_hour: self.opening_hour,
          meeting: date || addNewMeeting ? null : self.meeting,
          first_available_date: self.first_available_date,
          first_available_start_time: self.first_available_start_time,
          first_available_end_time: self.first_available_end_time,
          first_availability_updated_at: self.first_availability_updated_at,
        },
        dataType: "json", // type of response data
        success: function (data, status, xhr) {
          // success callback function
          if (data.hasOwnProperty('id')) {
            if (date || addNewMeeting) {
              const parsedDate = moment(date ? date : self.date, "DD-MM-YYYY").format("YYYY-MM-DD");
              if (self.selectedNextSlots.length === 0) {
                self.meeting = data.id;
                self.meeting_expiration_time = moment(data.expiration_time, "YYYY-MM-DD HH:mm");
              }
              self.selectedNextSlots.push({
                "date": parsedDate,
                "slot": self.slot,
                "calendar_id": self.calendarIdSelected,
                "meeting_id": data.id, 
                "opening_hour_id": "",
              });
              self.renderNextDate();
            } else {
              self.meeting = data.id;
              self.meeting_expiration_time = moment(data.expiration_time, "YYYY-MM-DD HH:mm");
            }
            self.updateValue();
          }
        },
        error: function (jqXhr, textStatus, errorMessage) {
          // error callback
          Swal.fire(
            `${Translator.trans('error_message_detail', {}, 'messages', self.$language)}`,
            `${Translator.trans('calendar_formio.draft_creation_error', {}, 'messages', self.$language)}`,
            'error'
          );
          // Reinitialize
          self.slot = self.available_slot;
          self.meeting = null;
          self.meeting_expiration_time = null;
          $("#range-picker").html("");
          self.updateValue();
          self.getDaySlots();
        },
        complete: function () {
          if (self.date && self.slot && self.meeting && self.opening_hour) {
            self.renderData();
          } else {
            $("#draft-expiration-container").addClass("alert alert-warning");
            $("#draft-expiration-container").html(`<i>${Translator.trans("calendar_formio.draft_creation_error", {}, "messages", self.$language)}</i>`)
          }
        },
      }
    );
  }

  getMonthlyAvailabilitiesUrl(date, available=true) {
    let self = this,
      calendarID,
      selectOpeningHours = this.component.select_opening_hours,
      openingHours = this.component.select_opening_hours
        ? this.component.opening_hours
        : [],
      location = window.location,
      explodedPath = location.pathname.split("/");

    if(this.component.calendarId){
      calendarID = typeof this.component.calendarId === 'object' ? this.component.calendarId : [this.component.calendarId]
    }
    const calendarIds = calendarID.map((param,index) => {

      if(index === 0){
        return(
          encodeURIComponent('calendar_ids') + '=' +
          encodeURIComponent(param)
        );
      }else{
        return  encodeURIComponent(param)
      }

    });

    let url = `${location.origin}/${explodedPath[1]}/api/availabilities?${calendarIds}`;

    let queryParameters = [
      `from_date=${moment(date).startOf('month').format("YYYY-MM-DD")}`,
      `to_date=${moment(date).endOf("month").format("YYYY-MM-DD")}`,
    ];
    if (available) {
      queryParameters.push(`available=${available}`);
    }
    // filter availabilities by selected opening-hours (this is mandatory in case of overlapped opening hours)
    if (selectOpeningHours && openingHours) {
      // Select specific opening hours
      queryParameters.push(`opening_hours=${openingHours.join()}`);
    }

    return `${url}&${queryParameters.join("&")}`;
  }

  getDailyAvailabilitiesUrl(date, available=false) {
    let self = this,
      calendarID = this.component.calendarId,
      selectOpeningHours = this.component.select_opening_hours,
      openingHours = this.component.select_opening_hours
        ? this.component.opening_hours
        : [],
      location = window.location,
      explodedPath = location.pathname.split("/");

    date = moment(date).format("YYYY-MM-DD");

    const calendarIds = calendarID.map((param,index) => {
      if(index === 0){
        return(
          encodeURIComponent('calendar_ids') + '=' +
          encodeURIComponent(param)
        );
      }else{
        return encodeURIComponent(param)
      }
    });


    let url = `${location.origin}/${explodedPath[1]}/api/availabilities/${date}?${calendarIds}`;

    let queryParameters = [];
    if (available) {
      queryParameters.push(`available=${available}`);
    }
    if (self.meeting) {
      // Exclude saved meeting from unavailabilities
      queryParameters.push(`exclude=${self.meeting}`);
    }
    if (selectOpeningHours && openingHours) {
      // Select specific opening hours
      queryParameters.push(`opening_hours=${openingHours.join()}`);
    }

    if (queryParameters) {
      url = `${url}&${queryParameters.join("&")}`;
    }

    return url;
  }

  removeNextData(index) {
    let self = this,
      location = window.location,
      explodedPath = location.pathname.split("/");
    let url = `${location.origin}/${explodedPath[1]}/api/meetings/${this.selectedNextSlots[index].meeting_id}`
    this.auth.execAuthenticatedCall((token) => {
      $.ajax({
        url: url,
        type: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token}`
        },
        success: function(response) {
          self.selectedNextSlots.splice(index, 1);
          self.renderData();
          if (self.selectedNextSlots.length === 0 && self.countdown) {
            
            $("#date-picker-print").removeClass("d-preview-calendar-none");
            $("#draft-expiration-dynamic").html("");
            $("#draft-expiration-dynamic-container").removeClass("alert");
            $("#draft-expiration-dynamic-container").removeClass("alert-info");
            self.countdown.stop();
            self.countdown = null;
            self.meeting = null;
            self.meeting_expiration_time = null;
          }
        },
        error: function(xhr, status, error) {
          self.selectedNextSlots.splice(index, 1);
          self.renderData();
          console.error(error);
        }
      });
    });
  }

  renderNextDate() {
    
    const self = this;
    const  selectedNextDate = document.getElementById('selectedNextDate');
    if (!selectedNextDate) {
      return
    }
    let list = ""
    this.selectedNextSlots.map((day, index) => { list = list +
      `<li>
        <div class="list-item">
          <div class="it-right-zone">
            <span class="text pl-4">${moment(moment(day.date, "YYYY-MM-DD")).format('DD-MM-YYYY')} ${Translator.trans("calendar_formio.at_hours", {}, "messages", self.$language)} ${day.slot}</span>
            ${ self.disabled ? '' : `<svg class="icon delete-icon" role="button" data-index="${index}"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-delete"></use></svg>`}
          </div>
        </div>
      </li>`;
      })
    // Display the selected value in the <p> element
    selectedNextDate.innerHTML = `
    <div class="it-list-wrapper">
      <ul class="it-list">
        ${list}
      </ul>
    </div>
    `;
    document.querySelectorAll('.delete-icon').forEach(icon => {
      icon.addEventListener('click', function(e) {
        const index = e.currentTarget.getAttribute('data-index');
        self.removeNextData(index);
      });
    });
    if (this.disabled) {
      const datepicker = $(`#calendar-container-${this.id}`)
      if (datepicker) {
        datepicker.removeClass("d-preview-calendar-none");
      }
    }
  }

  openModal() {
    const modal = document.getElementById(`modal-${this.id}`);
    if (modal) {
      modal.style.display = "block";
      $(`#save-btn-modal-${this.id}`).attr('disabled', 'disabled');
    }
  }

  closeModal() {
    const modal = document.getElementById(`modal-${this.id}`);
    if (modal) {
      modal.style.display = "none";
    }
  };
}
