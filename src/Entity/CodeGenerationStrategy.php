<?php

namespace App\Entity;

use App\Repository\CodeGenerationStrategyRepository;
use App\Utils\DateTimeUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;


/**
 * @ORM\Entity(repositoryClass=CodeGenerationStrategyRepository::class)
 */
class CodeGenerationStrategy
{

  use TimestampableEntity;

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   * @OA\Property(description="Uuid of the strategy", type="string")
   * @Groups({"read", "kafka"})
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Name of the strategy", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private ?string $name = '';

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   * @OA\Property(description="Prefix of the strategy", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private ?string $prefix = '';

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   * @OA\Property(description="Postfix of the strategy", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private ?string $postfix = '';

  /**
   * @ORM\Column(type="integer")
   * @OA\Property(description="Number of digit character of the strategy's counter. Es: a strategy with 3 in this field wil generate max 999 code resetting itself", type="integer")
   * @Groups({"read", "write", "kafka"})
   */
  private int $counterDigitLength = 3;

  /**
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Temporal reset of the strategy, defines every when the counters associated with the strategy will be reseted. Accepted values are: <code>none</code>, <code>daily</code>, <code>weekly</code>, <code>monthly</code>, <code>yearly</code>", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private string $temporalReset = DateTimeUtils::PERIOD_NONE;

  /**
   * @ORM\OneToMany(targetEntity=CodeCounter::class, mappedBy="codeGenerationStrategy", orphanRemoval=true)
   * @Serializer\Exclude()
   */
  private Collection $codeCounters;

  /**
   * @var \DateTime
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(type="datetime")
   * @Groups({"read", "kafka"})
   */
  protected $createdAt;

  /**
   * @var \DateTime
   * @Gedmo\Timestampable(on="update")
   * @ORM\Column(type="datetime")
   * @Groups({"read", "kafka"})
   */
  protected $updatedAt;

  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
    $this->codeCounters = new ArrayCollection();
  }

  public function getId()
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getPrefix(): ?string
  {
    return $this->prefix;
  }

  public function setPrefix(?string $prefix): self
  {
    $this->prefix = $prefix;

    return $this;
  }

  public function getPostfix(): ?string
  {
    return $this->postfix;
  }

  public function setPostfix(?string $postfix): self
  {
    $this->postfix = $postfix;

    return $this;
  }

  public function getCounterDigitLength(): int
  {
    return $this->counterDigitLength;
  }

  public function setCounterDigitLength(int $counterDigitLength): self
  {
    $this->counterDigitLength = $counterDigitLength;

    return $this;
  }

  public function getMaxCounterValue(): int
  {
    // Check if the input is a positive integer
    if ($this->counterDigitLength <= 0) {
      return 0;
    }

    return pow(10, $this->counterDigitLength) - 1;
  }

  public function getTemporalReset(): string
  {
    return $this->temporalReset;
  }

  public function setTemporalReset(string $temporalReset): self
  {
    $this->temporalReset = $temporalReset;

    return $this;
  }

  /**
   * @return Collection<int, CodeCounter>
   */
  public function getCodeCounters(): Collection
  {
    return $this->codeCounters;
  }

  public function addCodeCounter(CodeCounter $codeCounter): self
  {
    if (!$this->codeCounters->contains($codeCounter)) {
      $this->codeCounters[] = $codeCounter;
      $codeCounter->setCodeGenerationStrategy($this);
    }

    return $this;
  }

  public function removeCodeCounter(CodeCounter $codeCounter): self
  {
    if ($this->codeCounters->removeElement($codeCounter)) {
      // set the owning side to null (unless already changed)
      if ($codeCounter->getCodeGenerationStrategy() === $this) {
        $codeCounter->setCodeGenerationStrategy(null);
      }
    }

    return $this;
  }
}
