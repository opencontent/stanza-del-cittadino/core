<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application\ImportCommand;

use App\Entity\Ente;
use App\Entity\User;

class ImportCommand
{
  public Ente $ente;
  public User $requester;
  public ImportData $importData;
}
