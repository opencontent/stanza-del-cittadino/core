<?php

namespace App\DataTable;


use App\Entity\OperatoreUser;
use App\Entity\Servizio;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;

class ServiceTableType implements DataTableTypeInterface
{

  public function configure(DataTable $dataTable, array $options): void
  {
    $dataTable
      ->add('name', TwigColumn::class, [
        'label' => 'Gruppo -> Nome',
        'orderable' => true,
        'searchable' => true,
        'leftExpr' => 'LOWER(servizio.name)',
        'operator' => 'LIKE',
        'rightExpr' => function ($value) {
          return '%' . strtolower($value) . '%';
        },
        'template' => 'Admin/table/services/_name.html.twig',
      ])
      ->add('topics', TwigColumn::class, [
        'label' => 'Categoria',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/services/_topics.html.twig',
      ])
      ->add('status', TwigColumn::class, [
        'label' => 'general.stato',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/services/_status.html.twig',
      ])
      ->add('due', TwigColumn::class, [
        'label' => '',
        'className' => 'text-right',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/services/_due.html.twig',
      ])
      ->add('actions', TwigColumn::class, [
        'label' => '',
        'className' => 'text-right',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/services/_actions.html.twig',
      ])
      ->createAdapter(ORMAdapter::class, [
        'entity' => Servizio::class,
        'query' => function (QueryBuilder $builder) use ($options): void {
          $qb = $builder
            ->select('servizio')
            ->from(Servizio::class, 'servizio');

          if (isset($options['user']) && $options['user'] instanceof OperatoreUser) {
            $user = $options['user'];
            $qb
              ->andWhere('servizio IN (:allowedServices)')
              ->setParameter('allowedServices', $user->getAllEnabledServicesIds())
              ->andWhere('servizio.status != :statusDraft')
              ->setParameter('statusDraft', Servizio::STATUS_DRAFT);
          }
        },
      ])
      ->addOrderBy('name', DataTable::SORT_ASCENDING);
  }
}
