<?php


namespace App\Entity;

/**
 * Class StatusChange
 */
class StatusChange
{
  private $timestamp;
  private $evento;
  private $operatore;
  private $userGroup;
  private $responsabile;
  private $struttura;
  private $message;
  private $messageId;

  /**
   * StatusChange constructor.
   * @param array|null $data
   */
  public function __construct(?array $data = [])
  {

    $this->evento = $data['evento'] ?? null;
    $this->operatore = $data['operatore'] ?? null;
    $this->userGroup = $data['user_group'] ?? null;
    $this->responsabile = $data['responsabile'] ?? null;
    $this->struttura = $data['struttura'] ?? null;
    $this->timestamp = $data['timestamp'] ?? $data['time'] ?? null;

    $this->message = $data['message'] ?? null;
    $this->messageId = $data['message_id'] ?? null;

    if (!is_int($this->timestamp)) {
      try {
        $date = new \DateTime($this->timestamp, new \DateTimeZone('Europe/Rome'));
      } catch (\Exception $e) {
        $date = new \DateTime();
      }
      $this->timestamp = $date->getTimestamp();
    }
  }

  /**
   * @return int
   */
  public function getTimestamp(): int
  {
    return $this->timestamp;
  }

  /**
   * @param int $timestamp
   */
  public function setTimestamp(int $timestamp)
  {
    $this->timestamp = $timestamp;
  }

  /**
   * @return mixed
   */
  public function getEvento()
  {
    return $this->evento;
  }

  /**
   * @param mixed $evento
   */
  public function setEvento($evento)
  {
    $this->evento = $evento;
  }

  /**
   * @return mixed
   */
  public function getOperatore()
  {
    return $this->operatore;
  }

  /**
   * @param mixed $operatore
   */
  public function setOperatore($operatore)
  {
    $this->operatore = $operatore;
  }

  /**
   * @return mixed
   */
  public function getUserGroup()
  {
    return $this->userGroup;
  }

  /**
   * @param mixed $userGroup
   */
  public function setUserGroup($userGroup)
  {
    $this->userGroup = $userGroup;
  }

  /**
   * @return mixed
   */
  public function getResponsabile()
  {
    return $this->responsabile;
  }

  /**
   * @param mixed $responsabile
   */
  public function setResponsabile($responsabile)
  {
    $this->responsabile = $responsabile;
  }

  /**
   * @return mixed
   */
  public function getStruttura()
  {
    return $this->struttura;
  }

  /**
   * @param mixed $struttura
   */
  public function setStruttura($struttura)
  {
    $this->struttura = $struttura;
  }

  /**
   * @return mixed|null
   */
  public function getMessage()
  {
    return $this->message;
  }

  /**
   * @param string|null $message
   */
  public function setMessage(?string $message)
  {
    $this->message = $message;
  }

  /**
   * @return mixed
   */
  public function getMessageId()
  {
    return $this->messageId;
  }

  /**
   * @param mixed $messageId
   */
  public function setMessageId($messageId)
  {
    $this->messageId = $messageId;
  }

  /**
   * @return mixed|string|null
   */
  public function getAssignee() {
    $assignee = $this->operatore ?? $this->userGroup;

    if ($this->operatore && $this->userGroup) {
        $assignee = $assignee . ' (' . $this->userGroup . ')';
    }

    return $assignee;
  }

  /**
   * @return mixed|null
   */
  public function getAssigner() {
    if ($this->responsabile && $this->responsabile !== $this->operatore) {
      return $this->responsabile;
    }

    return null;
  }

  /**
   * @return string
   */
  public function toJson(): string
  {
    return json_encode(
      $this->toArray()
    );
  }

  /**
   * @return array
   */
  public function toArray(): array
  {
    return [
      'evento' => $this->evento,
      'operatore' => $this->operatore,
      'user_group' => $this->userGroup,
      'responsabile' => $this->responsabile,
      'struttura' => $this->struttura,
      'timestamp' => $this->timestamp,
      'message' => $this->message,
      'message_id' => $this->messageId
    ];
  }
}
