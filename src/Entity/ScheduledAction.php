<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Table(name="scheduled_action")
 */
class ScheduledAction
{
  public const STATUS_PENDING = 1;
  public const STATUS_DONE = 3;
  public const STATUS_INVALID = 4;

  use TimestampableEntity;

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   */
  protected $id;

  /**
   * @ORM\Column(type="string")
   */
  private string $service;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private string $type;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private ?string $params;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private ?string $hostname;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private ?string $log;

  /**
   * @ORM\Column(type="integer", options={"default": self::STATUS_PENDING})
   */
  private int $status = self::STATUS_PENDING;

  /**
   * @ORM\Column(type="integer", options={"default": 0})
   */
  private int $retry = 0;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private ?\DateTime $executeAt;

  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
  }

  public function getId()
  {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): self
  {
    $this->type = $type;
    return $this;
  }

  public function getParams(): ?string
  {
    return $this->params;
  }

  public function setParams(?string $params): self
  {
    $this->params = $params;
    return $this;
  }

  public function getService(): string
  {
    return $this->service;
  }

  public function setService($service): self
  {
    $this->service = $service;
    return $this;
  }

  public function getHostname(): ?string
  {
    return $this->hostname;
  }

  public function setHostname(?string $hostname): self
  {
    $this->hostname = $hostname;
    return $this;
  }

  public function getLog(): ?string
  {
    return $this->log;
  }

  public function setLog(?string $log): self
  {
    $this->log = $log;
    return $this;
  }

  public function getStatus(): int
  {
    return $this->status;
  }

  public function setStatus(int $status): self
  {
    $this->status = $status;
    return $this;
  }

  public function getRetry(): int
  {
    return $this->retry;
  }

  public function setRetry(int $retry): self
  {
    $this->retry = $retry;
    return $this;
  }

  public function incrementRetry(): self
  {
    $this->retry++;
    return $this;
  }

  public function setDone(): self
  {
    $this->status = self::STATUS_DONE;
    return $this;
  }

  public function setInvalid(): self
  {
    $this->status = self::STATUS_INVALID;
    return $this;
  }

  public function getExecuteAt(): ?\DateTime
  {
    return $this->executeAt;
  }

  public function setExecuteAt(\DateTime $executeAt): ScheduledAction
  {
    $this->executeAt = $executeAt;
    return $this;
  }

  /**
   * Check if the scheduled action is ready to be executed
   */
  public function isReadyToExecute(): bool
  {
    return $this->status === self::STATUS_PENDING && $this->executeAt <= new \DateTime('now');
  }
}
