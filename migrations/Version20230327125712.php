<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230327125712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE VIEW geo_application AS
                           SELECT id, CAST(dematerialized_forms -> 'data' -> 'address' ->> 'lat' AS numeric) AS lat,
                           CAST(dematerialized_forms -> 'data' -> 'address' ->> 'lon' AS numeric) AS lon,
                           ST_SetSRID(ST_MakePoint(CAST(dematerialized_forms -> 'data' -> 'address' ->> 'lon' AS numeric), CAST(dematerialized_forms -> 'data' -> 'address' ->> 'lat' AS numeric)), 4326) AS geo_point,
                           ST_Transform(ST_SetSRID(ST_MakePoint(CAST(dematerialized_forms -> 'data' -> 'address' ->> 'lon' AS numeric), CAST(dematerialized_forms -> 'data' -> 'address' ->> 'lat' AS numeric)), 4326), 3857) AS projected_point
                           FROM pratica
                           WHERE dematerialized_forms -> 'data' -> 'address' IS NOT NULL
                           AND dematerialized_forms -> 'data' -> 'address' ->> 'lat'  IS NOT NULL
                           AND dematerialized_forms -> 'data' -> 'address' ->> 'lon'  IS NOT NULL
                           AND dematerialized_forms -> 'data' -> 'address' ->> 'lat'  <> ''
                           AND dematerialized_forms -> 'data' -> 'address' ->> 'lon'  <> ''");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP VIEW geo_application');
    }
}
