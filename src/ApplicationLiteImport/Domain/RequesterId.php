<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Domain;

use App\Entity\User;

class RequesterId
{
  private string $requesterId;

  public static function fromUser(User $requester): RequesterId
  {
    $requesterId = $requester->getId();
    if (!is_string($requesterId)) {
      $requesterId = (string) $requesterId;
    }

    return new self($requesterId);
  }

  public function __construct(string $requesterId)
  {
    $this->requesterId = $requesterId;
  }

  public function value(): string
  {
    return $this->requesterId;
  }
}
