<?php

declare(strict_types=1);

namespace Tests\Functional\Services;

use App\Entity\Ente;
use App\Services\MeetingService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\Builders\CalendarBuilder;

/**
 * @group functional
 * @group meetings
 */
class MeetingServiceTest extends KernelTestCase
{
  private ?EntityManagerInterface $entityManager;
  private ?MeetingService $meetingService;

  protected function setUp(): void
  {
    $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
    $this->meetingService = static::getContainer()->get(MeetingService::class);

    $this->entityManager->beginTransaction();
  }

  protected function tearDown(): void
  {
    if ($this->entityManager->getConnection()->isTransactionActive()) {
      // Rollback della transazione alla fine del test
      $this->entityManager->rollback();
    }

    $this->entityManager->close();
    $this->entityManager = null;

    parent::tearDown();
  }

  /** @throws Exception */
  public function testGetAvailabilitiesByDateShouldWorkWithoutClosingPeriods(): void
  {
    $nextMonday = new DateTime('next monday');

    $dayToEvaluate = $nextMonday->format('Y-m-d');

    $openFrom = $nextMonday->format('Y-m-d 12:00:00');
    $nextMonday->modify('next monday');
    $openTo = $nextMonday->format('Y-m-d 16:00:00');

    $meetingDuration = 30;

    $expectedSlot = [
      '12:00-12:30',
      '12:30-13:00',
      '13:00-13:30',
      '13:30-14:00',
      '14:00-14:30',
      '14:30-15:00',
      '15:00-15:30',
      '15:30-16:00',
    ];

    $calendar = CalendarBuilder::creaTimeFixed()
      ->addOpeningHour(
        new DateTime($openFrom),
        new DateTime($openTo),
        [1, 2, 3, 4, 5],
        $meetingDuration,
        0,
      )
      ->build();

    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');

    $this->entityManager->persist($ente);
    $this->entityManager->persist($calendar->getOwner());
    $this->entityManager->persist($calendar);
    $this->entityManager->flush();

    $availabilities = $this->meetingService->getAvailabilitiesByDate(
      $calendar,
      new DateTime($dayToEvaluate),
      false,
      true,
    );

    $this->assertSame($expectedSlot, array_keys($availabilities));
  }

  /** @throws Exception */
  public function testGetAvailabilitiesByDateShouldCorrectlyRemoveSlotInClosedPeriod(): void
  {
    $nextMonday = new DateTime('next monday');

    $dayToEvaluate = $nextMonday->format('Y-m-d');

    $openFrom = $nextMonday->format('Y-m-d 09:00:00');
    $nextMonday->modify('next monday');
    $openTo = $nextMonday->format('Y-m-d 16:00:00');

    $closedFrom = "$dayToEvaluate 09:00:00";
    $closedTo = "$dayToEvaluate 14:00:00";

    $meetingDuration = 30;

    $expectedSlot = [
      '14:00-14:30',
      '14:30-15:00',
      '15:00-15:30',
      '15:30-16:00',
    ];

    $calendar = CalendarBuilder::creaTimeFixed()
      ->addOpeningHour(
        new DateTime($openFrom),
        new DateTime($openTo),
        [1, 2, 3, 4, 5],
        $meetingDuration,
        0,
      )
      ->addClosingPeriods(
        new DateTime($closedFrom),
        new DateTime($closedTo),
      )
      ->build();

    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');

    $this->entityManager->persist($ente);
    $this->entityManager->persist($calendar->getOwner());
    $this->entityManager->persist($calendar);
    $this->entityManager->flush();

    $availabilities = $this->meetingService->getAvailabilitiesByDate(
      $calendar,
      new DateTime($dayToEvaluate),
      false,
      true,
    );

    $this->assertSame($expectedSlot, array_keys($availabilities));
  }
}
