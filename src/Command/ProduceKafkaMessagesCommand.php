<?php

namespace App\Command;

use App\Entity\Message;
use App\Entity\Pratica;
use App\Services\KafkaService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\RouterInterface;
use Throwable;

class ProduceKafkaMessagesCommand extends Command
{
  private ?EntityManagerInterface $entityManager;

  private ?RouterInterface $router;

  private ?string $scheme;

  private ?string $host;

  private ?KafkaService $kafkaService;

  private bool $dryRun = false;

  private ?SymfonyStyle $symfonyStyle;

  /**
   * @param EntityManagerInterface $entityManager
   * @param RouterInterface $router
   * @param KafkaService $kafkaService
   * @param string $scheme
   * @param string $host
   */
  public function __construct(
    EntityManagerInterface $entityManager,
    RouterInterface $router,
    KafkaService $kafkaService,
    string $scheme,
    string $host
  ) {
    $this->entityManager = $entityManager;
    parent::__construct();
    $this->router = $router;
    $this->scheme = $scheme;
    $this->host = $host;
    $this->kafkaService = $kafkaService;
  }

  protected function configure()
  {
    $this
      ->setName('ocsdc:kafka:produce-messages')
      ->addOption('id', null, InputOption::VALUE_OPTIONAL, 'Id of the entity')
      ->addOption(
        'entity',
        null,
        InputOption::VALUE_OPTIONAL,
        'Entity name: application or message (default is application)'
      )
      ->addOption(
        'date',
        null,
        InputOption::VALUE_OPTIONAL,
        'If specified, only applications with an update date (or message with creation date) before to the date entered will be considered. Format yyyy-mm-dd'
      )
      ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run')
      ->setDescription('Produce kafka messages from applications');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $this->symfonyStyle = new SymfonyStyle($input, $output);
    $this->dryRun = $input->getOption('dry-run');

    $context = $this->router->getContext();
    $context->setHost($this->host);
    $context->setScheme($this->scheme);

    try {
      $id = $input->getOption('id');
      $date = $this->getSelectedDate($input->getOption('date'));
      $entity = $this->getSelectedEntity($input->getOption('entity'));
    } catch (Throwable $e) {
      $this->symfonyStyle->error($e->getMessage());
      return 1;
    }

    if (empty($id) && !$date instanceof DateTime) {
      $this->symfonyStyle->error('Missing id or date option');
      return 1;
    }

    $messages = 0;

    try {
      if ($entity === 'message') {
        $messages = $this->produceMessages($id, $date);
      } elseif ($entity === 'application') {
        $messages = $this->produceApplications($id, $date);
      }
    }catch (Throwable $e){
      $this->symfonyStyle->error($e->getMessage());
    }

    $this->symfonyStyle->success('Success! - Messages created: ' . $messages);
    return 0;
  }

  /**
   * @param string|null $selected
   * @return string|null
   * @throw InvalidArgumentException
   */
  private function getSelectedEntity(?string $selected): string
  {
    $allowedEntities = ['application', 'message'];
    if ($selected && !in_array($selected, $allowedEntities)) {
      throw new InvalidArgumentException('Entity not handled: ' . $selected);
    }
    if (!$selected) {
      $selected = 'application';
    }
    $this->symfonyStyle->success('Entity selected: ' . $selected);

    return $selected;
  }

  /**
   * @param string $date
   * @return DateTime|null
   * @throw InvalidArgumentException
   */
  private function getSelectedDate(?string $date): ?DateTime
  {
    $dateTime = null;
    if (!empty($date)) {
      $dateFormat = 'Y-m-d';
      $dateTime = DateTime::createFromFormat($dateFormat, $date);
      if ($dateTime || $dateTime->format($dateFormat) !== $date) {
        throw new InvalidArgumentException('Option date must be this format: yyyy-mm-dd');
      }

      $this->symfonyStyle->success('Date selected: ' . $dateTime->format('c'));
    }

    return $dateTime;
  }

  /**
   * @param string|null $id
   * @param DateTime|null $date
   * @return int
   * @throws GuzzleException
   * @throws ReflectionException
   */
  private function produceMessages(?string $id, ?DateTime $date): int
  {
    $messages = 0;
    $qb = $this->entityManager->createQueryBuilder()
      ->select('message')
      ->from('App:Message', 'message');

    if (!empty($id)) {
      $qb->andWhere('message.id = :id')
        ->setParameter('id', $id);
    }

    if ($date instanceof DateTime) {
      $qb->andWhere('message.created_at <= :date')
        ->setParameter('date', $date->format('U'));
    }

    $applicationMessages = $qb->getQuery()->getResult();
    $this->symfonyStyle->note('Will be created ' . count($applicationMessages) . ' kafka messages from entity message');

    if (!empty($applicationMessages)) {
      /** @var Message $message */
      foreach ($applicationMessages as $message) {
        if (!$this->dryRun) {
          $this->kafkaService->produceMessage($message);
          $messages++;
        }
      }
    }

    return $messages;
  }

  /**
   * @param string|null $id
   * @param DateTime|null $date
   * @return int
   * @throws GuzzleException
   * @throws ReflectionException
   */
  private function produceApplications(?string $id, ?DateTime $date): int
  {
    $messages = 0;

    $notAllowedStatuses = [Pratica::STATUS_DRAFT];
    $qb = $this->entityManager->createQueryBuilder()
      ->select('pratica')
      ->from('App:Pratica', 'pratica')
      ->where('pratica.status NOT IN (:status)')
      ->setParameter('status', $notAllowedStatuses);

    if (!empty($id)) {
      $qb->andWhere('pratica.id = :id')
        ->setParameter('id', $id);
    }

    if ($date instanceof DateTime) {
      $qb->andWhere('pratica.updated_at <= :date')
        ->setParameter('date', $date);
    }

    $applications = $qb->getQuery()->getResult();
    $this->symfonyStyle->note('Will be created ' . count($applications) . ' kafka messages from entity application');

    if (!empty($applications)) {
      /** @var Pratica $application */
      foreach ($applications as $application) {
        if (!$this->dryRun) {
          $this->kafkaService->produceMessage($application);
          $messages++;
        }
      }
    }

    return $messages;
  }
}
