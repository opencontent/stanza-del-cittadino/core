<?php

namespace App\Entity;

use App\Applications\Application\DTOs\ProcessHistory;
use App\Applications\Domain\ExternalAction;
use App\Applications\Exceptions\ApplicationLiteException;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class ApplicationLite extends Pratica
{
  public const TYPE_APPLICATION_LITE = 'application_lite';


  /** @ORM\Column(type="json", options={"jsonb":true}) */
  private ?array $externalActions = [];

  /** @throws ApplicationLiteException */
  private function __construct(
    Servizio $servizio,
    CPSUser $applicant,
    string $subject,
    int $status,
    Ente $ente
  ) {

    parent::__construct();

    $this->type = self::TYPE_APPLICATION_LITE;
    $this->dematerializedForms = [];

    $this->ensureThatServiceIsCompatible($servizio);
    $this->ensureThatStatusIsValid($status);

    $this->setOggetto($subject);
    $this->setServizio($servizio);
    $this->setStatus($status);
    $this->setUser($applicant);
    $this->setEnte($ente);
  }

  /** @throws ApplicationLiteException */
  public static function create(
    Servizio $servizio,
    CPSUser $applicant,
    string $subject,
    int $status,
    Ente $ente
  ): self {

    return new self(
      $servizio,
      $applicant,
      $subject,
      $status,
      $ente,
    );
  }

  /** @throws ApplicationLiteException */
  public static function createAsExternal(
    string $externalId,
    Servizio $servizio,
    CPSUser $applicant,
    string $subject,
    int $status,
    Ente $ente
  ): self {

    $self = new self(
      $servizio,
      $applicant,
      $subject,
      $status,
      $ente,
    );

    $self->setExternalId($externalId);

    return $self;
  }

  /** @throws ApplicationLiteException */
  public function update(int $status, DateTime $statusChangeAt)
  {
    $this->ensureThatStatusIsValid($status);

    $this->setStatus($status);

    $this->setLatestStatusChangeTimestamp($statusChangeAt->getTimestamp());
  }

  /** @throws ApplicationLiteException */
  private function ensureThatStatusIsValid(int $status): void
  {
    $isValid = in_array($status, [
      2000,
      4000,
      4200,
      7000,
      9000,
      20000,
      50000,
    ]);

    if (!$isValid) {
      throw ApplicationLiteException::becauseStatusIsInvalid($status);
    }
  }

  /** @throws ApplicationLiteException */
  private function ensureThatServiceIsCompatible(Servizio $servizio): void
  {
    if ($servizio->getPraticaFCQN() !== ApplicationLite::class) {
      throw ApplicationLiteException::becauseServiceIsNotCompatible($servizio);
    }
  }

  public function applicant(): CPSUser
  {
    return $this->getUser();
  }

  /**
   * @return ExternalAction[]
   * @throws ApplicationLiteException
   */
  public function externalActions(): array
  {
    $return = [];
    foreach ($this->externalActions as $externalAction) {
      if ($externalAction instanceof ExternalAction) {
        $return[] = $externalAction;
      } else {
        $return[] = ExternalAction::fromArray($externalAction);
      }
    }

    return $return;
  }

  /**
   * @param ExternalAction[] $externalActions
   * @throws ApplicationLiteException
   */
  public function replaceExternalActions(array $externalActions)
  {
    foreach ($externalActions as $externalAction) {
      if (!$externalAction instanceof ExternalAction) {
        throw ApplicationLiteException::becauseArgumentInvalid(ExternalAction::class);
      }
    }

    $this->externalActions = $externalActions;
  }

  /**  @param ProcessHistory[] $processHistory */
  public function replaceProcessHistory(array $processHistory)
  {
    $storicoStati = $this->getStoricoStati();
    $storicoStati->clear();

    usort($processHistory, function (ProcessHistory $a, ProcessHistory $b) {
      if ($a->statusChangedAt == $b->statusChangedAt) {
        return 0;
      }

      return ($a->statusChangedAt < $b->statusChangedAt) ? -1 : 1;
    });

    foreach ($processHistory as $historyElement) {

      $status = $historyElement->status;
      $timestamp = $historyElement->statusChangedAt->getTimestamp();
      $newStatus = [$status, null];

      if ($storicoStati->containsKey($timestamp)) {
        $updated = $storicoStati->get($timestamp);
        $updated[] = $newStatus;
      } else {
        $updated = [$newStatus];
      }
      $storicoStati->set($timestamp, $updated);
    }
  }

  public function externalId(): ?string
  {
    return $this->getExternalId();
  }

  public function isExternal(): bool
  {
    return $this->externalId() !== null;
  }

  public function service(): Servizio
  {
    return $this->getServizio();
  }

  public function status(): int
  {
    return $this->getStatus();
  }

  public function subject(): string
  {
    return $this->getOggetto();
  }
}
