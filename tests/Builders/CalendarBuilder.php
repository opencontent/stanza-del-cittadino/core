<?php

declare(strict_types=1);

namespace Tests\Builders;

use App\Entity\Calendar;
use App\Entity\CPSUser;
use App\Entity\OpeningHour;
use DateTime;
use DateTimeInterface;

class CalendarBuilder
{
  private string $calendarType;
  private CPSUser $calendarOwner;
  private array $closingPeriods;
  private array $openingHours;

  public static function creaTimeFixed(): self
  {
    $self = new self(Calendar::TYPE_TIME_FIXED);

    return $self;
  }

  private function __construct(string $calendarType)
  {
    $this->calendarType = $calendarType;

    $this->calendarOwner = $this->createCalendarOwner();
    $this->closingPeriods = [];
    $this->openingHours = [];
  }

  private function createCalendarOwner(): CPSUser
  {
    $owner = new CPSUser();
    $owner->setUsername('calendar_owner');

    return $owner;
  }

  public function addClosingPeriods(
    DateTime $from,
    DateTime $to
  ): self {

    $this->closingPeriods[] = [
      'from_time' => $from->format(DateTimeInterface::RFC3339),
      'to_time' => $to->format(DateTimeInterface::RFC3339),
    ];

    return $this;
  }

  public function addOpeningHour(
    DateTime $from,
    DateTime $to,
    array $daysOfWeek,
    int $meetingMinutes,
    int $intervalMinutes
  ): self {

    $openingHour = new OpeningHour();
    $openingHour->setDaysOfWeek($daysOfWeek);

    $openingHour->setStartDate(new DateTime($from->format('Y-m-d 00:00:00')));
    $openingHour->setEndDate(new DateTime($to->format('Y-m-d 00:00:00')));

    $openingHour->setBeginHour(new DateTime($from->format('H:i')));
    $openingHour->setEndHour(new DateTime($to->format('H:i')));

    $openingHour->setMeetingMinutes($meetingMinutes);
    $openingHour->setIntervalMinutes($intervalMinutes);


    $this->openingHours[] = $openingHour;

    return $this;
  }

  public function build(): Calendar
  {
    $calendar = new Calendar();

    $calendar->setType($this->calendarType);
    $calendar->setOwner($this->calendarOwner);

    $calendar->setTitle('Calendar title');
    $calendar->setLocation('Calendar location');

    $calendar->setClosingPeriods($this->closingPeriods);

    foreach ($this->openingHours as $openingHour) {
      $calendar->addOpeningHour($openingHour);
    }

    return $calendar;

  }
}
