<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application\ImportCommand;

use App\Applications\Application\DTOs\ApplicationLiteData;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class ImportData
{
  /**
   * @OA\Property(ref=@Model(type=ApplicationLiteData::class))
   * @Groups({"read", "write"})
   */
  public ApplicationLiteData $application;

  /**
   * @var DocumentData[]
   * @Type("array<App\ApplicationLiteImport\Application\ImportCommand\DocumentData>")
   */
  public array $documents;

  /**
   * @var PaymentData[]
   * @Type("array<App\ApplicationLiteImport\Application\ImportCommand\PaymentData>")
   */
  public array $payments;
}
