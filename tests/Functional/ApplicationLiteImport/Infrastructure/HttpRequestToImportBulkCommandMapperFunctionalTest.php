<?php

declare(strict_types=1);

namespace Tests\Functional\ApplicationLiteImport\Infrastructure;

use App\ApplicationLiteImport\Application\ImportCommand\ImportBulkCommand;
use App\ApplicationLiteImport\Infrastructure\HttpRequestToImportBulkCommandMapper;
use App\Applications\Application\DTOs\ApplicationLiteData;
use App\Entity\CPSUser;
use App\Entity\Ente;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @group functional
 * @group ApplicationLite
 */
class HttpRequestToImportBulkCommandMapperFunctionalTest extends KernelTestCase
{
  private ?HttpRequestToImportBulkCommandMapper $mapper;
  private ?EntityManagerInterface $entityManager;
  private Ente $ente;

  protected function setUp(): void
  {
    $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
    $this->mapper = static::getContainer()->get(HttpRequestToImportBulkCommandMapper::class);

    $this->entityManager->beginTransaction();

    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');
    $this->entityManager->persist($ente);
    $this->ente = $ente;
  }

  protected function tearDown(): void
  {
    if ($this->entityManager->getConnection()->isTransactionActive()) {
      // Rollback della transazione alla fine del test
      $this->entityManager->rollback();
    }

    $this->entityManager->close();
    $this->entityManager = null;

    parent::tearDown();
  }

  public function testMapValidJson(): void
  {
    $json = <<<'JSON'
      [{
          "application": {
              "external_id": "12345",
              "subject": "Oggetto della pratica",
              "applicant": {
                  "given_name": "Mario",
                  "family_name": "Rossi",
                  "identifiers": {
                      "tax_code": "RSSMRA80A01H501Z"
                  }
              },
              "status": 2000,
              "service": {
                  "id": "7037f0f0-28eb-4237-9cc7-28c150f13a0d"
              },
              "actions": []
          }
      }]
      JSON;

    $requester = new CPSUser();

    $importCommand = $this->mapper->map($json, $this->ente, $requester);

    $this->assertInstanceOf(ImportBulkCommand::class, $importCommand);
    $this->assertCount(1, $importCommand->importData);
    $this->assertInstanceOf(ApplicationLiteData::class, $importCommand->importData[0]->application);
  }
}
