<?php

namespace App\Controller\Rest;

use App\Entity\CPSUser;
use App\Entity\FormIO;
use App\Entity\Pratica;
use App\FormIO\SchemaFactoryInterface;
use App\Services\Manager\PraticaManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Phpro\ApiProblem\Http\ForbiddenProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\NotFoundProblem;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use App\Entity\Servizio;

/**
 * Class AvailabilitiesAPIController
 * @property EntityManagerInterface $entityManager
 * @property LoggerInterface $logger
 * @package App\Controller
 * @Route("/submissions")
 */
class SubmissionsAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;
  private SchemaFactoryInterface $schemaFactory;
  private PraticaManager $praticaManager;


  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, SchemaFactoryInterface $schemaFactory, PraticaManager $praticaManager)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->schemaFactory = $schemaFactory;
    $this->praticaManager = $praticaManager;
  }


  /**
   * Generate a submission for a service with known user data
   * @Rest\Get("/service/{id}", name="submissions_api_service")
   *
   *
   * @OA\Parameter(
   *      name="id",
   *      in="path",
   *      @OA\Schema(
   *          type="string",
   *          example="71cb742a-c088-4b09-961f-61c53647eff7"
   *      ),
   *      required=true,
   *      description="Service id for generate user submission"
   * )
   *
   *
   * @OA\Response(
   *    response=200,
   *    description="Generated submission"
   * )
   *
   * @OA\Response(
   *    response=401,
   *    description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=403,
   *    description="Forbidden"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Service not found"
   * )
   *
   * @OA\Tag(name="submissions")
   *
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function submissionServiceAction($id): View
  {

    try {
      $user = $this->getUser();
      if (!$user instanceof CPSUser) {
        throw new ApiProblemException(
          new ForbiddenProblem('Current user can not generate submissions')
        );
      }

      $repositoryService = $this->entityManager->getRepository(Servizio::class);
      $service = $repositoryService->find($id);
      if (!$service instanceof Servizio) {
        throw new ApiProblemException(
          new NotFoundProblem("Service with id {$id} not found")
        );
      }

      $schema = $this->schemaFactory->createFromService($service);
      $cpsUserData = $this->praticaManager->getMappedFormDataWithUserData($schema, $user);
      $profileBlocksData = $this->praticaManager->getProfileBlockManager()->generateSubmissionFromProfileBlocks($user, $schema->getProfileBlocks());
      $result = array_merge($cpsUserData, $profileBlocksData);

      return $this->view($result, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('submissions_api_service', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('submissions_api_service', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  /**
   * Get an application submission by id, only application in draft status could be requested
   * @Rest\Get("/application/{id}", name="submissions_api_application")
   *
   *
   * @OA\Parameter(
   *      name="id",
   *      in="path",
   *      @OA\Schema(
   *          type="string",
   *          example="71cb742a-c088-4b09-961f-61c53647eff7"
   *      ),
   *      required=true,
   *      description="Application id for get submssion"
   * )
   *
   *
   * @OA\Response(
   *    response=200,
   *    description="Application submission"
   * )
   *
   * @OA\Response(
   *    response=401,
   *    description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=403,
   *    description="Forbidden"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   *
   * @OA\Response(
   *      response=400,
   *      description="Bad request"
   *  )
   *
   * @OA\Tag(name="submissions")
   *
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function submissionApplicationAction($id): View
  {

    try {

      if (!Uuid::isValid($id)) {
        throw new ApiProblemException(
          new BadRequestProblem("Id {$id} must be a valid UUID")
        );
      }

      $repositoryApplication = $this->entityManager->getRepository(FormIO::class);
      $application = $repositoryApplication->find($id);
      if (!$application instanceof FormIO) {
        throw new ApiProblemException(
          new NotFoundProblem("Application with id {$id} not found")
        );
      }

      $user = $this->getUser();
      if (!PraticaManager::isOwner($application, $user)) {
        throw new ApiProblemException(
          new ForbiddenProblem('Current user is not the owner of this application')
        );
      }

      if ($application->getStatus() !== Pratica::STATUS_DRAFT) {
        throw new ApiProblemException(
          new ForbiddenProblem('Submission of draft applications only can be retrieved')
        );
      }

      $data = $application->getDematerializedForms() ? $application->getDematerializedForms()['data'] : [];

      return $this->view($data, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('submissions_api_application', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('submissions_api_application', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

}
