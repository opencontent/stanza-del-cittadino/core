<?php

namespace App\View\Model;

use App\Entity\Pratica;
use Symfony\Contracts\Translation\TranslatorInterface;

class PraticaStatusChangeViewModel
{
  private Pratica $pratica;
  private TranslatorInterface $translator;

  public function __construct(
    TranslatorInterface $translator,
    Pratica             $pratica
  ) {
    $this->translator = $translator;
    $this->pratica = $pratica;
  }

  public function getHtmlTemplate(): string
  {
    if ($this->hasBeenAssignedToUserGroup()) {
      return 'Emails/Operatore/pratica_assigned_to_user_group.html.twig';
    }

    return 'Emails/Operatore/pratica_status_change.html.twig';
  }

  public function getTextTemplate(): string
  {
    if ($this->hasBeenAssignedToUserGroup()) {
      return 'Emails/Operatore/pratica_assigned_to_user_group.txt.twig';
    }

    return 'Emails/Operatore/pratica_status_change.txt.twig';
  }

  public function praticaStatusMessage(): string
  {
    $change = $this->pratica->lastStatusChangeFromStoricoStati();

    if ($this->hasBeenAssignedToOperatore($change)) {

      $statusMsg = 'email.pratica.operatore.status.assigned';

      return $this->translate(
        $statusMsg,
        [
          '%assigner_user_name%' => $change[1]['responsabile'],
          '%user_pratica%' => $this->pratica->getUser()->getFullName(),
          '%servizio%' => $this->pratica->getServizio()->getName(),
          '%protocollo%' => $this->pratica->getNumeroProtocollo(),
        ],
      );
    }

    if ($this->hasBeenAssignedToUserGroup()) {

      $statusMsg = 'email.pratica.user_group.assigned';

      return $this->translate(
        $statusMsg,
        [
          '%user_pratica%' => $this->pratica->getUser()->getFullName(),
          '%servizio%' => $this->pratica->getServizio()->getName(),
          '%user_group_name%' => $this->pratica->getUserGroup()->getName(),
        ],
      );
    }

    $statusCode = $change[0];
    $statusMsg = 'email.pratica.operatore.' . strtolower($this->pratica->getStatusNameByCode($statusCode));
    $statusMsg = str_replace('_', '.', $statusMsg);

    return $this->translate(
      $statusMsg,
      [
        '%user_pratica%' => $this->pratica->getUser()->getFullName(),
        '%servizio%' => $this->pratica->getServizio()->getName(),
        '%protocollo%' => $this->pratica->getNumeroProtocollo(),
      ],
    );
  }

  private function hasBeenAssignedToUserGroup(): bool
  {
    $user_name = $this->pratica->getOperatore() ? $this->pratica->getOperatore()->getFullName() : null;
    $user_group_name = $this->pratica->getUserGroup() ? $this->pratica->getUserGroup()->getName() : null;

    if ($user_name === null && $user_group_name !== null) {
      return true;
    }

    return false;
  }

  private function hasBeenAssignedToOperatore(array $change): bool
  {
    $statusCode = $change[0];

    return $statusCode == Pratica::STATUS_PENDING
      && !empty($change[1]['responsabile'])
      && !empty($change[1]['operatore'])
      && $change[1]['responsabile'] != $change[1]['operatore'];
  }

  private function translate(string $statusMsg, array $params): string
  {
    $locale = $this->pratica->getLocale();

    return $this->translator->trans($statusMsg, $params, null, $locale);

  }

}
