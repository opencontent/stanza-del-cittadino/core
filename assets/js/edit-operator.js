import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import './utils/datatables'

$(document).ready(function () {
  const systemUserCheckbox = $('#App_operatoreuser_system_user');

  // Change email requirements and hide services on system user checkbox change
  systemUserCheckbox.on("change", function () {
    let emailField = $('#App_operatoreuser_email');
    let label = $("label[for='App_operatoreuser_email']");
    let servicesList = $('#servicesList');
    let servicesListAlert = $('#servicesListAlert');

    if ($(this).is(":checked")) {
      emailField.removeAttr('required');
      label.removeClass('required');
      servicesList.addClass('d-none');
      servicesListAlert.removeClass('d-none');
    } else {
      emailField.attr('required', 'required');
      label.addClass('required');
      servicesList.removeClass('d-none');
      servicesListAlert.addClass('d-none');
    }
  }).trigger("change");

  const GROUPS = $("#mainList").find("input.group");
  const CHILDREN = $("#mainList").find("input").not(".group");

  GROUPS.on("change", function (e, isPageLoad) {
    let children = $(this).parents("li:first").find("ul").find("input");
    if ($(this).is(":checked")) {
      if (!isPageLoad) children.prop("checked", true);
    } else {
      if (!isPageLoad) children.prop("checked", false);
    }
  }).trigger("change", [true]);

  CHILDREN.on("change", function () {
    checkGroups();
  });

  function checkGroups() {
    GROUPS.each((idx, el) => {
      let children = $(el).parents("li:first").find("ul").find("input");
      if (children.length > 0) {
        let countChecked = 0;
        $(children).each((idx, elm) => {
          if ($(elm).is(":checked")) {
            countChecked++;
          }
        });
        if (countChecked === $(children).length) {
          $(el).prop("checked", true);
        } else {
          $(el).prop("checked", false);
        }
      }
    });
  }

  checkGroups();

  const $tableContainer = $('#operators');
  if ($tableContainer.length > 0) {
    let table; //Table reference
    const datatableSetting = JSON.parse(decodeURIComponent($tableContainer.data('config')));
    const datatableOptions = {
      dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      searching: true,
      paging: true,
      pagingType: 'simple_numbers',
    };

    $tableContainer.initDataTables(datatableSetting, datatableOptions)
      .then(function (dt) {
        // dt contains the initialized instance of DataTables
        table = dt.api();
      });
  }

});
