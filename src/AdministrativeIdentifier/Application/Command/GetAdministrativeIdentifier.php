<?php

namespace App\AdministrativeIdentifier\Application\Command;

class GetAdministrativeIdentifier
{
  private string $id;

  public function __construct(
    string $id
  ) {
    $this->id = $id;
  }

  public function getId(): string
  {
    return $this->id;
  }
}
