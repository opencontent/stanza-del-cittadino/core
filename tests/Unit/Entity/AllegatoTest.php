<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Allegato;
use App\Entity\Pratica;
use PHPUnit\Framework\TestCase;

class AllegatoTest extends TestCase
{
  public function testCanAddAndRemovePratica(): void
  {
    $allegato = new Allegato();
    $pratica = new Pratica();

    $this->assertEquals(0, $allegato->getPratiche()->count());

    $allegato->addPratica($pratica);
    $this->assertEquals(1, $allegato->getPratiche()->count());

    $allegato->addPratica($pratica);
    $this->assertEquals(1, $allegato->getPratiche()->count());

    $allegato->removePratica($pratica);
    $this->assertEquals(0, $allegato->getPratiche()->count());
  }
}
