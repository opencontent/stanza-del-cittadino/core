<?php

namespace App\Doctrine\DBAL\Detector;

class PostgresGoneAwayDetector implements GoneAwayDetector
{
  /** @var string[] */
  protected array $goneAwayExceptions = [
    'Database server has gone away',
    'Lost connection to Database server during query',
    'terminating current active connection due to forced scale event',
    'SSL connection has been closed unexpectedly',
    'server closed the connection unexpectedly',
  ];

  /** @var string[] */
  protected array $goneAwayInUpdateExceptions = [
    'Database server has gone away',
    'terminating current active connection due to forced scale event',
    'SSL connection has been closed unexpectedly',
    'server closed the connection unexpectedly',
  ];

  public function isGoneAwayException(\Throwable $exception, string $sql = null): bool
  {
    if ($this->isSavepoint($sql)) {
      return false;
    }

    $possibleMatches = $this->isUpdateQuery($sql)
      ? $this->goneAwayInUpdateExceptions
      : $this->goneAwayExceptions;

    $message = $exception->getMessage();

    foreach ($possibleMatches as $goneAwayException) {
      if (str_contains($message, $goneAwayException)) {
        return true;
      }
    }

    return false;
  }

  private function isUpdateQuery(?string $sql): bool
  {
    return !preg_match('/^[\s\n\r\t(]*(select|show|describe)[\s\n\r\t(]+/i', (string)$sql);
  }

  /**
   * @see \Doctrine\DBAL\Platforms\AbstractPlatform::createSavePoint
   */
  private function isSavepoint(?string $sql): bool
  {
    return str_starts_with(trim((string)$sql), 'SAVEPOINT');
  }
}
