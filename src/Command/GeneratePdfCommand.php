<?php

namespace App\Command;

use App\Services\ModuloPdfBuilderService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GeneratePdfCommand extends Command
{

  /**
   * @var RouterInterface
   */
  private RouterInterface $router;

  /**
   * @var string
   */
  private string $scheme;

  /**
   * @var string
   */
  private string $host;
  /**
   * @var EntityManagerInterface
   */
  private EntityManagerInterface $entityManager;
  /**
   * @var string
   */
  private string $locale;
  /**
   * @var TranslatorInterface
   */
  private TranslatorInterface $translator;
  /**
   * @var ModuloPdfBuilderService
   */
  private
  ModuloPdfBuilderService $moduloPdfBuilderService;

  /**
   * @param EntityManagerInterface $entityManager
   * @param RouterInterface $router
   * @param TranslatorInterface $translator
   * @param ModuloPdfBuilderService $moduloPdfBuilderService
   * @param string $locale
   * @param string $scheme
   * @param string $host
   */
  public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, TranslatorInterface $translator, ModuloPdfBuilderService $moduloPdfBuilderService, string $locale, string $scheme, string $host)
  {
    $this->entityManager = $entityManager;
    $this->router = $router;
    $this->translator = $translator;
    $this->locale = $locale;
    $this->scheme = $scheme;
    $this->host = $host;
    parent::__construct();

    $this->moduloPdfBuilderService = $moduloPdfBuilderService;
  }

  protected function configure()
  {
    $this
      ->setName('ocsdc:generate-pdf')
      ->setDescription('Generate pdf for applications')
      ->addOption('update', null, InputOption::VALUE_NONE, 'Override existing pdf')
      ->addOption('ids', null, InputOption::VALUE_OPTIONAL, 'List of comma separated application ids');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $io = new SymfonyStyle($input, $output);

    $this->translator->setLocale($this->locale);
    $context = $this->router->getContext();
    $context->setHost($this->host);
    $context->setScheme($this->scheme);

    $needsUpdate = $input->getOption('update');
    $applicationIdsOption = $input->getOption('ids');

    $isInteractive = true;
    if (!empty($applicationIdsOption)) {
      $isInteractive = false;
    }

    if ($isInteractive) {
      $helper = $this->getHelper('question');

      $question = new Question('Inserisci id della pratica (se più di uno separati da ,): ', '');
      $applicationIdsOption = $helper->ask($input, $output, $question);
    }

    $applicationIds = explode(',', $applicationIdsOption);

    if (empty($applicationIds)) {
      $io->error('Devi inserire un id di pratica o di più pratiche separate da virgola');
      return 1;
    }

    $repository = $this->entityManager->getRepository('App\Entity\Pratica');

    $count = 0;
    foreach ($applicationIds as $applicationId) {
      try {
        $application = $repository->find($applicationId);

        if (!$application) {
          $io->error("La pratica con id: {$applicationId} non esiste");
          continue;
        }

        if (!$needsUpdate || $application->getModuliCompilati()->isEmpty()) {
          $pdf = $this->moduloPdfBuilderService->createForPratica($application);
          $application->addModuloCompilato($pdf);
          $io->success("Generato pdf per la pratica: {$applicationId}");
        } else {
          $this->moduloPdfBuilderService->updateForPratica($application);
          // Non è necessario aggiungere il pdf perchè viene sostituito il modulo compilato corrente
          $io->success("Rigenerato pdf per la pratica: {$applicationId}");
        }

        $count ++;
      } catch (Exception $e) {
        $io->error("Si è verificato un errore nella generazione del pdf per la pratica {$applicationId}: {$e->getMessage()}");
      }
    }
    $this->entityManager->flush();

    $io->success("Sono stati generati {$count} pdf");

    return 0;

  }
}
