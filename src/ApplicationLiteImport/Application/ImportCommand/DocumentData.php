<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application\ImportCommand;

class DocumentData
{
  public string $type;
  public string $registration_code;
  public MainDocument $main_document;
}
