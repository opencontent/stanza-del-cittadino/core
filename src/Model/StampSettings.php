<?php


namespace App\Model;


class StampSettings implements \JsonSerializable
{

  const PHASE_REQUEST = 'request';
  const PHASE_RELEASE = 'release';

  private string $reason;

  private string $identifier;

  private float $amount;

  private string $phase;

  /**
   * @return string
   */
  public function getReason(): string
  {
    return $this->reason;
  }

  /**
   * @param string $reason
   */
  public function setReason(string $reason): void
  {
    $this->reason = $reason;

  }

  /**
   * @return string
   */
  public function getIdentifier(): string
  {
    return $this->identifier;
  }

  /**
   * @param string|null $identifier
   */
  public function setIdentifier(string $identifier): void
  {
    $this->identifier = $identifier;
  }

  /**
   * @return float
   */
  public function getAmount(): float
  {
    return $this->amount;
  }

  /**
   * @param float $amount
   */
  public function setAmount(float $amount): void
  {
    $this->amount = $amount;
  }

  /**
   * @return string
   */
  public function getPhase(): string
  {
    return $this->phase;
  }

  /**
   * @param string $phase
   */
  public function setPhase(string $phase): void
  {
    $this->phase = $phase;
  }

  public function jsonSerialize(): array
  {
    return [
      'reason' => $this->reason,
      'identifier' => $this->identifier,
      'amount' => $this->amount,
      'phase' => $this->phase
    ];
  }

  public static function fromArray($data = []): StampSettings
  {
    $stamp = new self();
    $stamp->setReason($data['reason'] ?? '');
    $stamp->setIdentifier($data['identifier'] ?? '');
    $stamp->setAmount($data['amount'] ?? 0);
    $stamp->setPhase($data['phase'] ?? self::PHASE_REQUEST);

    return $stamp;
  }

}
