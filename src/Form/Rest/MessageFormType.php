<?php

namespace App\Form\Rest;


use App\Dto\Message as MessageDto;
use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class MessageFormType extends AbstractType
{

  /** @var array  */
  private array $allowedExtensions;

  /** @var TranslatorInterface  */
  private TranslatorInterface $translator;

  /**
   * @param $allowedExtensions
   * @param TranslatorInterface $translator
   */
  public function __construct(TranslatorInterface $translator, $allowedExtensions)
  {
    $this->translator = $translator;
    $this->allowedExtensions = array_merge(...$allowedExtensions);
  }

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    /** @var MessageDto $message */
    $message = $builder->getData();

    $builder
      ->add('sent_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('read_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('clicked_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('protocolled_at', DateTimeType::class, [
        'widget' => 'single_text',
        'required' => false,
        'empty_data' => ''
      ])
      ->add('protocol_number')
      ->add('external_id')
      ->add('protocol_required', CheckboxType::class, [
        'required' => false
      ]);


    if (!$message->getId()) {
      // Campi modificabili solo alla creazione del messaggio
      $builder
        ->add('message')
        ->add('subject')
        ->add('email', EmailType::class, [
          'required' => false
        ])
        ->add('visibility', ChoiceType::class, [
          'choices' => [Message::VISIBILITY_APPLICANT, Message::VISIBILITY_INTERNAL],
          'empty_data' => Message::VISIBILITY_APPLICANT
        ])
        ->add('attachments', CollectionType::class, [
          'entry_type' => FileType::class,
          "allow_add" => true,
          "allow_delete" => true,
          'prototype' => true,
          "label" => false,
        ]);
    }

    $builder->addEventListener(FormEvents::SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event)
  {
    /** @var Message $data */
    $data = $event->getData();

    $extraData = $event->getForm()->getExtraData();
    $nonEditableFields = ['message', 'subject', 'email', 'visibility', 'attachments'];

    foreach (array_keys($extraData) as $k) {
      if (in_array($k, $nonEditableFields)) {
        $event->getForm()->addError(
          new FormError($this->translator->trans('form_errors.non_editable_field', ['%fieldName%' => $k]))
        );
      }
    }

    $uploadedAttachments = $data->getAttachments() ?? [];
    foreach ($uploadedAttachments as $attachment) {
      if ($attachment->getId() == null) {
        if (!in_array($attachment->getMimeType(), $this->allowedExtensions)) {
          return $event->getForm()->addError(
            new FormError($this->translator->trans('documenti.documento.invalid_mime_type'))
          );
        }

        $extension = explode('.', $attachment->getName());
        if (count($extension) < 2) {
          return $event->getForm()->addError(
            new FormError($this->translator->trans('documenti.documento.missing_extension', ['%fieldName%' => 'name']))
          );

        } else if (!array_key_exists(end($extension), $this->allowedExtensions)) {
          return $event->getForm()->addError(new FormError($this->translator->trans('documenti.documento.invalid_extension')));
        }

        if (empty($attachment->getFile())) {
          return $event->getForm()->addError(new FormError($this->translator->trans('form_errors.mandatory_field', ['%fieldName%', 'file'])));
        }
      }
    }
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Dto\Message',
      'allow_extra_fields' => true,
      'csrf_protection' => false
    ));
  }

}
