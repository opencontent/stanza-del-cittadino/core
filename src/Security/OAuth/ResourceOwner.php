<?php

namespace App\Security\OAuth;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class ResourceOwner implements ResourceOwnerInterface
{
  private array $response;

  private string $codiceFiscale;

  private ?string $capDomicilio;

  private ?string $capResidenza;

  private ?string $cellulare;

  private ?string $cittaDomicilio;

  private ?string $cittaResidenza;

  private ?string $cognome;

  private ?string $dataNascita;

  private ?string $emailAddress;

  private ?string $emailAddressPersonale;

  private ?string $indirizzoDomicilio;

  private ?string $indirizzoResidenza;

  private ?string $luogoNascita;

  private ?string $nome;

  private ?string $provinciaDomicilio;

  private ?string $provinciaNascita;

  private ?string $provinciaResidenza;

  private ?string $sesso;

  private ?string $statoDomicilio;

  private ?string $statoNascita;

  private ?string $statoResidenza;

  private ?string $telefono;

  private ?string $titolo;

  private ?string $idCard;

  private ?string $spidCode;

  private ?string $authenticationMethod;

  private ?string $sessionId;

  private int $spidLevel;

  private ?string $instant;

  private ?string $provider;

  private ?string $sessionIndex;

  public function __construct(array $response)
  {
    $this->response = $response;
  }

  public function getId()
  {
    return $this->codiceFiscale;
  }

  public function toArray(): array
  {
    $properties = get_object_vars($this);
    foreach ($properties as $key => $value) {
      if (empty($value)) {
        $properties[$key] = null;
      }
    }

    return $properties;
  }

  /**
   * @param string $codiceFiscale
   * @return ResourceOwner
   */
  public function setCodiceFiscale(string $codiceFiscale): ResourceOwner
  {
    $this->codiceFiscale = $codiceFiscale;
    return $this;
  }

  /**
   * @param string|null $capDomicilio
   * @return ResourceOwner
   */
  public function setCapDomicilio(?string $capDomicilio): ResourceOwner
  {
    $this->capDomicilio = $capDomicilio;
    return $this;
  }

  /**
   * @param string|null $capResidenza
   * @return ResourceOwner
   */
  public function setCapResidenza(?string $capResidenza): ResourceOwner
  {
    $this->capResidenza = $capResidenza;
    return $this;
  }

  /**
   * @param string|null $cellulare
   * @return ResourceOwner
   */
  public function setCellulare(?string $cellulare): ResourceOwner
  {
    $this->cellulare = $cellulare;
    return $this;
  }

  /**
   * @param string|null $cittaDomicilio
   * @return ResourceOwner
   */
  public function setCittaDomicilio(?string $cittaDomicilio): ResourceOwner
  {
    $this->cittaDomicilio = $cittaDomicilio;
    return $this;
  }

  /**
   * @param string|null $cittaResidenza
   * @return ResourceOwner
   */
  public function setCittaResidenza(?string $cittaResidenza): ResourceOwner
  {
    $this->cittaResidenza = $cittaResidenza;
    return $this;
  }

  /**
   * @param string|null $cognome
   * @return ResourceOwner
   */
  public function setCognome(?string $cognome): ResourceOwner
  {
    $this->cognome = $cognome;
    return $this;
  }

  /**
   * @param string|null $dataNascita
   * @return ResourceOwner
   */
  public function setDataNascita(?string $dataNascita): ResourceOwner
  {
    $this->dataNascita = $dataNascita;
    return $this;
  }

  /**
   * @param string|null $emailAddress
   * @return ResourceOwner
   */
  public function setEmailAddress(?string $emailAddress): ResourceOwner
  {
    $this->emailAddress = $emailAddress;
    return $this;
  }

  /**
   * @param string|null $emailAddressPersonale
   * @return ResourceOwner
   */
  public function setEmailAddressPersonale(?string $emailAddressPersonale): ResourceOwner
  {
    $this->emailAddressPersonale = $emailAddressPersonale;
    return $this;
  }

  /**
   * @param string|null $indirizzoDomicilio
   * @return ResourceOwner
   */
  public function setIndirizzoDomicilio(?string $indirizzoDomicilio): ResourceOwner
  {
    $this->indirizzoDomicilio = $indirizzoDomicilio;
    return $this;
  }

  /**
   * @param string|null $indirizzoResidenza
   * @return ResourceOwner
   */
  public function setIndirizzoResidenza(?string $indirizzoResidenza): ResourceOwner
  {
    $this->indirizzoResidenza = $indirizzoResidenza;
    return $this;
  }

  /**
   * @param string|null $luogoNascita
   * @return ResourceOwner
   */
  public function setLuogoNascita(?string $luogoNascita): ResourceOwner
  {
    $this->luogoNascita = $luogoNascita;
    return $this;
  }

  /**
   * @param string|null $nome
   * @return ResourceOwner
   */
  public function setNome(?string $nome): ResourceOwner
  {
    $this->nome = $nome;
    return $this;
  }

  /**
   * @param string|null $provinciaDomicilio
   * @return ResourceOwner
   */
  public function setProvinciaDomicilio(?string $provinciaDomicilio): ResourceOwner
  {
    $this->provinciaDomicilio = $provinciaDomicilio;
    return $this;
  }

  /**
   * @param string|null $provinciaNascita
   * @return ResourceOwner
   */
  public function setProvinciaNascita(?string $provinciaNascita): ResourceOwner
  {
    $this->provinciaNascita = $provinciaNascita;
    return $this;
  }

  /**
   * @param string|null $provinciaResidenza
   * @return ResourceOwner
   */
  public function setProvinciaResidenza(?string $provinciaResidenza): ResourceOwner
  {
    $this->provinciaResidenza = $provinciaResidenza;
    return $this;
  }

  /**
   * @param string|null $sesso
   * @return ResourceOwner
   */
  public function setSesso(?string $sesso): ResourceOwner
  {
    $this->sesso = $sesso;
    return $this;
  }

  /**
   * @param string|null $statoDomicilio
   * @return ResourceOwner
   */
  public function setStatoDomicilio(?string $statoDomicilio): ResourceOwner
  {
    $this->statoDomicilio = $statoDomicilio;
    return $this;
  }

  /**
   * @param string|null $statoNascita
   * @return ResourceOwner
   */
  public function setStatoNascita(?string $statoNascita): ResourceOwner
  {
    $this->statoNascita = $statoNascita;
    return $this;
  }

  /**
   * @param string|null $statoResidenza
   * @return ResourceOwner
   */
  public function setStatoResidenza(?string $statoResidenza): ResourceOwner
  {
    $this->statoResidenza = $statoResidenza;
    return $this;
  }

  /**
   * @param string|null $telefono
   * @return ResourceOwner
   */
  public function setTelefono(?string $telefono): ResourceOwner
  {
    $this->telefono = $telefono;
    return $this;
  }

  /**
   * @param string|null $titolo
   * @return ResourceOwner
   */
  public function setTitolo(?string $titolo): ResourceOwner
  {
    $this->titolo = $titolo;
    return $this;
  }

  /**
   * @param string|null $idCard
   * @return ResourceOwner
   */
  public function setIdCard(?string $idCard): ResourceOwner
  {
    $this->idCard = $idCard;
    return $this;
  }

  /**
   * @param string|null $spidCode
   * @return ResourceOwner
   */
  public function setSpidCode(?string $spidCode): ResourceOwner
  {
    $this->spidCode = $spidCode;
    return $this;
  }

  /**
   * @param string|null $authenticationMethod
   * @return ResourceOwner
   */
  public function setAuthenticationMethod(?string $authenticationMethod): ResourceOwner
  {
    $this->authenticationMethod = $authenticationMethod;
    return $this;
  }

  /**
   * @param string|null $sessionId
   * @return ResourceOwner
   */
  public function setSessionId(?string $sessionId): ResourceOwner
  {
    $this->sessionId = $sessionId;
    return $this;
  }

  /**
   * @param int $spidLevel
   * @return ResourceOwner
   */
  public function setSpidLevel(int $spidLevel): ResourceOwner
  {
    $this->spidLevel = $spidLevel;
    return $this;
  }

  /**
   * @param string|null $instant
   * @return ResourceOwner
   */
  public function setInstant(?string $instant): ResourceOwner
  {
    $this->instant = $instant;
    return $this;
  }

  /**
   * @param string|null $provider
   * @return ResourceOwner
   */
  public function setProvider(?string $provider): ResourceOwner
  {
    $this->provider = $provider;
    return $this;
  }

  /**
   * @param string|null $sessionIndex
   * @return ResourceOwner
   */
  public function setSessionIndex(?string $sessionIndex): ResourceOwner
  {
    $this->sessionIndex = $sessionIndex;
    return $this;
  }

}
