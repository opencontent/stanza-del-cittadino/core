<?php


namespace App\Security\Voters;


use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Handlers\Servizio\ForbiddenAccessException;
use App\Handlers\Servizio\ServizioHandlerRegistry;
use App\Services\Manager\PraticaManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ApplicationVoter extends Voter
{

  public const EDIT = 'edit';
  public const VIEW = 'view';
  public const SUBMIT = 'submit';
  public const ASSIGN = 'assign';
  public const ACCEPT_OR_REJECT = 'accept_or_reject';
  public const CHANGE_STATUS = 'change_status';
  public const WITHDRAW = 'withdraw';
  public const COMPILE = 'compile';
  public const DELETE = 'delete';

  private ServizioHandlerRegistry $servizioHandlerRegistry;
  private Security $security;

  public function __construct(Security $security, ServizioHandlerRegistry $servizioHandlerRegistry)
  {
    $this->security = $security;
    $this->servizioHandlerRegistry = $servizioHandlerRegistry;
  }

  protected function supports($attribute, $subject): bool
  {
    // if the attribute isn't one we support, return false
    if (!in_array($attribute, [
      self::EDIT,
      self::VIEW,
      self::SUBMIT,
      self::ASSIGN,
      self::ACCEPT_OR_REJECT,
      self::CHANGE_STATUS,
      self::WITHDRAW,
      self::COMPILE,
      self::DELETE
    ])) {
      return false;
    }

    // only vote on `Pratica` objects
    if ($subject && !$subject instanceof Pratica) {
      return false;
    }


    return true;
  }

  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    // you know $subject is a Pratica object, thanks to `supports()`
    /** @var Pratica $pratica */
    $pratica = $subject;

    switch ($attribute) {
      case self::SUBMIT:
      case self::EDIT:
        return $this->canEdit($pratica, $user);
      case self::VIEW:
        return $this->canView($pratica, $user);
      case self::ASSIGN:
        return $this->canAssign($pratica, $user);
      case self::ACCEPT_OR_REJECT:
        return $this->canAcceptOrReject($pratica, $user);
      case self::CHANGE_STATUS:
        return $this->canChangeStatus($pratica, $user);
      case self::WITHDRAW:
        return $this->canWithdraw($pratica, $user);
      case self::COMPILE:
        return $this->canCompile($pratica, $user);
      case self::DELETE:
        return $this->canDelete($pratica, $user);
    }

    throw new \LogicException('This code should not be reached!');
  }

  private function canView(Pratica $pratica, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      if (PraticaManager::isOperatorEnabled($pratica, $user) || PraticaManager::isOperatorAssigned($pratica, $user)) {
        return true;
      }
    }

    if (PraticaManager::isOwner($pratica, $user)) {
      return true;
    }

    $cfs = $pratica->getRelatedCFs();
    if (!is_array($cfs)) {
      $cfs = [$cfs];
    }
    return $user instanceof CPSUser && in_array($user->getCodiceFiscale(), $cfs);
  }

  private function canChangeStatus(Pratica $pratica, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      if (PraticaManager::isOperatorEnabled($pratica, $user) || PraticaManager::isOperatorAssigned($pratica, $user) || $user->isSystemUser()) {
        return true;
      }
    }

    return false;
  }

  private function canEdit(Pratica $pratica, UserInterface $user): bool
  {
    if ($this->security->isGranted('ROLE_ADMIN')) {
      return true;
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {
      /** @var OperatoreUser $user */
      if (PraticaManager::isOperatorEnabled($pratica, $user) || PraticaManager::isOperatorAssigned($pratica, $user)) {
        return true;
      }
    }

    return (PraticaManager::isOwner($pratica, $user) && $this->isServiceActive($pratica));
  }

  private function canAssign(Pratica $pratica, UserInterface $user): bool
  {

    if (in_array($pratica->getStatus(), Pratica::FINAL_STATES)) {
      return false;
    }

    /** @var OperatoreUser $user */
    if ($this->security->isGranted('ROLE_OPERATORE') && (PraticaManager::isOperatorEnabled($pratica, $user) || PraticaManager::isOperatorAssigned($pratica, $user, true))) {
      return true;
    }
    return false;
  }

  private function canAcceptOrReject(Pratica $pratica, UserInterface $user): bool
  {
    // Fixme: Solo l'operatore che ha direttamente in carico la pratica può accettare o rifiutare
    /** @var OperatoreUser $user */
    if ($this->security->isGranted('ROLE_OPERATORE')){
      /** @var OperatoreUser $user */
      if ($user->isSystemUser() || PraticaManager::isOperatorAssigned($pratica, $user, true)) {
        return true;
      }
    }
    return false;
  }

  private function canWithdraw(Pratica $pratica, UserInterface $user): bool
  {
    // Non è possibile ritirare una pratica se non è abilitato il ritiro, se è presente un dovuto di pagamento o se l'utente non è il richiedente
    if (!empty($pratica->getPaymentData()) || !$pratica->getServizio()->isAllowWithdraw() || $pratica->getUser()->getId() !== $user->getId()) {
      return false;
    }

    // Se il servizio ha un workflow di tipo inoltro e la pratica è stata "inoltrata" NON deve comparire il pulsante ritira.
    if ($pratica->getStatus() !== Pratica::STATUS_PRE_SUBMIT && $pratica->getServizio()->getWorkflow() == Servizio::WORKFLOW_FORWARD) {
      return false;
    }

    // Se il servizio ha la protocollazione attiva ed e la pratica è in STATUS_PRE_SUBMIT, altrimenti genera un errore in fase di protocollazzione del documento di ritiro
    if ($pratica->getStatus() === Pratica::STATUS_PRE_SUBMIT && $pratica->getServizio()->isProtocolRequired()) {
      return false;
    }

    return in_array($pratica->getStatus(), [Pratica::STATUS_PRE_SUBMIT, Pratica::STATUS_SUBMITTED, Pratica::STATUS_REGISTERED, Pratica::STATUS_PENDING]);
  }

  private function canCompile(Pratica $pratica, UserInterface $user): bool
  {
    $canCompile = false;
    // se la pratica è in bozza oppure in attesa di creazione del pagamento
    if (PraticaManager::isOwner($pratica, $user) && (in_array($pratica->getStatus(), [Pratica::STATUS_DRAFT, Pratica::STATUS_DRAFT_FOR_INTEGRATION]) || $pratica->needsPayment() || $pratica->hasStampsToPay())) {
      $canCompile = $this->isServiceActive($pratica);
    }
    return $canCompile;
  }

  private function canDelete(Pratica $pratica, UserInterface $user): bool
  {
    return PraticaManager::isOwner($pratica, $user);
  }

  private function isServiceActive(Pratica $pratica): bool
  {
    $handler = $this->servizioHandlerRegistry->getByName($pratica->getServizio()->getHandler());
    try {
      $handler->canAccess($pratica->getServizio());
      return true;
    } catch (ForbiddenAccessException $e) {
      return false;
    }
  }
}
