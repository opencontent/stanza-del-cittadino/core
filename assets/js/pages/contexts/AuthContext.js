import React, {useMemo} from 'react';
import {createContext, useContext, useEffect, useState} from "react";
import axios from "axios"

export const AuthContext = createContext();

export function useAuth(){
  return useContext(AuthContext)
}

export function AuthProvider(props) {

  const [authUser,setAuthUser] = useState(null);
  const [isLoggedIn,setIsLoggedIn] = useState(false);

  useEffect( () => {
    if (!authUser) {
      let isCancelled = false
     const fetchData =  async () => {
        const currentUser = await axios.get(`/${window.location.pathname.split('/')[1]}/api/session-auth`);
       if(!isCancelled){
          setAuthUser(currentUser.data);
        }
      }
      fetchData()

      return () => {
        isCancelled = true
      }
    }
  },[])

  const value = useMemo(() => ({
    authUser,
    setAuthUser,
    isLoggedIn,
    setIsLoggedIn
  }),[authUser])

  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  )
}
