import '../../styles/components/_fileupload.scss';
import Dropzone from "dropzone";
import axios from "axios";
Dropzone.autoDiscover = false;

const humanReadableFileSize = (size) => {
  let selectedSize = 0;
  let selectedUnit = "b";
  if (size > 0) {
    let units = ["TB", "GB", "MB", "KB", "B"];

    for (let i = 0; i < units.length; i++) {
      let unit = units[i];
      let cutoff = Math.pow(1000, 4 - i) / 10;
      if (size >= cutoff) {
        selectedSize = size / Math.pow(1000, 4 - i);
        selectedUnit = unit;
        break;
      }
    }
    selectedSize = Math.round(10 * selectedSize) / 10; // Cutting of digits
  }
  return `${selectedSize} ${selectedUnit}`;
}

const defaultSettings = {
  previewTemplate: `
          <li class="dz-preview dz-file-preview upload-file">
            <svg class="icon icon-sm" aria-hidden="true"><use href="/bootstrap-italia/dist/svg/sprite.svg#it-file"></use></svg>
            <p class="dz-details">
              <span class="sr-only">File caricato:</span>
              <span data-dz-name></span> <span class="upload-file-weight" data-dz-size></span>
            </p>
            <div class="dz-error-message"><span class="px-2 text-danger small" data-dz-errormessage></span></div>
            <button type="button" data-dz-remove>
              <span class="sr-only">Elimina file caricato</span>
              <svg class="icon" aria-hidden="true"><use href="/bootstrap-italia/dist/svg/sprite.svg#it-close"></use></svg>
            </button>
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </li>`,
  //previewsContainer: `<ul className="upload-file-list dropzone-previews"></ul>`, // Define the container to display the previews
  //clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
}


const extend = (obj, ext) => {
  Object.keys(ext).forEach((key) => {
    obj[key] = ext[key];
  });
  return obj;
}
class FileUpload{
  static init(elm, opt) {
    if ($(elm).length > 0) {
      $(elm).each((_, el) => {
        const $uploadZone = $(el);
        const instancePrefix = $uploadZone.data('instance-prefix');
        const locale = document.documentElement.lang.toString();
        const previewContainerId = 'dz-' + Math.random().toString(16).slice(2);
        const def = defaultSettings;
        let attachments = []
        $uploadZone.parent().append(`<ul class="upload-file-list dropzone-previews" id="${previewContainerId}"></ul>`)

        let attr = opt ? extend(def, opt) : def;
        attr = extend(attr, $uploadZone.data());
        attr.previewsContainer = '#' + previewContainerId;

        $uploadZone.addClass('dropzone');

        const dz = new Dropzone(el, attr);
        if (!attr.multiple) {
          dz.hiddenFileInput.removeAttribute('multiple');
        }

        dz.on("success", function (file, response) {
          file.remote_id = response.id;
          attachments.push({
            id: response.id,
            name: response.name,
          });
          file.previewElement.querySelector(".progress").remove();

          if (dz.options.inputfield !== undefined) {
            $('#'+ dz.options.inputfield).val(JSON.stringify(attachments));
          }
        })

        dz.on("addedfile", function(file) {
          file.previewElement.classList.add('success');
          let fileSizeElement = file.previewElement.querySelector("[data-dz-size]");
          fileSizeElement.innerHTML = humanReadableFileSize(file.size);  // Custom size display
        });


        // Update the total progress bar
        dz.on("uploadprogress", function(file, progress, byteSent) {
          const progressBar = file.previewElement.querySelector('.progress-bar');
          if (progressBar === undefined || progressBar === null) {
            return;
          }
          progressBar.style.width = progress + '%';
        });

        dz.on('removedfile', function (file) {
          attachments = attachments.filter(att => att.id !== file.remote_id);
          $('#'+ dz.options.inputfield).val(JSON.stringify(attachments));
          if (file.remote_id !== undefined) {
            axios.delete(`/${instancePrefix}/${locale}/allegati/` + file.remote_id)
              .then((response) => {})
              .catch(function (error) {
                console.log(error);
              });
          }
        });

        /*dz.on("sending", function(file) {
          // Show the total progress bar when upload starts
          document.querySelector("#total-progress").style.opacity = "1";
          // And disable the start button
          file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        dz.on("queuecomplete", function(progress) {
          document.querySelector("#total-progress").style.opacity = "0";
        });

        // Setup the buttons for all transfers
        // The "add files" button doesn't need to be setup because the config
        // `clickable` has already been specified.
        document.querySelector("#actions .start").onclick = function() {
          dz.enqueueFiles(dz.getFilesWithStatus(Dropzone.ADDED));
        };
        document.querySelector("#actions .cancel").onclick = function() {
          dz.removeAllFiles(true);
        };*/

        return dz;
      });
    }
  }
}

export default FileUpload;
