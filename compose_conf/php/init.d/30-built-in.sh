#!/bin/bash

[[ $DEBUG == 1 ]] && set -x

if [[ $ENABLE_MIGRATIONS == 'true' ]]; then

  echo "==> Running built-in updates"

  # initialization && migrations on municipalities
  for identifier in $(./bin/tenants); do

    echo " * Running built-in updates on $identifier"
    bin/console ocsdc:built-in-services-import -f "./data/built-in/bookings.json" -i "$identifier" --no-interaction
    bin/console ocsdc:built-in-services-import -f "./data/built-in/helpdesk.json" -i "$identifier" --no-interaction
    bin/console ocsdc:built-in-services-import -f "./data/built-in/inefficiencies.json" -i "$identifier" --no-interaction

  done
fi
