<?php


namespace App\Dto;


use App\Entity\Allegato;
use App\Entity\AllegatoMessaggio;
use App\Entity\CPSUser;
use App\Entity\Message as MessageEntity;
use App\Entity\Pratica;
use App\Entity\User;
use App\Model\File;
use App\Utils\StringUtils;
use DateTime;
use Exception;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;
use Nelmio\ApiDocBundle\Annotation\Model;

class Message
{

  const TRANSMISSION_TYPE_INBOUND = 'inbound';
  const TRANSMISSION_TYPE_OUTBOUND = 'outbound';
  const TRANSMISSION_TYPE_INTERNAL = 'internal';

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Unique identifier of the message", type="string", format="uuid")
   * @Groups({"read", "kafka"})
   */
  private $id;

  /**
   * @Assert\NotBlank(message="Message is mandatory")
   * @Assert\NotNull(message="Message is mandatory")
   * @Serializer\Type("string")
   * @OA\Property(description="Message text, accepts html tags", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private $message;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Message subject, this value will be used as the subject in the email sent to the recipient", type="string")
   * @Groups({"read", "write", "kafka"} )
   */
  private $subject;

  /**
   * @var User|null
   * @Serializer\Exclude()
   */
  private ?User $author;

  /**
   * @var Pratica
   * @Assert\NotBlank(message="Application is mandatory")
   * @Assert\NotNull(message="Application is mandatory")
   * @Serializer\Type("string")
   * @OA\Property(description="Application to which message is linked", type="string", format="uuid")
   * @Groups({"read"})
   */
  private Pratica $application;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Message visibility, set applicant for public messages, internal for private ones", type="string", enum={"applicant", "internal"})
   * @Groups({"read", "write"})
   */
  private $visibility;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Email address of the recipient to whom the email communication was sent", type="string", format="email")
   * @Groups({"read", "write"})
   */
  private $email;

  /**
   * @var User[]
   * @Serializer\Exclude()
   */
  private array $receivers;

  /**
   * @var DateTime
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Date and time the message was created", type="string", format="date-time")
   * @Groups({"read", "kafka"})
   */
  private $createdAt;

  /**
   * @var DateTime|null
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Date and time of sending the communication by e-mail", type="string", format="date-time")
   * @Groups({"read", "write", "kafka"})
   */
  private $sentAt;

  /**
   * @var DateTime|null
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Date and time the message was read by the recipient. This field is filled in when the email is opened or when the recipient views the application", type="string", format="date-time")
   * @Groups({"read", "write", "kafka"})
   */
  private $readAt;

  /**
   * @var DateTime|null
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Clicked at date time", type="string", format="date-time")
   * @Groups({"read", "write", "kafka"})
   */
  private $clickedAt;

  /**
   * @var bool
   * @Serializer\Type("bool")
   * @OA\Property(description="This value indicates the need to register the message, by default the registration is not required", type="boolean")
   * @Groups({"read", "write", "kafka"})
   */
  private $protocolRequired;

  /**
   * @var File[]
   * @OA\Property(description="List of attachments sent within the message", property="attachments", type="array", @OA\Items(ref=@Model(type=File::class, groups={"read", "write"})))
   * @Serializer\Type("array")
   * @Groups({"read", "write", "kafka"})
   */
  private $attachments;

  /**
   * @var DateTime|null
   * @Serializer\Type("DateTime")
   * @OA\Property(description="Date and time when the message was registered", type="string", format="date-time")
   * @Groups({"read", "write", "kafka"})
   */
  private $protocolledAt;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Protocol number of the message", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private $protocolNumber;

  /**
   * @var string|null
   * @Serializer\Type("string")
   * @OA\Property(description="External identifier of the message for external integrations", type="string")
   * @Groups({"read", "write", "kafka"})
   */
  private $externalId;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id): void
  {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getMessage()
  {
    return $this->message;
  }

  /**
   * @param mixed $message
   */
  public function setMessage($message): void
  {
    $this->message = $message;
  }

  /**
   * @return mixed
   */
  public function getSubject()
  {
    return $this->subject;
  }

  /**
   * @param mixed $subject
   */
  public function setSubject($subject): void
  {
    $this->subject = $subject;
  }

  /**
   * @return User|null
   */
  public function getAuthor(): ?User
  {
    return $this->author;
  }

  /**
   * @param mixed $author
   */
  public function setAuthor($author): void
  {
    $this->author = $author;
  }

  /**
   * @Serializer\VirtualProperty(name="author")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("author")
   * @OA\Property(description="Author of the message, if null the message was generated by the system, for example when the application status changed", type="string", format="uuid")
   * @Groups({"read"})
   */
  public function getAuthorId(): ?string
  {
    return $this->author ? $this->author->getId() : null;
  }

  /**
   * @Serializer\VirtualProperty(name="author_id")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("author_id")
   * @OA\Property(description="Author of the message, if null the message was generated by the system, for example when the application status changed", type="string", format="uuid")
   * @Groups({"kafka"})
   */
  public function getKafkaAuthorId(): ?string
  {
    return $this->getAuthorId();
  }

  /**
   * @return Pratica
   */
  public function getApplication(): Pratica
  {
    return $this->application;
  }

  /**
   * @param Pratica $application
   */
  public function setApplication(Pratica $application): void
  {
    $this->application = $application;
  }

  /**
   * @Serializer\VirtualProperty(name="application")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("application")
   * @OA\Property(description="Application in which the message is sent", type="string", format="uuid")
   * @Groups({"read"})
   */
  public function getApplicationId(): ?string
  {
    return $this->application->getId();
  }

  /**
   * @Serializer\VirtualProperty(name="related_entity_id")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("related_entity_id")
   * @OA\Property(description="Related entity identifier (application_id)", type="string", format="uuid")
   * @Groups({"kafka"})
   */
  public function getKafkaRelatedEntityId(): ?string
  {
    return $this->application;
  }

  /**
   * @Serializer\VirtualProperty(name="related_entity_type")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("related_entity_type")
   * @OA\Property(description="Related entity type (Right now messages only support related applications)", type="string", enum={"application"})
   * @Groups({"kafka"})
   */
  public function getKafkaRelatedEntityType(): ?string
  {
    return 'application';
  }

  /**
   * @Serializer\VirtualProperty(name="remote_collection")
   * @Serializer\Type("array")
   * @Serializer\SerializedName("remote_collection")
   * @OA\Property(description="Remote collection identifier and type", type="object")
   * @Groups({"kafka"})
   */
  public function getRemoteCollection(): ?array
  {
    return [
      "id" => $this->application->getServizio()->getId(),
      "type" => "service"
    ];
  }

  /**
   * @return mixed
   */
  public function getVisibility()
  {
    return $this->visibility;
  }

  /**
   * @param mixed $visibility
   */
  public function setVisibility($visibility): void
  {
    $this->visibility = $visibility;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail(string $email)
  {
    $this->email = $email;
  }


  /**
   * @return array
   */
  public function getReceivers(): array
  {
    return $this->receivers ?? [];
  }

  /**
   * @param array|null $receivers
   */
  public function setReceivers(?array $receivers = [])
  {
    $this->receivers = $receivers;
  }

  /**
   * @Serializer\VirtualProperty(name="to")
   * @Serializer\Type("array<string>")
   * @Serializer\SerializedName("to")
   * @OA\Property(description="List of recipients to whom the e-mail communication of the message has been sent", type="array", @OA\Items(type="string", format="email"))
   * @Groups({"kafka"})
   * @return array
   */
  public function getReceiversEmails(): array
  {
    $receiverEmails = [];

    foreach ($this->receivers as $receiver) {
      $receiverEmails[] = $receiver->getEmail();
    }
    return $receiverEmails;
  }

  /**
   * @Serializer\VirtualProperty(name="receivers_ids")
   * @Serializer\Type("array<string>")
   * @Serializer\SerializedName("receivers_ids")
   * @OA\Property(description="List of recipients ids to whom the message has been sent", type="array", @OA\Items(type="string", format="uuid"))
   * @Groups({"kafka"})
   * @return array
   */
  public function getReceiversIds(): array
  {
    $receiverIds = [];

    foreach ($this->receivers as $receiver) {
      $receiverIds[] = $receiver->getId();
    }
    return $receiverIds;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
    return $this->createdAt;
  }

  /**
   * @param mixed $createdAt
   */
  public function setCreatedAt($createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  /**
   * @return mixed
   */
  public function getSentAt()
  {
    return $this->sentAt;
  }

  /**
   * @param mixed $sentAt
   */
  public function setSentAt(?\DateTime $sentAt): void
  {
    $this->sentAt = $sentAt;
  }

  /**
   * @return mixed
   */
  public function getReadAt()
  {
    return $this->readAt;
  }

  /**
   * @param mixed $readAt
   */
  public function setReadAt(?DateTime $readAt): void
  {
    $this->readAt = $readAt;
  }

  /**
   * @return mixed
   */
  public function getClickedAt()
  {
    return $this->clickedAt;
  }

  /**
   * @param mixed $clickedAt
   */
  public function setClickedAt(?DateTime $clickedAt): void
  {
    $this->clickedAt = $clickedAt;
  }

  /**
   * @return File[]
   */
  public function getAttachments(): ?array
  {
    return $this->attachments;
  }

  /**
   * @param File[] $attachments
   */
  public function setAttachments(array $attachments): void
  {
    $this->attachments = $attachments;
  }

  /**
   * @return bool
   */
  public function isProtocolRequired(): bool
  {
    return $this->protocolRequired ?? false;
  }

  /**
   * @param bool $protocolRequired
   */
  public function setProtocolRequired(bool $protocolRequired): void
  {
    $this->protocolRequired = $protocolRequired;
  }

  /**
   * @return mixed
   */
  public function getProtocolledAt()
  {
    return $this->protocolledAt;
  }

  /**
   * @param mixed $protocolledAt
   */
  public function setProtocolledAt(?DateTime $protocolledAt): void
  {
    $this->protocolledAt = $protocolledAt;
  }

  /**
   * @return mixed
   */
  public function getProtocolNumber()
  {
    return $this->protocolNumber;
  }

  /**
   * @param mixed $protocolNumber
   */
  public function setProtocolNumber($protocolNumber): void
  {
    $this->protocolNumber = $protocolNumber;
  }

  /**
   * Get the value of externalId
   *
   * @return string|null
   */
  public function getExternalId()
  {
    return $this->externalId;
  }

  /**
   * Set the value of externalId
   *
   * @param string|null $externalId
   *
   * @return self
   */
  public function setExternalId($externalId)
  {
    $this->externalId = $externalId;

    return $this;
  }

  /**
   * @Serializer\VirtualProperty(name="transmission_type")
   * @Serializer\Type("string")
   * @Serializer\SerializedName("transmission_type")
   * @OA\Property(description="Transmission status of the message, inbound if the message is sent by the citizen to the tenant, outbound if the message is sent by an operator of the tenant to the citizen and internal if it is an internal message of the tenant and it is not visible to the citizen", type="string", enum={"inbound", "outbound", "internal"})
   * @Groups({"read", "kafka"})
   */
  public function getTransmissionType(): string
  {
    // Interno se il messaggio è privato
    if ($this->visibility === MessageEntity::VISIBILITY_INTERNAL) {
      return self::TRANSMISSION_TYPE_INTERNAL;
    }

    // In entrata se il messaggio è pubblico e indirizzato all'ente
    if ($this->getAuthor() instanceof CPSUser) {
      return self::TRANSMISSION_TYPE_INBOUND;
    }

    // In uscita se il messaggio è pubblico e indirizzato all'utente
    return self::TRANSMISSION_TYPE_OUTBOUND;
  }

  /**
   * @param MessageEntity $message
   * @param $applicationBaseUrl
   * @return Message
   */
  public static function fromEntity(MessageEntity $message, $applicationBaseUrl): Message
  {
    $dto = new self();
    $dto->id = $message->getId();
    $dto->message = $message->getMessage();
    $dto->subject = $message->getSubject();
    $dto->author = $message->getAuthor();
    $dto->application = $message->getApplication();
    $dto->visibility = $message->getVisibility();
    $dto->email = $message->getEmail();
    $dto->receivers = $message->getReceivers();

    $dto->createdAt = self::dateTimeFromTimestamp($message->getCreatedAt());
    $dto->sentAt = self::dateTimeFromTimestamp($message->getSentAt());
    $dto->readAt = self::dateTimeFromTimestamp($message->getReadAt());
    $dto->clickedAt = self::dateTimeFromTimestamp($message->getClickedAt());
    $dto->protocolledAt = self::dateTimeFromTimestamp($message->getProtocolledAt());

    $dto->protocolRequired = $message->isProtocolRequired();
    $dto->protocolNumber = $message->getProtocolNumber();

    $dto->attachments = self::prepareFileCollection(
      $message->getAttachments(),
      $applicationBaseUrl . '/messages/' . $message->getId()
    );

    $dto->externalId = $message->getExternalId();

    return $dto;
  }

  /**
   * @param MessageEntity|null $entity
   * @return MessageEntity
   */
  public function toEntity(MessageEntity $entity = null): ?MessageEntity
  {
    if (!$entity) {
      $entity = new MessageEntity();
    }

    $entity->setMessage($this->getMessage());
    $entity->setSubject($this->getSubject());
    if ($this->getAuthor() instanceof User) {
      $entity->setAuthor($this->getAuthor());
    }
    if ($this->getApplication() instanceof Pratica) {
      $entity->setApplication($this->getApplication());
    }
    $entity->setVisibility($this->getVisibility());
    $entity->setEmail($this->getEmail());
    if ($this->getCreatedAt() instanceof DateTime) {
      $entity->setCreatedAt($this->getCreatedAt()->getTimestamp());
    }
    if ($this->getSentAt() instanceof DateTime) {
      $entity->setSentAt($this->getSentAt()->getTimestamp());
    }
    if ($this->getReadAt() instanceof DateTime) {
      $entity->setReadAt($this->getReadAt()->getTimestamp());
    }
    if ($this->getClickedAt() instanceof DateTime) {
      $entity->setClickedAt($this->getClickedAt()->getTimestamp());
    }
    if ($this->getProtocolledAt() instanceof DateTime) {
      $entity->setProtocolledAt($this->getProtocolledAt()->getTimestamp());
    }
    $entity->setProtocolRequired($this->isProtocolRequired());
    $entity->setProtocolNumber($this->getProtocolNumber());

    // Update attachment protocol data
    if ($entity->getProtocolNumber()) {
      foreach ($entity->getAttachments() as $attachment) {
        /** @var AllegatoMessaggio $attachment */
        if (!$attachment->getNumeroProtocollo()) {
          $attachment->setNumeroProtocollo($entity->getProtocolNumber());
          $attachment->addNumeroDiProtocollo([
            "id" => null,
            "protocollo" => $entity->getProtocolNumber()
          ]);
        }
      }
    }

    $entity->setExternalId($this->getExternalId());

    return $entity;
  }

  /**
   * @param $collection
   * @param string $attachmentEndpointUrl
   * @return array
   */
  public static function prepareFileCollection( $collection, $attachmentEndpointUrl = ''): array
  {
    $files = [];
    if ( $collection == null) {
      return $files;
    }
    /** @var Allegato $c */
    foreach ($collection as $c) {
      $files[]= self::prepareFile($c, $attachmentEndpointUrl);
    }
    return $files;
  }

  /**
   * @param Allegato $file
   * @param string $attachmentEndpointUrl
   * @return mixed
   */
  public static function prepareFile(Allegato $file, $attachmentEndpointUrl = '')
  {
    /*

    $filename = $file->getOriginalFilename();
    $filenameParts = explode('.', $filename);
    $systemFilename = $file->getFilename();
    $systemFilenameParts = explode('.', $systemFilename);
    if (end($filenameParts) != end($systemFilenameParts)) {
      $filename .=  '.' . end($systemFilenameParts);
    }

    $description = $file->getDescription();
    if (empty($description) || $description === $filename) {
      $description =  Allegato::DEFAULT_DESCRIPTION . ' - ' . $filename;
    } else {
      $description .= ' - ' . $filename;
    }

    $temp['id'] = $file->getId();
    $temp['name'] = $systemFilename;
    $temp['url'] = $baseUrl . '/attachments/' . $file->getId() . '?version=' . $version;
    $temp['originalName'] = StringUtils::sanitizeFileName($filename);
    $temp['description'] = $description;
    $temp['created_at'] = $file->getCreatedAt();
    $temp['protocol_required'] = $file->isProtocolRequired();
    $temp['protocol_number'] = $file->getNumeroProtocollo() ?: ($file->getIdDocumentoProtocollo() ?: null);

    */

    $filename = $file->getOriginalFilename();
    $filenameParts = explode('.', $filename);
    $systemFilename = $file->getFilename();
    $systemFilenameParts = explode('.', $systemFilename);
    if (end($filenameParts) != end($systemFilenameParts)) {
      $filename .=  '.' . end($systemFilenameParts);
    }

    $description = $file->getDescription();
    if (empty($description) || $description === $filename) {
      $description =  Allegato::DEFAULT_DESCRIPTION . ' - ' . $filename;
    } else {
      $description .= ' - ' . $filename;
    }

    $temp = new File();
    $temp->setId($file->getId());
    $temp->setName($systemFilename);
    $temp->setUrl($attachmentEndpointUrl . '/attachments/' .  $file->getId());
    $temp->setOriginalName(StringUtils::sanitizeFileName($filename));
    $temp->setDescription($description);
    $temp->setCreatedAt($file->getCreatedAt());
    $temp->setProtocolRequired($file->isProtocolRequired());
    $temp->setProtocolNumber($file->getNumeroProtocollo() ?: ($file->getIdDocumentoProtocollo() ?: null));
    //$temp->getMimeType($file->getFile()->getMimeType());
    $temp->setExternalId($file->getExternalId());
    return $temp;
  }
  /**
   * @param $value
   * @return DateTime|null
   */
  public static function dateTimeFromTimestamp($value): ?DateTime
  {
    try {
      if ($value > 0) {
        $date = new DateTime();
        return $date->setTimestamp($value);
      }
    } catch (Exception $e) {
      return null;
    }
    return null;
  }

}
