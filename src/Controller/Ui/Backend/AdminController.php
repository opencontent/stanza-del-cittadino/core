<?php

namespace App\Controller\Ui\Backend;

use App\DataTable\Traits\FiltersTrait;
use App\Dto\ServiceDto;
use App\Entity\AuditLog;
use App\Entity\Categoria;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Entity\Webhook;
use App\Form\Admin\Servizio\CardDataType;
use App\Form\Admin\Servizio\CustomTemplateType;
use App\Form\Admin\Servizio\FeedbackMessagesDataType;
use App\Form\Admin\Servizio\FormIOBuilderRenderType;
use App\Form\Admin\Servizio\FormIOI18nType;
use App\Form\Admin\Servizio\FormIOTemplateType;
use App\Form\Admin\Servizio\GeneralDataType;
use App\Form\Admin\Servizio\IntegrationsDataType;
use App\Form\Admin\Servizio\IOIntegrationDataType;
use App\Form\Admin\Servizio\MultiplePaymentsDataType;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Form\Admin\Servizio\PdndType;
use App\Form\Admin\Servizio\ProtocolDataType;
use App\Form\Admin\Servizio\AdvancedSettingsType;
use App\Form\Admin\Servizio\ReceiptType;
use App\Form\Admin\Servizio\StampsDataType;
use App\FormIO\SchemaFactoryInterface;
use App\Model\FlowStep;
use App\Model\PublicFile;
use App\Model\Service;
use App\Model\ServiceSource;
use App\Services\FileService\ServiceAttachmentsFileService;
use App\Services\FormServerApiAdapterService;
use App\Services\InstanceService;
use App\Services\IOService;
use App\Services\Manager\PdndManager;
use App\Services\Manager\ServiceManager;
use App\Services\Satisfy\SatisfyService;
use App\Utils\FormUtils;
use App\Utils\StringUtils;
use DateTime;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Flagception\Manager\FeatureManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use League\Flysystem\FileNotFoundException;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\Admin\Ente\EnteType;
use App\Form\ServizioFormType;


/**
 * Class AdminController
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
  use FiltersTrait;

  private InstanceService $instanceService;

  private FormServerApiAdapterService $formServer;

  private TranslatorInterface $translator;

  private SchemaFactoryInterface $schemaFactory;
  private IOService $ioService;

  private DataTableFactory $dataTableFactory;

  private LoggerInterface $logger;

  private ServiceManager $serviceManager;

  private EntityManagerInterface $entityManager;

  private ServiceAttachmentsFileService $fileService;
  private string $defaultLocale;

  private $locales;


  /**
   * @param InstanceService $instanceService
   * @param TranslatorInterface $translator
   * @param DataTableFactory $dataTableFactory
   * @param EntityManagerInterface $entityManager
   * @param $locales
   */
  public function __construct(
    InstanceService               $instanceService,
    TranslatorInterface           $translator,
    DataTableFactory              $dataTableFactory,
    EntityManagerInterface        $entityManager,
                                  $locales
  )
  {
    $this->instanceService = $instanceService;
    $this->translator = $translator;
    $this->dataTableFactory = $dataTableFactory;
    $this->locales = explode('|', $locales);
    $this->entityManager = $entityManager;
  }


  /**
   * @Route("/", name="admin_index")
   * @return Response
   */
  public function indexAction(): Response
  {
    return $this->render('Admin/index.html.twig', [
      'user' => $this->getUser(),
    ]);
  }

  /**
   * @Route("/ente", name="admin_edit_ente")
   * @param Request $request
   * @return Response|RedirectResponse
   */
  public function editEnteAction(Request $request)
  {

    $servizi = $this->instanceService->getServices();
    $services = [];
    $services ['all'] = $this->translator->trans('tutti');
    foreach ($servizi as $s) {
      $services[$s->getId()] = $s->getName();
    }

    $ente = $this->instanceService->getCurrentInstance();

    $form = $this->createForm(EnteType::class, $ente);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $ente = $form->getData();

      $this->entityManager->persist($ente);
      $this->entityManager->flush();

      return $this->redirectToRoute('admin_edit_ente');
    }

    return $this->render('Admin/editEnte.html.twig', [
      'user' => $this->getUser(),
      'ente' => $ente,
      'statuses' => Webhook::TRIGGERS,
      'services' => $services,
      'form' => $form->createView(),
      'locales' => $this->locales
    ]);
  }

  /**
   * @Route("/ente/preview", name="admin_preview_theme")
   * @param Request $request
   * @return Response
   */
  public function previewThemeAction(Request $request): Response
  {
    $ente = $this->instanceService->getCurrentInstance();
    $theme = $request->query->get('theme', $ente->getThemeOptions()->getIdentifier());
    $lightTopHeader = $request->query->getBoolean('light-top-header', false);
    $lightCenterHeader = $request->query->getBoolean('light-center-header', false);
    $lightNavbarHeader = $request->query->getBoolean('light-navbar-header', false);
    return $this->render('Admin/tenant/_theme_preview.html.twig', [
      'options' => [
        'theme' => $theme,
        'light_top_header' => $lightTopHeader,
        'light_center_header' => $lightCenterHeader,
        'light_navbar_header' => $lightNavbarHeader,
      ]
    ]);
  }

  /**
   * @Route("/logs", name="admin_logs_index", methods={"GET", "POST"})
   */
  public function indexLogsAction(Request $request): Response
  {
    $table = $this->dataTableFactory->create()
      ->add('type', TextColumn::class, ['label' => $this->translator->trans('event')])
      ->add(
        'eventTime',
        DateTimeColumn::class,
        ['label' => $this->translator->trans('date'), 'format' => 'd-m-Y H:i', 'searchable' => false]
      )
      ->add('user', TextColumn::class, ['label' => $this->translator->trans('meetings.labels.user')])
      ->add('description', TextColumn::class, ['label' => $this->translator->trans('general.descrizione')])
      ->add('ip', TextColumn::class, ['label' => 'Ip'])
      ->createAdapter(ORMAdapter::class, [
        'entity' => AuditLog::class,
      ])
      ->handleRequest($request);

    if ($table->isCallback()) {
      return $table->getResponse();
    }

    return $this->render('Admin/indexLogs.html.twig', [
      'user' => $this->getUser(),
      'datatable' => $table,
    ]);
  }
}


