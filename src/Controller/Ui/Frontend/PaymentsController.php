<?php

namespace App\Controller\Ui\Frontend;

use App\Entity\CPSUser;
use App\Services\BreadcrumbsService;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * Class PaymentsController
 * @package App\Controller
 * @Route("/user")
 */
class PaymentsController extends AbstractController
{

  /**
   * @var EntityManagerInterface
   */
  private $entityManager;
  /**
   * @var TranslatorInterface
   */
  private $translator;
  /**
   * @var LoggerInterface
   */
  private $logger;
  /**
   * @var BreadcrumbsService
   */
  private $breadcrumbsService;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   * @param TranslatorInterface $translator
   * @param BreadcrumbsService $breadcrumbsService ,
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, TranslatorInterface $translator, BreadcrumbsService $breadcrumbsService)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->translator = $translator;
    $this->breadcrumbsService = $breadcrumbsService;
  }

  /**
   * @Route("/payments", name="payments_list_cpsuser")
   * @return Response
   */
  public function cpsUserListPaymentsAction(): Response
  {
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem(ucfirst($this->translator->trans('personal_area')), 'user_dashboard');
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem('general.payment_data', 'payments_list_cpsuser');

    /** @var CPSUser $user */
    $user = $this->getUser();


    return $this->render('Payments/cpsUserListPayments.html.twig', [
      'user' => $user
    ]);
  }

  /**
   * @Route("/payments/{paymentId}", name="payments_details_cpsuser")
   * @param Request $request
   * @param $paymentId
   * @return RedirectResponse|Response|null
   */
  public function cpsUserDetailsPaymentAction(Request $request, $paymentId)
  {
    /** @var CPSUser $user */
    $user = $this->getUser();

    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem(ucfirst($this->translator->trans('personal_area')), 'user_dashboard');
    $this->breadcrumbsService->getBreadcrumbs()->addRouteItem('general.payment_data', 'payments_list_cpsuser');
    $this->breadcrumbsService->getBreadcrumbs()->addItem($paymentId);

    return $this->render('Payments/cpsUserDetailsPayment.html.twig', [
      'user' => $user
    ]);
  }

}
