<?php

namespace App\Form;

use App\BackOffice\BackOfficeInterface;
use App\Dto\Tenant;
use App\Entity\Servizio;
use App\Model\Gateway;
use App\Payment\GatewayCollection;
use App\Services\BackOfficeCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class TenantType extends AbstractType
{

  /**
   * @var BackOfficeCollection
   */
  private BackOfficeCollection $backOfficeCollection;
  /**
   * @var GatewayCollection
   */
  private GatewayCollection $gatewayCollection;
  /**
   * @var TranslatorInterface
   */
  private TranslatorInterface $translator;
  /**
   * @var EntityManagerInterface
   */
  private EntityManagerInterface $entityManager;

  public function __construct(BackOfficeCollection $backOffices, GatewayCollection $gatewayCollection, TranslatorInterface $translator, EntityManagerInterface $entityManager)
  {
    $this->backOfficeCollection = $backOffices;
    $this->gatewayCollection = $gatewayCollection;
    $this->translator = $translator;
    $this->entityManager = $entityManager;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $backOfficesData = [];
    foreach ($this->backOfficeCollection->getBackOffices() as $b) {
      $backOfficesData[$b->getName()] = $b->getPath();
    }

    $builder
      ->add('name', TextType::class, ['label' => false])
      ->add('ipa_code', TextType::class, ['label' => false, 'required' => false])
      ->add('aoo_code', TextType::class, ['label' => false, 'required' => false])
      ->add('cadastral_code', TextType::class, ['label' => false, 'required' => false])
      ->add('mechanographic_code', TextType::class, ['label' => false, 'required' => false])
      ->add('administrative_code', TextType::class, ['label' => false, 'required' => false])
      ->add('site_url', TextType::class, ['label' => false])
      ->add('meta', TextareaType::class, ['empty_data' => ""])
      ->add('io_enabled', CheckboxType::class)
      ->add('satisfy_entrypoint_id', TextType::class, ['label' => false, 'required' => false])
      ->add('gateways', CollectionType::class, [
        'entry_type' => GatewayType::class,
        'allow_add' => true,
        'allow_delete' => true
      ])
      ->add('default_pdnd_client_id')
      ->add('pdnd_config_ids', CollectionType::class, [
        'entry_type' => TextType::class,
        'allow_add' => true,
        'allow_delete' => true
      ])
      ->add('closing_periods', CollectionType::class, [
        'required' => false,
        'label' => false,
        'entry_type' => DateTimeIntervalType::class,
        'allow_add' => true,
        'allow_delete' => true
      ]);

    if ( !empty($backOfficesData)) {
      $builder->add('backoffice_enabled_integrations', ChoiceType::class, [
        'choices' => $backOfficesData,
        'multiple' => true,
        'required' => false,
      ])
        ->add('linkable_application_meetings', CheckboxType::class);
    }

    $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
  }

  public function onPreSubmit(FormEvent $event)
  {
    /** @var Tenant $ente */
    $ente = $event->getForm()->getData();
    $data = $event->getData();

    $availableGateways = array_keys($this->gatewayCollection->getAvailablePaymentGateways());

    $gateways = [];
    if (isset($data['gateways'])) {
      foreach ($data['gateways'] as $g) {
        // Per ogni gateway che si vuole salvare verifico che l'identifier compaia tra quelli disponibili
        if (!in_array($g['identifier'], $availableGateways)) {
          $event->getForm()->addError(new FormError($this->translator->trans('payment.error_invalid_gateway', [
            '%selected_gateway%' => $g['identifier'],
            '%enabled_tenant_gateways%' => implode(', ', $availableGateways)
          ])));
          continue;
        }
        $gateway = new Gateway();

        $gateway->setIdentifier($g["identifier"]);
        $gateway->setParameters($g["parameters"]);
        $gateways[$g["identifier"]] = $gateway;
      }

      $servicesCountByIdentifier = $this->entityManager->getRepository(Servizio::class)->countPaymentServicesByIdentifier();

      foreach ($ente->getGateways() as $gateway) {
        $identifier = $gateway->getIdentifier();
        // Se il gateway è stato disabilitato ma ci sono servizi collegati impedisco la modifica dell'ente
        if (!array_key_exists($identifier, $gateways) && isset($servicesCountByIdentifier[$identifier])) {
          $event->getForm()->addError(new FormError($this->translator->trans(
            'servizio.error_gateway_linked_services',
            ['%identifier%' => $gateway->getIdentifier()]
          )));
        }
      }
      $ente->setGateways($gateways);
    }
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Dto\Tenant',
      'csrf_protection' => false,
    ));
  }

  public function getBlockPrefix()
  {
    return 'app_bundle_tenant_type';
  }
}
