<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application;

use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\Applications\Application\DTOs\ActionData;
use App\Applications\Application\DTOs\ApplicantData;
use App\Applications\Application\DTOs\ApplicationLiteData;
use App\Applications\Application\DTOs\BeneficiaryData;
use App\Applications\Application\DTOs\ContactsData;
use App\Applications\Application\DTOs\IdentifiersData;
use App\Applications\Application\DTOs\ServiceData;
use App\Entity\ApplicationLite;
use App\Entity\CPSUser;
use App\Entity\Servizio;

class DataTransformer
{
  public static function toApplicationLiteImportApiModel(ApplicationLite $applicationLite): ImportData
  {
    $importModel = new ImportData();
    $importModel->application = self::toApplicationLiteApiModel($applicationLite);

    return $importModel;
  }

  public static function toApplicationLiteApiModel(ApplicationLite $applicationLite): ApplicationLiteData
  {
    $model = new ApplicationLiteData();
    $model->id = "" . $applicationLite->getId();
    $model->externalId = $applicationLite->externalId();
    $model->subject = $applicationLite->subject();
    $model->status = $applicationLite->status();
    $model->applicant = self::toApplicantModel($applicationLite->applicant());
    $model->beneficiary = self::toBeneficiaryModel($applicationLite->beneficiary());
    $model->service = self::toServiceModel($applicationLite->service());

    $model->actions = [];
    foreach ($applicationLite->externalActions() as $externalAction) {
      $actionData = new ActionData();
      $actionData->action = $externalAction->action();
      $actionData->url = $externalAction->url();
      $model->actions[] = $actionData;
    }

    return $model;
  }

  private static function toApplicantModel(CPSUser $person): ApplicantData
  {
    $applicant = new ApplicantData();
    $applicant->givenName = $person->getNome();
    $applicant->familyName = $person->getCognome();
    $applicant->identifiers = self::toAdmIdsDataModel($person);
    $applicant->contacts = self::toContactsModel($person);

    return $applicant;
  }

  private static function toBeneficiaryModel(?CPSUser $person): ?BeneficiaryData
  {
    if (null === $person) {
      return null;
    }

    $beneficiary = new BeneficiaryData();
    $beneficiary->givenName = $person->getNome();
    $beneficiary->familyName = $person->getCognome();
    $beneficiary->identifiers = self::toAdmIdsDataModel($person);
    $beneficiary->contacts = self::toContactsModel($person);

    return $beneficiary;
  }

  private static function toAdmIdsDataModel(CPSUser $person): IdentifiersData
  {
    $admIds = new IdentifiersData();
    $admIds->taxCode = $person->getCodiceFiscale();

    return $admIds;
  }

  private static function toContactsModel(CPSUser $person): ContactsData
  {
    $contacts = new ContactsData();
    $contacts->email = $person->getEmail();
    $contacts->pec = $person->getPec();
    $contacts->mobile = $person->getCpsCellulare();
    $contacts->phone = $person->getCpsTelefono();

    return $contacts;
  }

  private static function toServiceModel(Servizio $service): ServiceData
  {
    $serviceModel = new ServiceData();
    $serviceModel->id = "" . $service->getId();
    $serviceModel->name = $service->getName();

    return $serviceModel;
  }
}
