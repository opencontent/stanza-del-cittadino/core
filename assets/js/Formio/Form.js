import Calendar from "../Calendar";
import DynamicCalendar from "../DynamicCalendar";
import PageBreak from "../PageBreak";
import FinancialReport from "../FinancialReport";
import SdcFile from "../SdcFile";
import AddressMap from "../AddressMap";
import "formiojs";
import "formiojs/dist/formio.form.min.css";
import axios from "axios";
import FormioI18n from "../utils/FormioI18n";
import wizardNav from "./templates/wizardNav/index.js";
import wizardHeader from "./templates/wizardHeader/index.js";
import fieldset from "./templates/fieldset/index.js";
import checkbox from "./templates/checkbox/index.js";
import componentModal from "./templates/componentModal";
import modalPreview from "./templates/modalPreview";
import {Formio, Utils} from "formiojs";
import Applications from "../rest/applications/Applications";

import Swal from 'sweetalert2/src/sweetalert2.js';
import Users from '../rest/users/Users';
import FormIoHelper from '../utils/FormIoHelper';
import inefficienciesI18n from '../../app/schemas/inefficiencies/i18n.json';
import helpdeskI18n from '../../app/schemas/helpdesk/i18n.json';
import bookingsI18n from '../../app/schemas/bookings/i18n.json';
import moment from 'moment';
import Pdnd from "./Pdnd";


Formio.registerComponent("calendar", Calendar);
Formio.registerComponent("dynamic_calendar", DynamicCalendar);
Formio.registerComponent("pagebreak", PageBreak);
Formio.registerComponent("financial_report", FinancialReport);
Formio.registerComponent("sdcfile", SdcFile);
Formio.registerComponent("address_map", AddressMap);

const language = document.documentElement.lang.toString();
const applications = new Applications();
const users = new Users();
const feedbackContainer = $('#feedback');
const applicationOwner = $('#application-owner');

moment.locale(language);


// Overwrite nav buttons formio
Formio.Templates.current = {
  wizardNav: {
    form: (ctx) => wizardNav(ctx)
  },
  wizardHeader: {
    form: (ctx) => wizardHeader(ctx)
  },
  fieldset: {
    form: (ctx) => fieldset(ctx)
  },
  checkbox: {
    form: (ctx) => checkbox(ctx)
  },
  componentModal: {
    form: (ctx) => componentModal(ctx)
  },
  modalPreview: {
    form: (ctx) => modalPreview(ctx)
  },
};


class Form {
  submissionForm = null;

  static onClickSubmit(customErrorContainer, submission, form, dataContainer, realSubmitButton, $container) {
    let submitButton = $('#formio button');
    submitButton.hide();
    $(
      `<p id="loading-button" class="w-100" role="alert">
       <i class="fa fa-refresh fa-spin"></i> ${Translator.trans('waiting', {}, 'messages', language)}...
       </p>`
    ).insertAfter(submitButton.last());
    customErrorContainer.empty().hide();
    axios
      .post($container.data('form_validate'), JSON.stringify(submission.data))
      .then(function (response) {
        customErrorContainer.empty();
        if (response.data.errors) {
          response.data.errors.forEach((error) => {
            customErrorContainer.append(
              '<p class="m-0">' + error.toString() + '</p>'
            );
          });
          customErrorContainer.show();
          $('#formio #loading-button').remove();
          submitButton.show();
        } else {
          form.emit('submitDone', submission);
          dataContainer.val(JSON.stringify(submission.data));
          realSubmitButton.trigger('click');
        }
      });
  }

  static createStepsMobile() {

    $('.info-progress-wrapper[data-loop!=\'first\']').each(function (idx) {
      $(this).attr('data-progress', idx + 1);
    });

    // Hide craue element if formio steps are > 4
    if ($('[data-wizard*=\'header\']').length > 6) {
      $('[data-item=\'craue\']').each(function () {
        $(this).removeClass('d-lg-flex');
      });
    }

    const step =
      ($('.step-active').data('progress')
        ? $('.step-active').data('progress')
        : '1') +
      '/' +
      ($('.info-progress-wrapper').length - 1);
    const stepLabel = $('.step-active span').attr('title');

    $('.step').html(step);
    $('.step-label').html(stepLabel);
  }

  static initEditable(containerId) {
    const $container = $('#' + containerId);
    const baseUrl = $container.data('formserver_url');
    const isBuiltIn = $container.hasClass('built-in');
    let locale = $container.data('locale') || 'it';
    const serviceIdentifier = $container.data('service-identifier');
    const standardI18n = FormioI18n.languages();
    const pdndConfigs = $container.data('pdnd-configs') || null;
    const applicationId = $container.data('application-id');
    const isAnonymousUser = $container.data('anonymous-user');

    let formUrl = baseUrl + '/form/' + $container.data('form_id');
    let i18nUrl = i18nUrl = formUrl + '/i18n?lang=' + locale;
    if (isBuiltIn) {
      i18nUrl = '/bundles/app/schemas/' + serviceIdentifier + '/i18n.json';
      formUrl = '/bundles/app/schemas/' + serviceIdentifier + '.json';
    }

    $.getJSON(
      i18nUrl,
      function (data) {

        let translations = {};
        if (isBuiltIn) {
          translations[locale] = {...standardI18n[locale], ...data[locale]};
        } else {
          translations[locale] = data[locale];
        }

        let customErrorContainer = $('#formio-custom-errors');
        Form.getFormSchema(formUrl, false).then((formSchema) => {

          Formio.setBaseUrl(baseUrl);
          Formio.icons = 'fontawesome';
          Formio.createForm(document.getElementById(containerId), formSchema, {
            noAlerts: true,
            language: locale,
            i18n: translations,
            buttonSettings: {
              showCancel: false
            },
            breadcrumbSettings: {
              clickable: true
            },
            breadCrumb: {clickable: true},
            hooks: {
              beforeCancel: () => Form.handleBeforeSubmit(event)
            },
            sanitizeConfig: {
              allowedAttrs: ['ref', 'src', 'url', 'data-oembed-url'],
              allowedTags: ['oembed', 'svg', 'use'],
              addTags: ['oembed', 'svg', 'use'],
              addAttr: ['url', 'data-oembed-url']
            }
          }).then(function (form) {

            form.setPage(Form.getPage(applicationId));

            let dataContainer = $('#formio_render_dematerialized_forms');
            // Recupero i dati della pratica se presenti
            if (dataContainer.val()) {
              form.submission = {
                data: JSON.parse(dataContainer.val()).data
              };
            }

            form.formReady.then(() => {
              if (!isAnonymousUser) {
                setTimeout(disableApplicant, 1000);
              }
              Form.createStepsMobile();
              Form.submissionForm = form;
              Form.initDraftButton();
              Form.createCustomNavItem(form.component, false, data);
              Form.enableRestrictedOperatorFields(form);
              Form.openPrivacyLink();

              Pdnd.initEditablePdnd(form, pdndConfigs);
            });

            form.on('change', () => {
              Form.createCustomNavItem(form.component, false, data);
              Pdnd.initEditablePdnd(form, pdndConfigs);
            });


            if (form.hasOwnProperty('wizard')) {
              $('.craue_formflow_current_step.active').addClass('wizard');
            }

            form.on('nextPage', function (e) {
              if (!isAnonymousUser) {
                setTimeout(disableApplicant, 1000);
              }
              Pdnd.initEditablePdnd(form, pdndConfigs)
              Form.customBreadcrumbButton(form, data);
              setTimeout(Form.checkWizardNavCancelButton, 500);
              document.getElementById('formio').scrollIntoView();
              Form.createStepsMobile();
              Form.saveDraft();
              Form.initDraftButton();
              Form.createCustomNavItem(form.component, false, data);
              Form.openPrivacyLink();
              Form.setPage(applicationId, form.page);
            });

            form.on('prevPage', () => {
              if (!isAnonymousUser) {
                setTimeout(disableApplicant, 1000);
              }
              Pdnd.initEditablePdnd(form, pdndConfigs)

              Form.customBreadcrumbButton(form, data);
              setTimeout(Form.checkWizardNavCancelButton, 500);
              Form.createStepsMobile();
              Form.initDraftButton();
              Form.createCustomNavItem(form.component, false, data);
              Form.openPrivacyLink();
              Form.setPage(applicationId, form.page);
            });

            form.on('pagesChanged', () => {
              if (!isAnonymousUser) {
                setTimeout(disableApplicant, 1000);
              }
              Form.customBreadcrumbButton(form, data);
              Form.openPrivacyLink();
              Form.setPage(applicationId, form.page);
            });

            const realSubmitButton = $('.craue_formflow_button_class_next');
            form.nosubmit = true;

            // Triggered when they click the submit button.
            form.on('submit', function () {
              Form.onClickSubmit(customErrorContainer, form.submission, form, dataContainer, realSubmitButton, $container);
            });
          });
          Form.autoCloseAlert(customErrorContainer);

          //Funzione per rendere il form Applicant readOnly
          const disableApplicant = function () {
            $('.formio-component-applicant input').each(function (k) {
              if ($(this).closest('.formio-component-address').length <= 0) {
                if ($(this).prop('type') === 'radio') {
                  let name = $(this).prop('name');
                  if ($(this).prop('checked')) {
                    $('input[name=\'' + name + '\']').attr('disabled', 'disabled');
                  }
                } else if ($(this).val() && $(this).prop('type') !== 'email' && $(this).prop('name') !== 'data[cell_number]' && $(this).prop('name') !== 'data[phone_number]') {
                  $(this).attr('disabled', 'disabled');
                }
              }
            });
          };
        });
      });
  }

  static autoCloseAlert(customErrorContainer) {
    if (customErrorContainer && customErrorContainer.length > 0) {
      customErrorContainer.each(function () {
        var time_period = customErrorContainer.attr('auto-close');
        setTimeout(function () {
          customErrorContainer.empty().hide();
        }, time_period);
      });
    }
  }

  static initPrintable(containerId) {
    const $container = $('#' + containerId);
    const formUrl = $container.data('formserver_url') + '/printable/' + $container.data('form_id');
    $.getJSON(
      $container.data('formserver_url') + '/form/' + $container.data('form_id') + '/i18n?lang=' + $container.data('locale'),
      function (data) {
        Formio.icons = 'fontawesome';
        Formio.createForm(document.getElementById(containerId), formUrl, {
          noAlerts: true,
          language: $container.data('locale'),
          i18n: data,
          readOnly: true,
          buttonSettings: {showCancel: false},
          hooks: {
            beforeCancel: () => Form.handleBeforeSubmit(event)
          }
          //renderMode: 'html'
        }).then(function (form) {
          form.submission = {
            data: $container.data('submission')
          };

          // Called when the form has completed the render, attach, and one initialization change event loop
          // Da migliorare il controllo sulla fine del rendering del form
          form.on('initialized', () => {
            setTimeout(function () {
              console.log('initialized');
              window.status = 'ready';
            }, 5000);
          });

        });
      }
    );
  }

  static initPrintableBuiltIn(containerId) {
    const $container = $('#' + containerId);
    const formUrl = $container.data('url');
    const baseUrl = $container.data('formserver_url');

    Form.getFormSchema(formUrl, true).then((formSchema) => {
      Formio.setBaseUrl(baseUrl);
      Formio.icons = 'fontawesome';

      Formio.createForm(document.getElementById(containerId), formSchema, {
        noAlerts: true,
        language: $container.data('locale'),
        i18n: FormioI18n.languages(),
        readOnly: true,
        buttonSettings: {showCancel: false},
        hooks: {
          beforeCancel: () => Form.handleBeforeSubmit(event)
        }
        //renderMode: 'html'
      }).then(function (form) {
        form.submission = {
          data: $container.data('submission')
        };

        form.ready.then(() => {
          setTimeout(function () {
            console.log('initialized');
            window.status = 'ready';
          }, 5000);
        });
      });
    });
  }

  static initPreview(containerId) {
    const $container = $('#' + containerId);
    const formUrl =
      $container.data('formserver_url') + '/form/' + $container.data('form_id');
    $.getJSON(
      formUrl + '/i18n?lang=' + $container.data('locale'),
      function (data) {
        Formio.icons = 'fontawesome';
        Formio.createForm(document.getElementById(containerId), formUrl, {
          noAlerts: true,
          language: $container.data('locale'),
          i18n: data,
          readOnly: false,
          buttonSettings: {showCancel: false},
          hooks: {
            beforeCancel: () => Form.handleBeforeSubmit(event)
          }
        }).then(function (form) {
          form.formReady.then(() => {
            Form.createStepsMobile();
            Form.customBreadcrumbButton(form, data);
            setTimeout(Form.checkWizardNavCancelButton, 500);
            Form.createStepsMobile();
            Form.createCustomNavItem(form.component, false, data);
            Form.openPrivacyLink();
          });

          form.on('change', () => {
            Form.createCustomNavItem(form.component, false, data);
          });

          form.on('nextPage', function () {
            Form.customBreadcrumbButton(form, data);
            setTimeout(Form.checkWizardNavCancelButton, 500);
            Form.createStepsMobile();
            Form.createCustomNavItem(form.component, false, data);
            Form.openPrivacyLink();
          });

          form.on('prevPage', function () {
            Form.customBreadcrumbButton(form, data);
            setTimeout(Form.checkWizardNavCancelButton, 500);
            Form.createStepsMobile();
            Form.createCustomNavItem(form.component, false, data);
            Form.openPrivacyLink();
          });
        });
      }
    );
  }

  static initSummary(containerId) {

    const $container = $('#' + containerId);
    const formUrl = $container.data('formserver_url') + '/form/' + $container.data('form_id');
    const printableFormUrl = $container.data('formserver_url') + '/printable/' + $container.data('form_id');

    $.getJSON(
      formUrl + '/i18n?lang=' + $container.data('locale'),
      function (data) {
        Formio.icons = 'fontawesome';
        Formio.createForm(
          document.getElementById(containerId),
          printableFormUrl,
          {
            readOnly: true,
            noAlerts: true,
            language: $container.data('locale'),
            i18n: data,
            sanitizeConfig: {
              allowedAttrs: ['ref', 'src', 'url', 'data-oembed-url', 'svg'],
              allowedTags: ['oembed', 'svg'],
              addTags: ['oembed', 'svg'],
              addAttr: ['url', 'data-oembed-url']
            }
          }
        ).then(function (form) {
          form.submission = {
            data: $container.data('submission')
          };

          form.on("change", () => {
            Form.disableAttachmentLinks();
          });

          form.ready.then(() => {
            Form.getStoredSteps();
            Form.createStepsMobile();
            Form.createCustomNavItem(form.component, true, data);
            Form.openPrivacyLink();
            Pdnd.initSummaryPdnd(form);
          });

          Form.onFormRendered(form, () => {
            Form.showConfirmButton();
          })

        });
      }
    );
  }

  static initSummaryBuiltIn(containerId) {
    const $container = $('#' + containerId);
    const formUrl = $container.data('url');
    const baseUrl = $container.data('formserver_url');

    Form.getFormSchema(formUrl, true).then((formSchema) => {
      Formio.setBaseUrl(baseUrl);

      Formio.icons = 'fontawesome';
      Formio.createForm(
        document.getElementById(containerId),
        formSchema,
        {
          readOnly: true,
          noAlerts: true,
          language: $container.data('locale'),
          i18n: FormioI18n.languages(),
          sanitizeConfig: {
            allowedAttrs: ['ref', 'src', 'url', 'data-oembed-url', 'svg'],
            allowedTags: ['oembed', 'svg'],
            addTags: ['oembed', 'svg'],
            addAttr: ['url', 'data-oembed-url']
          }
        }
      ).then(function (form) {
        form.submission = {
          data: $container.data('submission')
        };

        form.on("change", () => {
          Form.disableAttachmentLinks();
        });

        form.ready.then(() => {
          Form.getStoredSteps();
          Form.createStepsMobile();
          Form.createCustomNavItem(form.component, true, FormioI18n.languages());
          Form.openPrivacyLink();
        });

        Form.onFormRendered(form, () => {
          Form.showConfirmButton();
        })
      });
    });
  }

  static initBuiltInForm() {
    const builtinContainer = document.getElementById('formio-builtin');
    const formUrl = builtinContainer.getAttribute('data-url');
    const baseUrl = builtinContainer.getAttribute('data-formserver_url');
    const dataSubmission = builtinContainer.getAttribute('data-submission');
    const dataIdentifier = builtinContainer.getAttribute('data-identifier');
    const standardI18n = FormioI18n.languages();
    const locale = builtinContainer.getAttribute('data-locale') || 'it';
    let translations = {};

    if (dataIdentifier === 'inefficiencies')
      translations[locale] = {...standardI18n[locale], ...inefficienciesI18n[locale]};
    else if (dataIdentifier === 'helpdesk')
      translations[locale] = {...standardI18n[locale], ...helpdeskI18n[locale]};
    else if (dataIdentifier === 'bookings')
      translations[locale] = {...standardI18n[locale], ...bookingsI18n[locale]};

    Form.getFormSchema(formUrl, false).then((formSchema) => {
      Formio.setBaseUrl(baseUrl);
      Formio.icons = 'fontawesome';
      Formio.createForm(
        builtinContainer,
        formSchema,
        {
          readOnly: false,
          noAlerts: true,
          language: locale,
          i18n: translations,
          sanitizeConfig: {
            allowedAttrs: ['ref', 'src', 'url', 'data-oembed-url', 'svg'],
            allowedTags: ['oembed', 'svg'],
            addTags: ['oembed', 'svg'],
            addAttr: ['url', 'data-oembed-url']
          }
        }
      ).then(function (form) {

        form.ready.then(() => {
          Form.createStepsMobile();
          Form.enableRestrictedOperatorFields(form);
          Form.openPrivacyLink();
          form.nosubmit = true;
          if (dataSubmission.length) {
            form.submission = {
              data: JSON.parse(dataSubmission).data
            };
          }
        });

        form.on('prevPage', function () {
          Form.openPrivacyLink();
        });

        form.on('nextPage', function () {
          Form.openPrivacyLink();
        });
        document.querySelector('.btn-wizard-nav-previous').addEventListener('click', function (e) {
          e.preventDefault();
          location.reload();
        });

        // Triggered when they click the submit button.
        form.on('submit', function (submission) {
          let submitButton = $(builtinContainer).find('.btn-wizard-nav-submit');
          submitButton.html(`<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ${Translator.trans('salva', {}, 'messages', language)} ...`);

          let application = {};
          application.user = builtinContainer.getAttribute('data-user');
          application.service = builtinContainer.getAttribute('data-service');
          application.data = submission.data;
          application.status = 1900;
          applications.postApplication(JSON.stringify(application))
            .fail(function (xhr, type, exception) {
              submitButton.html(`${Translator.trans('salva', {}, 'messages', language)}`);
              Swal.fire(exception, '', 'error');
            })
            .done(function (data, code, xhr) {

              builtinContainer.classList.add('d-none');
              applicationOwner.addClass('d-none');
              feedbackContainer.removeClass('d-none');
            });
        });
      });
    });
  }

  static onFormRendered(formioInstance, callback) {
    if (!formioInstance) {
      console.error('Invalid Form.io instance');
      return;
    }

    formioInstance?.ready
      .then(() => {
        console.log('Main form rendered!');

        const nestedForms = formioInstance.getComponents().filter(comp => comp.type === 'form');
        if (nestedForms.length === 0) {
          console.log('No nested forms found.');
          callback();
          return;
        }

        console.log(`Waiting for ${nestedForms.length} nested form(s)...`);
        return Promise.all(nestedForms.map(nested => nested.subForm.ready));
      })
      .then(() => {
        console.log('All nested forms rendered!');
        if (typeof callback === 'function') {
          callback();
        }
      })
      .catch(err => {
        console.error('Error while rendering the form:', err);
      });
  }

  static showConfirmButton() {
    $('.formio-wizard-nav-container').removeClass('d-none');
  }

  static init(containerId) {
    // Init form editable
    if ($('#' + containerId + '.editable').length > 0) {
      this.initEditable(containerId);
    }

    // Init form printable
    if ($('#' + containerId + '.printable').length > 0) {
      this.initPrintable(containerId);
    }

    // Init form preview
    if ($('#' + containerId + '.preview').length > 0) {
      this.initPreview(containerId);
    }

    // Init form summary
    if ($('#' + containerId + '.formio-summary').length > 0) {
      this.initSummary(containerId);
    }

    // Init built-in printable
    if ($('#' + containerId + '.built-in-printable').length > 0) {
      this.initPrintableBuiltIn(containerId);
    }

    // Init built-in summary
    if ($('#' + containerId + '.formio-built-in-summary').length > 0) {
      this.initSummaryBuiltIn(containerId);
    }
  }

  static customBreadcrumbButton(form, translations) {
    // Handle click Breadcrumb Buttons
    $('.info-progress-body.completed').on('click', function (event) {
      event.preventDefault();
      event.stopImmediatePropagation();
      const indexPage = $(this).data('index');
      if (indexPage >= 0) {
        form.setPage(indexPage);
        form.triggerChange();
        Form.customBreadcrumbButton(form, translations);
        Form.createCustomNavItem(form.component, false, translations);
      }
    });
  }

//Funzione per aggiungere l'attributo type=button al pulsante "Annulla" se è visibile
  static checkWizardNavCancelButton() {
    if ($('.btn-wizard-nav-cancel').length > 0) {
      $('.btn-wizard-nav-cancel').attr('type', 'button');
    }
  }

// Refresh page on handle "cancel button"
  static handleBeforeSubmit() {
    if (
      confirm(
        `${Translator.trans(
          'pratica.you_want_cancel',
          {},
          'messages',
          language
        )}`
      )
    ) {
      document.location.reload();
    }
  }

  static saveDraft() {
    const draftButton = $('#save-draft');
    const draftInfo = $('.save-draft-info');
    const draftTextInfo = draftInfo.find('span');
    let text = draftButton.html();
    draftButton.html(
      `<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>${Translator.trans(
        'save_processing',
        {},
        'messages',
        language
      )}`
    );
    axios
      .post($('#formio').data('save-draft-url'), Form.submissionForm.data)
      .then(function (response) {
        draftTextInfo.html(
          `<i class="fa fa-clock-o" aria-hidden="true"></i> ${Translator.trans(
            'buttons.last_save',
            {},
            'messages',
            language
          )} ${Translator.trans(
            'time.few_seconds_ago',
            {},
            'messages',
            language
          )}`
        );
      })
      .catch(function (error) {
        Swal.fire(
          `${Translator.trans('error_message_detail', {}, 'messages', language)}`,
          error.response && error.response && error.response.data.status ? error.response.data.status : `${Translator.trans('servizio.error_from_save', {}, 'messages', language)}`,
          'error'
        );
        draftTextInfo.text(
          `${Translator.trans(
            'servizio.error_from_save',
            {},
            'messages',
            language
          )}`
        );
      })
      .finally(function () {
        draftButton.html(text);
      });
  }

  static initDraftButton() {
    $('#save-draft').on('click', function (e) {
      e.preventDefault();
      Form.saveDraft();
    });
  }

  static getStoredSteps() {
    let parent = $('#wizardHeader');
    const steps = JSON.parse(localStorage.getItem('steps')) || null;
    if (parent && steps) {
      parent.prepend(
        steps.map(function (x) {
          return x.replace(/step-active/g, '');
        })
      );
    }
  }

  static createCustomNavItem(data, isSummary, translations) {
    // Filter only fieldset components
    let navItem = [];
    let tmpNavItem = [];
    if (isSummary) {
      //Search there are fieldset fields
      const filterItem = data.components.filter(item => item.type === 'fieldset');
      data.components.forEach(item => {
        // If fieldset fields
        if (item.components.filter(el => el.type === 'fieldset').length) {
          const filtered = item.components.filter(el => el.type === 'fieldset');
          tmpNavItem = [...tmpNavItem, ...filtered];
        } else {
          // If not fieldset fields
          tmpNavItem = [...tmpNavItem, ...item.components];
        }
      });

      tmpNavItem.forEach(item => {
        if (!($(`#${item.id}`).hasClass('formio-hidden'))) {
          navItem.push(item);
        }
      });

      if (filterItem.length > 0) {
        Form.renderNavFieldsetItems(navItem, translations);
      } else {
        $('.cmp-navscroll').hide();
      }


    } else {
      const filterItem = data.components.filter(item => item.type === 'fieldset');
      if (filterItem.length > 0) {
        filterItem.forEach(item => {
          if (!($(`#${item.id}`).hasClass('formio-hidden'))) {
            navItem.push(item);
          }
        });
        Form.renderNavFieldsetItems(navItem, translations);
      } else if (filterItem.length === 0) {
        $('.cmp-navscroll').hide();
      }
    }

  }

  static renderNavFieldsetItems(navItem, translations) {
    // Reset list items
    $('#navItems').empty();
    navItem.forEach((el, idx) => {
      // Write item
      $('#navItems').append(`<li class="nav-item"><a class="nav-link ${idx === 0 ? 'active' : ''}" href="#${el.key}">
        <span class="title-medium">${translations[language] && translations[language][el.legend] ? translations[language][el.legend] : el.legend}
        </span></a></li>`);
    });
  }

  static renderCustomNavItems(navItem, translations) {

    // Reset list items
    $('#navItems').empty();
    navItem.forEach((el, idx) => {
      // Write item
      $('#navItems').append(`<li class="nav-item"><a class="nav-link ${idx === 0 ? 'active' : ''}" href="#${el.key}">
        <span class="title-medium">${translations[language] && translations[language][el.label] ? translations[language][el.label] : el.label}
        </span></a></li>`);
    });
  }

  static getFormSchema(formUrl, printable = false) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: formUrl,
        dataType: 'json',
        type: 'GET',
        success: function (form) {
          if (printable) {
            form = Form.wizardConverter(form);
          }
          resolve(form);
        },
        error: function (error) {
          reject(error);
        }
      });
    });
  }

  static wizardConverter(form) {
    if (form.display === 'wizard') {
      // Metodo 1: conversione semplice: espansione delle pagine in verticale
      form.display = 'form';
    }
    return Form.disable(form);
  }

  static disable(form) {
    let disabledForm = JSON.parse(JSON.stringify(form));

    if (disabledForm.components) {
      // Disable form components
      disabledForm.components.forEach(function (component, index) {
        disabledForm.components[index] = Form.disable(component);
      });
    } else if (disabledForm.columns) {
      // Disable columns components
      disabledForm.columns.forEach(function (column, index) {
        disabledForm.columns[index] = Form.disable(column);
      });
    } else {
      // Simple component
      // Disable JS custom calculated values
      if (disabledForm.type === 'select' && ['custom', 'url'].includes(disabledForm['dataSrc'])) {
        disabledForm.dataSrc = 'custom';
        if (disabledForm.data && disabledForm.data.custom) {
          // Disable custom js
          disabledForm.data.custom = '';
        }
      }
      // Disable JS custom default values
      if (disabledForm.customDefaultValue) {
        disabledForm.customDefaultValue = '';
      }
      // Disable JS custom calculated values
      if (disabledForm.calculateValue) {
        disabledForm.calculateValue = '';
      }
      // Disable JS validation
      if (disabledForm.validate) {
        disabledForm.validate.custom = '';
      }
    }
    return disabledForm;
  }

  static initApplicationForm(applicationFormContainer) {
    const formUrl =
      applicationFormContainer.data('formserver_url') + '/form/' + applicationFormContainer.data('form_id');
    $.getJSON(
      formUrl + '/i18n?lang=' + applicationFormContainer.data('locale'),
      function (data) {
        Formio.icons = 'fontawesome';
        Formio.createForm(document.getElementById('formio'), applicationFormContainer.data('formserver_url') + '/form/' + applicationFormContainer.data('form_id'), {
          noAlerts: true,
          language: language,
          i18n: data,
          buttonSettings: {
            showCancel: false
          },
          sanitizeConfig: {
            allowedAttrs: ['ref', 'src', 'url', 'data-oembed-url'],
            allowedTags: ['oembed', 'svg', 'use'],
            addTags: ['oembed', 'svg', 'use'],
            addAttr: ['url', 'data-oembed-url']
          }
        }).then(function (form) {

          form.ready.then(() => {
            Form.createStepsMobile();
            Form.enableRestrictedOperatorFields(form);
          });

          // Recupero i dati della pratica se presenti
          if (applicationFormContainer.data('submission') !== '' && applicationFormContainer.data('submission') !== null) {
            form.submission = {
              data: applicationFormContainer.data('submission').data
            };
          }
          form.on('prevPage', function () {
          });
          $('.btn-wizard-nav-cancel').on('click', function (e) {
            e.preventDefault();
            location.reload();
          });

          form.nosubmit = true;
          // Triggered when they click the submit button.
          form.on('submit', function (submission) {
            let submitButton = applicationFormContainer.find('.btn-wizard-nav-submit');
            submitButton.html(`<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ${Translator.trans('salva', {}, 'messages', language)} ...`);

            let application = {};
            application.user = applicationFormContainer.data('user');
            application.service = applicationFormContainer.data('service');
            application.data = submission.data;
            application.status = 1900;

            applications.postApplication(JSON.stringify(application))
              .fail(function (xhr, type, exception) {
                submitButton.html(`${Translator.trans('salva', {}, 'messages', language)}`);
                Swal.fire(exception, '', 'error');
              })
              .done(function (data, code, xhr) {
                applicationFormContainer.addClass('d-none');
                applicationOwner.addClass('d-none');
                feedbackContainer.removeClass('d-none');
              });
          });

        });
      });

  }

  static async enableRestrictedOperatorFields(form) {

    const userRoles = await users.decodeAuthUser();

    if (userRoles && userRoles.roles.length > 0 && (userRoles.roles.includes('ROLE_OPERATORE') || userRoles.roles.includes('ROLE_MANAGER'))) {
      let foundFields = Utils.searchComponents(form.components, {
        'component.properties.operator_restricted': 'true'
      });
      if (foundFields.length > 0) {
        foundFields.forEach(component => {
          component.component.attributes = {};
          component.component.properties = {};
          component.component.customConditional = '';
          //Get reference field and set visible
          let getField = Utils.getComponent(form.components, component.component.key);
          getField.visible = true;
          getField.redraw();
        });
      }
    }
  }

  static getPage(key) {
    return parseInt(localStorage.getItem(key), 10) || 0;
  }

  static setPage(key, page) {
    localStorage.setItem(key, page);
  }

  static openPrivacyLink() {
    const formioHelper = new FormIoHelper();
    // off() This will remove all event handlers:
    $('.formio-helper-privacy-url').off().on('click', (event) => {
      formioHelper.getTenantInfo().then(result => {
        const meta = JSON.parse(result.meta[0]) || null;
        if (meta.legals.privacy_info) {
          window.open(meta.legals.privacy_info);
          return false;
        }
      }).catch(e => {
        console.error('error getTenantInfo', e);
      });
    });
  }

  static disableAttachmentLinks() {
    $('.formio-component-file a,.formio-component-sdcfile a').each(function () {
      $(this).parent().html($(this).html());
    });
  }
}

export default Form;
